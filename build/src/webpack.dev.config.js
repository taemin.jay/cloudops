const HtmlWebPackPlugin = require("html-webpack-plugin")

module.exports = {
    entry: ['babel-polyfill', './client/src/index.js'],
    output: {
    path: __dirname + '/client/dist_dev',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devtool: 'sourcemap',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                }
            },
            {
                test: /\.(gif|png|jpe?g|webp)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true, // webpack@1.x
                            disable: true, // webpack@2.x and newer
                        },
                    },
                ],
            },
            { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./client/src/assets/html/index.html",
            filename: "./index.html"
        })
    ],
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    devServer: {
        contentBase: './dist_dev',
        historyApiFallback: true
    }
};