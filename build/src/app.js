const express = require('express')
const app = express()
const expressValidator = require('express-validator')
const cors = require('cors')
const fs = require('fs')
const http = require('http')
const https = require('https')
const path    = require("path")
const bcrypt = require('bcrypt')
const sqlite3 = require('sqlite3').verbose()
const httpServer = http.createServer(app);
const privateKey  = fs.readFileSync('./server/certs/server.key', 'utf8');
const certificate = fs.readFileSync('./server/certs/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate}
const httpsServer = https.createServer(credentials, app)
const bodyParser = require("body-parser")

const logging = require('./server/logging')
const LOG = new logging.log("web-server",'',true)

const request = require('./server/requests_api')
const splunk = require('./server/splunk')
const dbCheck = require('./server/db_check')
const checkConnection = require('./server/check_vpn')
const dbAPI = require('./server/db_api')
const historyFormate = require('./server/history')
const getConfig = require('./server/get_config')
const saltRounds = 10

const db_path = './server/db_application.sqlite'
const API_CONFIG_CUSTOM = './conf/api-config.yml'
const API_CONFIG_DEFAULT = './api-config-default.yml'
const WEB_CONFIG_CUSTOM = './conf/web-config.yml'
const WEB_CONFIG_DEFAULT = './web-config-default.yml'

app.set('views', __dirname + '/client/dist/html');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

app.use(cors());
app.use(express.static(__dirname + '/client/dist'));
app.use('/css/fonts', express.static(__dirname + '/client/dist/fonts'));
app.use(express.static(__dirname + '/client/dist/images'));
app.use(express.static(__dirname + '/client/dist/css/semantic/out/components/'));
app.use('/themes/default/assets/fonts', express.static(__dirname + '/client/dist/css/semantic/out/themes/default/assets/fonts'));
app.use(express.json());

//---------------------------------------------------------------------------------------------------------------------
//----- AUTH ----------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------


app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(expressValidator());

const session = require('express-session');
const passport = require('passport');
const SQLiteStore = require('connect-sqlite3')(session);
const LocalStrategy = require('passport-local').Strategy;

app.use(session({
    store: new SQLiteStore({dir:path.join(__dirname+'/server'), db:'db_sessions.sqlite', table:'sessions'}),
    secret: 'sgsutklfgkjhr',
    cookie: { maxAge: 1 * 8 * 60 * 60 * 1000 }, // 8 houres
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}))

app.set('trust proxy', 1)
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy( (username, password, done) => {

    const db_conn = new sqlite3.Database('./server/db_sessions.sqlite')

    db_conn.serialize(function () {
        db_conn.all(`SELECT id, password, login FROM users WHERE login='${username}'`, (err, results) => {
            if (err) done(err)

            if (results.length === 0) {
                return done(null, false)
            } else {

                const hash = results[0].password.toString()
                bcrypt.compare(password, hash, function(err,response) {
                    if (response) {
                        return done(null, results[0].login)
                    }
                    return done(null, false)
                })

            }

        });
    })
}));


app.get('/', noCache, function(req,res) {

    LOG.info(`GET request ${req.originalUrl}`)
    LOG.debug(`User = ${req.user}, isAuthenticated = ${req.isAuthenticated()}`)

    if (req.isAuthenticated()) {
        res.sendFile(__dirname + "/client/dist/html/index.html");
    } else {
        res.redirect('/login')
    }
    //__dirname : It will resolve to your project folder.
});

function noCache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}

app.post('/registration', function(req,res) {
    const name = req.body.name,
        password = req.body.password,
        createDate = new Date()

    if (req.body.password === req.body.passwordConfirm ) {

        const db_conn = new sqlite3.Database('./server/db_sessions.sqlite')

        bcrypt.hash(password, saltRounds, function(err, hash) {

            LOG.debug(`Request /registration GET : ${name}, ${hash}, ${createDate}`)

            db_conn.run(`INSERT INTO users ('login','password','createDate') VALUES('${name}','${hash}','${createDate}')`, function (err) {
                if (err) {
                    LOG.error(err.message);
                    res.status(200).send({error: true, message: err.message});
                    return
                }
                res.status(200).send({error: false, message: "Пользователь  "+name+" создан"});
            });
            db_conn.close()

        })


    } else {
        res.status(200).send({error: true, message: "Пароли не совпадают."});
    }
    res.end
});

app.route('/login')
    .get(function(req,res) {
        res.render('login');
    })
    .post(function(req, res, next) {

        LOG.info(`GET request ${req.originalUrl}`)
        LOG.debug(req.body)

        const errors = req.validationErrors();

        if(errors) {
            LOG.error(errors)
            return res.render('login', { name: errors[0].msg})
        }

        passport.authenticate('local', function(err, user, info) {
            if (err) { return logger.log('error',err); }
            if (!user) { return res.redirect('/login'); }
            req.login(user, function(err) {
                if (err) { return next(err); }
                return res.redirect('/');
            });
        })(req, res, next);
    })

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

app.get("/logout", function(req,res) {
    LOG.info(`GET request ${req.originalUrl}`)
    res.redirect('/login')
    req.logout()
    req.session.destroy()
    //__dirname : It will resolve to your project folder.
});

//---------------------------------------------------------------------------------------------------------------------
//----- API -----------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

app.route('/api/v1/')
    .get( (req,res) => {
        LOG.info(`GET request ${req.originalUrl}`)
        return res.send("WEB tool started successfully")

    })

app.route('/api/v1/api-conf')
    .get( (req,res) => {
        LOG.info(`GET request ${req.originalUrl}`)
        return getConfig.get_config(API_CONFIG_CUSTOM,API_CONFIG_DEFAULT)
            .then( resolve => {
                LOG.debug(resolve)
                res.json(resolve)
            })

    })

app.route('/api/v1/web-conf')
    .get( (req,res) => {
        LOG.info(`GET request ${req.originalUrl}`)
        return getConfig.get_config(WEB_CONFIG_CUSTOM,WEB_CONFIG_DEFAULT)
            .then( resolve => {
                LOG.debug(resolve)
                res.json(resolve)
            })

    })

app.route('/api/v1/template/get/titles')
    .get(function(req,res) {
        LOG.info(`GET request ${req.originalUrl}`)
        let db_conn = dbAPI.dbConnect(db_path)
        dbAPI.DBGetTempTitle(db_conn)
            .then(resolve => {
                let output = dbAPI.titlesTemplatesFormate(resolve)
                res.send(output)
            })
            .catch(reject => {
                LOG.error(reject)
            })
            .then(dbAPI.dbClose(db_conn))
    })

app.route('/api/v1/template/get/services/:id')
    .get(function(req,res) {
        LOG.info(`GET request ${req.originalUrl}`)
        let db_conn = dbAPI.dbConnect(db_path)
        dbAPI.DBGetTempSrvs(db_conn, req.params.id)
            .then(resolve => {
                let output = dbAPI.srvsTemplatesFormate(resolve)
                res.send(output)
            })
            .catch(reject => {
               LOG.error(JSON.stringify(reject))
            })
            .then(dbAPI.dbClose(db_conn))
    })

app.route('/api/v1/template/delete/template/:id')
    .post(function(req,res) {
        LOG.info(`POST request ${req.originalUrl}`)
        let db_conn = dbAPI.dbConnect(db_path)
        dbAPI.DBDeleteTemplate(db_conn, req.params.id)
            .then(resolve => {
                LOG.debug(JSON.stringify(resolve))
                res.send(resolve)
            })
            .catch(reject => {
                LOG.error(JSON.stringify(reject))
                res.send(reject)
            })
            .then(dbAPI.dbClose(db_conn))
    })

app.route('/api/v1/template/save')
    .post(function(req,res) {
        LOG.info(`POST request ${req.originalUrl}`)
        let db_conn = dbAPI.dbConnect(db_path)
        dbAPI.DBSaveTemplate(db_conn, req.body)
            .then(resolve => {
                LOG.debug(JSON.stringify(resolve))
                res.send(resolve)
            })
            .catch(reject => {
                LOG.error(JSON.stringify(reject))
                res.send(reject)
            })
            .then(dbAPI.dbClose(db_conn))
    })

app.route('/api/v1/template/update/title/:id/:value')
    .post(function(req,res) {
        LOG.info(`POST request ${req.originalUrl}`)
        let db_conn = dbAPI.dbConnect(db_path)
        dbAPI.DBUpdateTemplateTitle(db_conn, req.params.id, req.params.value)
            .then(resolve => {
                LOG.debug(JSON.stringify(resolve))
                res.send(resolve)
            })
            .catch(reject => {
                LOG.error(JSON.stringify(reject))
                res.send(reject)
            })
            .then(dbAPI.dbClose(db_conn))
    })

app.route('/api/v1/:tool/:api/:env_or_id')
    .get(async function(req,res){
        LOG.info(`GET request ${req.originalUrl}`)
        return request.getRequestAPI(request.getOptions(req.params.tool, req.params.api, req.params.env_or_id, 'GET'))
            .then(answer => {

                //отдельный модуль под спланк
                if (req.params.tool === "splunk-tool" && req.params.api === "status") {

                    if (answer.progress === 100 && answer.status === "DONE") {

                        let splunk_result_option = {
                            hostname: request.appServices["splunk-tool"].hostname,
                            port: request.appServices["splunk-tool"].port,
                            path: request.appServices["splunk-tool"].api.result + req.params.env_or_id,
                            method: 'GET',
                            timeout: 600000,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }

                        splunk.getRequestAPI(splunk_result_option)
                            .then(resolve => {

                                answer['length'] = resolve

                                return res.json(answer)

                            })
                            .catch(err => {
                                LOG.error(err)
                                res.json(err)
                            })

                    } else {

                        res.json(answer)

                    }

                } else {

                    res.json(answer)

                }
            })
            .catch(err => {
                LOG.error(err)
                res.json(err)
            })
    })

app.route('/api/v1/:tool/:api')
    .get(function(req,res){
        LOG.info(`GET request ${req.originalUrl}`)
        return request.getRequestAPI(request.getOptions(req.params.tool, req.params.api, null, 'GET'))
            .then(answer => {
                LOG.debug(JSON.stringify(answer))

                if (req.params.api === "getHistory") {

                    answer = historyFormate.groupHistoryJobFunction(answer)

                }

                res.json(answer)
            })
            .catch(err => {
                LOG.error(JSON.stringify(err))
                res.json(err)
            })
    })
    .post(function(req,res){
        LOG.info(`POST request ${req.originalUrl}`)
        return request.postRequestAPI(request.getOptions(req.params.tool, req.params.api, null, 'POST'),req.body)
            .then(answer => {
                LOG.debug(JSON.stringify(answer))
                res.json(answer)
            })
            .catch(err => {
                LOG.error(JSON.stringify(err))
                res.json(err)
            })
    })

app.route('/api/v1/splunk-tool/get/result/:start/:end')
    .get(function(req,res) {
        LOG.info(`GET request ${req.originalUrl}`)

        return splunk.getChunkData(req.params.start,req.params.end)
            .then(resolve => {
                LOG.debug(JSON.stringify(resolve))
                res.send(resolve)
            })
            .catch(reject => {
                LOG.error(JSON.stringify(reject))
                res.send(reject)
            })


    })

app.route('/api/v1/check/connection/ip/:ip')
    .get(function(req,res){
        LOG.info(`GET request ${req.originalUrl}`)
        return checkConnection.checking_connection(req.params.ip)
            .then(answer => {
                res.send(answer)
            })
    })

app.get(['/deploy', '/console', '/deploy-tool', '/aws', '/splunk', '/settings', '/history', '/settings-deploy', '/templates'], (req,res) =>{
    res.sendFile(path.join(__dirname+'/client/dist/html/index.html'));
});


httpServer.listen(8007,'0.0.0.0',function(){
    LOG.info(`started http://localhost:8007`)
});
httpsServer.listen(8008,'0.0.0.0',function(){
    LOG.info(`started https://localhost:8008`)
    dbCheck.dbCheck()
});
