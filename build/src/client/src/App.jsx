import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './components/Header/Header'
import Menu from './components/Menu/Menu'
import Applications from './components/Applications/Applications'
import Notifications from "./components/Patterns/Notifications";
import Cart from './components/Applications/Deploy/Cart/Cart'
import DataAdditionalBlock from "./components/Patterns/DataAdditionalBlock"

import 'bootstrap/dist/css/bootstrap.css'
import './assets/semantic/out/components/icon.css'

import './assets/css/color-theme-blue.css'
import './assets/css/index.css'
import {connect} from "react-redux";
import library_c from "./assets/js/library_c";
import axios from "axios/index";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cart_disabled: false,
            timer: false,
            requestDelay: 5000
        }
        this.function = {
            updateServiceList: this.updateServiceList
        }
    }

    componentDidMount() {

        const task_id = this.props.task_id
        if (task_id) {

            this.updateConsoleFunction(task_id)
            this.console = setInterval(
                () => this.updateConsoleFunction(task_id),
                this.state.requestDelay
            );
            this.state.timer = true

        } else {

            clearInterval(this.console);
            this.setState({
                timer: false
            })

        }

        return this.getAPIconf(), this.getWEBconf(), this.location_setPath()
    }

    componentWillUnmount() {

        this.unlisten();
        clearInterval(this.console);
        return this.setState({
            timer: true
        })

    }

    componentDidUpdate(prevProps) {

        const timer = this.props.timer,
            task_id = this.props.task_id

        if (!timer && prevProps.timer) {

            clearInterval(this.console);
            return this.setState({
                timer: false
            })

        } else if (timer && !prevProps.timer) {

            this.console = setInterval(
                () => this.updateConsoleFunction(task_id),
                this.state.requestDelay
            );

            return this.setState({
                timer: true
            })

        } else if (timer) {

            if (prevProps.task_id !== task_id ) {

                clearInterval(this.console);
                this.updateConsoleFunction(task_id)
                this.console = setInterval(
                    () => this.updateConsoleFunction(task_id),
                    this.state.requestDelay
                );
            }
        }

    }

    Notification(type, message) {

        switch(type) {

            case 'Update-Console':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Can't to update Console. Creating options failed!",
                    title: "Console"
                })

            case 'Excludes-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Can't to set/post Excludes. Creating options failed!",
                    title: "Settings"
                })

            case 'Includes-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Can't to set/post Includes. Creating options failed!",
                    title: "Settings"
                })

            case 'Excludes-Success':
                return this.props.notifyMe({
                    type: 'success',
                    message: "Excludes have been applied",
                    title: "Settings"
                })

            case 'Includes-Success':
                return this.props.notifyMe({
                    type: 'success',
                    message: "Includes have been applied",
                    title: "Settings"
                })

            case "Updating-List-Warning":
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Can't to update services list. Creating options failed!",
                    title: "Settings"
                })

            case 'Updating-List-Success':
                return this.props.notifyMe({
                    type: 'success',
                    message: message,
                    title: "Settings"
                })

            case 'Сonf-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: `Can't to get api-conf.yml. All the requests will not work!`,
                    title: "Application"
                })
            case 'Сonf-Success':
                return this.props.notifyMe({
                    type: 'success',
                    message: message,
                    title: "Application"
                })
            case 'Сonf-Warn':
                return this.props.notifyMe({
                    type: 'warning',
                    message: message,
                    title: "Application",
                    browserNotify: false
                })
        }

    }


    //необохдимо для замены опций в header если изменилась url
    location_setPath() {
        this.unlisten = this.props.history.listen((location) => {
            this.props.showVariableOpt(location.pathname)
        });
        return this.props.showVariableOpt(location.pathname)
    }

    //getting application config: api
    getAPIconf() {
        const url = this.props.server + this.props.get_api_conf

        let answer
        axios.get(url)
            .then(response => {

                console.log('Get API configuration: '+ url)

                answer = response.data

                if (answer) {
                    if (answer.error1 || answer.error2) {
                        if (answer.error1) console.log(answer.error1)
                        if (answer.error2) console.log(answer.error2)
                        this.Notification('Сonf-Warn',answer.message)
                    } else {
                        this.Notification('Сonf-Success', answer.message)
                    }
                    return this.props.setAPIConf(answer), answer
                }

                this.Notification('Сonf-Error')
                return console.log("ERROR: APP.jsx: getAPIconf: response.data is not available: "+ JSON.stringify(response))

            })
            .catch( (err) => {

                console.log("ERROR: APP.jsx: getAPIConf: try to fetching api config list is failed: ")
                console.log(err)
                return this.Notification('Сonf-Error'), setTimeout(() => {return this.getAPIconf()}, 10000)

            })
    }

    //getting application config: web
    getWEBconf() {
        const url = this.props.server + this.props.get_web_conf

        let answer
        axios.get(url)
            .then(response => {

                console.log('Get WEB configuration: '+ url)

                answer = response.data

                if (answer) {
                    if (answer.error1 || answer.error2) {
                        if (answer.error1) console.log(answer.error1)
                        if (answer.error2) console.log(answer.error2)
                        this.Notification('Сonf-Warn',answer.message)
                    } else {
                        this.Notification('Сonf-Success',answer.message)
                    }
                    return this.props.setVPNSettings(answer.vpn)
                }

                this.Notification('Сonf-Warn', "Can't to get web-conf.yml. Will using settings from the box.")
                return console.log("ERROR: APP.jsx: getWEBconf: response.data is not available: "+ JSON.stringify(response))

            })
            .catch( (err) => {

                console.log("ERROR: APP.jsx: getWEBconf: try to fetching api config list is failed: ")
                console.log(err)
                return this.Notification('Сonf-Warn', "Can't to get web-conf.yml. Will try one more time"), setTimeout(() => {return this.getWEBconf()}, 10000)

            })
    }

    //---- DEPLOY.CONSOLE -----
    updateConsoleFunction = (id) => {

        //Validation
        if (!library_c.validationFunction(id)) return false

        //---- API. GET Template Service list ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_task_status', [id])

        //Notification
        if (!options) return this.Notification('Update-Console')

        console.log('Get services list from: '+ JSON.stringify(options))

        axios(options)
            .then( response => {

                let answer = response.data
                console.log(answer)
                if (answer) {
                    if (this.props.runningTasks !== answer.runningTasks) {
                        this.props.addTaskId("ADD_TASKS_ID", answer.runningTasks)
                    }
                    return this.props.updateOutput("UPDATE_OUTPUT", answer.task)
                }

                console.log("ERROR: APP.jsx updateConsole: response.data is not available: "+ JSON.stringify(response))
                return false

            })
            .catch( (err) => {

                console.log(err)

                console.log("ERROR: APP.jsx updateConsole: trying to fetch status failed: " + JSON.stringify(err))
                return false

            })

    }

    //---- DEPLOY.SETTINGS -----
    updateServiceList = async (data, auth) => {

        this.props.saveDataUpdating(data)

        let env = data.env,
            exclude = {env,excludes:data.excludes},
            simple_include = {simpleIncludes:data.simpleIncludes},
            options

        //---- API. SET Excludes --------------
        options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'exclude', exclude)

        //Notification
        if (!options) return this.Notification('Excludes-Warning')

        console.log('POST excludes: '+ JSON.stringify(options))

        axios(options)
            .then( () => {

                //Notification
                this.Notification('Excludes-Success')

            })
            .catch( (err) => {

                console.log("ERROR: APP.jsx updateServicesList: trying to post Excludes failed: " + JSON.stringify(err))
                //Notification
                this.Notification('Excludes-Warning')

            })

        //---- API. SET Includes --------------
        options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'simple_include', simple_include)

        //Notification
        if (!options) return this.Notification('Includes-Warning')

        console.log('POST includes: '+ JSON.stringify(options))

        axios(options)
            .then( () => {

                //Notification
                this.Notification('Includes-Success')

            })
            .catch( (err) => {

                console.log("ERROR: APP.jsx updateServicesList: trying to post Includes failed: " + JSON.stringify(err))
                //Notification
                this.Notification('Includes-Warning')

            })

        //---- API. Update serviceDB --------------
        auth = {...auth,env}
        options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'generate', auth)

        //Notification
        if (!options) return this.Notification('Updating-List-Warning')

        console.log('Updating List: '+ JSON.stringify(options))

        this.props.startUpdating()

        axios(options)
            .then(response => {

                console.log(response)
                //Notification
                this.Notification('Updating-List-Success', `Service list for ${env} has been updated`)
                return this.props.stopUpdating()

            })
            .catch( (err) => {

                console.log("ERROR: APP.jsx updateServicesList: trying to update failed: " + JSON.stringify(err))
                //Notification
                return this.Notification('Updating-List-Warning')

            })

    }

    render() {

        return (
            <div className="App">
                <Header />
                <main className={this.props.showCart ? "blur" : ""}>
                    <Menu />
                    <Applications />
                </main>
                <Cart />
                <Notifications />
                <DataAdditionalBlock function={this.function} />
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        task_id: state.building.task_id,
        timer: state.building.timer,
        server: state.appinfo.server,
        api: state.appinfo.api,
        get_api_conf: state.appinfo.get_api_conf,
        get_web_conf: state.appinfo.get_web_conf,
        settings: state.settings,
        showCart: state.cart.showCart
    };
}

export default withRouter(connect(mapStateToProps,
    dispatch => ({
        removeAllFromCart: () => {
            dispatch({type: 'REMOVE_ALL_FROM_CART'})
        },
        showVariableOpt: (path) => {
            dispatch({type: 'SHOW_VARIABL_OPTIONS', path: path})
        },
        updateOutput: (type, output) => {
            dispatch({type: type, output: output})
        },
        saveDataUpdating: (data) => {
            dispatch({type: "SETTINGS_SET_INC&&EXCLUDES", data: data})
        },
        startUpdating: () => {
            dispatch({type: "START_UPDATING"})
        },
        stopUpdating: () => {
            dispatch({type: "STOP_UPDATING"})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        },
        setAPIConf: (data) => {
            dispatch({type: 'APP_SET_API', data: data})
        },
        setVPNSettings: (data) => {
            dispatch({type: 'CHANGE_VPN', data: data})
        },
        addTaskId: (type, data) => {
            dispatch({type: type, data})
        }
    }))(App))

