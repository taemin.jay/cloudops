import React, { Component } from 'react';
import {connect} from "react-redux";
import axios from "axios/index";

class DataAdditionalBlock extends Component {

    constructor () {
        super()
        this.state = {
            data: {}
        }
    }

    componentDidMount() {

    }

    handleChange = (e, key) => {

        return this.state.data[key] = e.target.value

    }

    hideBlock = () => {

        return this.props.hideBlock(), this.setState({
            data: {}
        })

    }

    submitForm(event, type) {

        event.preventDefault()

        let data, buff = []

        switch(type) {

            case "update-servicedb":

                data = Object.assign(this.props.additionaldatablock.data)

                this.props.function.updateServiceList(data, {login: this.state.data.login, password: this.state.data.password})

                //Notification
                this.props.notifyMe({
                    type: 'info',
                    message: `Updating service list...`,
                    title: "Settings"
                })

                return this.hideBlock()

            default:

                return false
        }

    }

    showBlock(type) {

        switch(type) {

            case "update-servicedb":

                return (
                    <div className="login d-flex flex-column">
                        <button type="button" onClick={this.hideBlock.bind()}><i className="close icon"></i></button>
                        <form method="" onSubmit={(e)=>this.submitForm(e,type)}>
                            <h2>
                                Authentication
                            </h2>
                            <div className="d-flex flex-row">
                                <label>Login</label><input className="default" required onChange={(e) => this.handleChange(e, "login")} />
                            </div>
                            <div className="d-flex flex-row">
                                <label>API</label><input className="default" required onChange={(e) => this.handleChange(e, "password")} />
                            </div>
                            <button type="submit">Update</button>
                        </form>
                    </div>
                )

            case "new-template":

                return (
                    <div className="new-template d-flex flex-column">
                        <button type="button" onClick={this.hideBlock.bind()}><i className="close icon"></i></button>
                        <form method="" onSubmit={(e)=>this.submitForm(e,type)}>
                        <h2>
                            New Template
                        </h2>
                        <input required onChange={(e) => this.handleChange(e, 'template')} />
                        <button type="submit">Save</button>
                        </form>
                    </div>
                )

            default:

                return (
                    <div className="default-additional-block d-flex flex-column">
                        <h2>There is a additional data block.</h2>
                        <p>You need to choose type of data for the applying the entering objects.</p>
                    </div>
                )
        }

    }

    render() {
        return (
            (this.props.additionaldatablock.show) ? (
                <div className="data-additional-block">
                    {this.showBlock(this.props.additionaldatablock.type)}
                </div>
            ) : null
        )

    }
}

function mapStateToProps (state) {
    return {
        additionaldatablock: state.additionaldatablock,
        server: state.appinfo.server,
        application: state.appinfo,
        settings: state.settings
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        hideBlock: () => {
            dispatch({type: 'HIDE_ADDITIONAL_BLOCK'})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(DataAdditionalBlock)