import React, { Component } from 'react';
import MaterialIcon from 'material-icons-react';

class Textarea extends Component {

    constructor (props) {
        super(props)
        this.state = {
            disabled: props.disabled || false,
            mouseover: false,
            focus: false,
            value: ""
        }
    }

    componentDidUpdate(prevProps) {

        if (prevProps.disabled !== this.props.disabled) {
            return this.setState({
                disabled: this.props.disabled || false
            })
        }

    }

    showA(data, type) {

        return data.map((item, index) => {

            let color

            if (type === "excludes") {
                color = "pink"
            } else {
                color = "teal"
            }
            return (
                <a key={index} className={`ui ${color} tag label`}
                   onClick={this.deleteItem.bind(this, item)}>{item}</a>
            )
        })



    }

    showList(data, type) {

        if (Array.isArray(data)) {

                return (
                    <React.Fragment>
                        <div className="d-flex flex-row flex-wrap align-items-baseline">
                            {this.showA(data, type)}
                        </div>
                        <div onMouseOver={this.trueActiveClass} onMouseOut={this.falseActiveClass} className="settings-add-new-exinclude">

                                <MaterialIcon onClick={this.checkValue.bind()} icon="add" color="#4CAF50"/>

                            <input value={this.state.value} className={`settings-deploy ${ (this.state.mouseover || this.state.focus) ? 'mouseover' : ''}`} onFocus={this.focusActiveClass} onBlur={this.checkValue} onChange={this.getValue.bind(this)}/>
                        </div>
                    </React.Fragment>
                )

        }

        return (
            <React.Fragment>
                <pre>No data</pre>
            </React.Fragment>
        )

    }

    getValue = (e) => {

        return this.setState({
            value: e.target.value
        })

    }

    trueActiveClass = () => {
        return this.setState({
            mouseover: true
        })
    }

    falseActiveClass = () => {
        return this.setState({
            mouseover: false
        })
    }

    focusActiveClass = () => {
        return this.setState({
            focus: true
        })
    }

    checkValue = () => {

        if (this.state.value.length > 0) {

            this.setState({
                value: "",
            })
            this.props.callback.get(this.state.value)

        }

        return this.setState({
            focus: false
        })

    }

    deleteItem = (item) => {

        return this.props.callback.delete(item)

    }


    render() {
        return this.showList(this.props.data, this.props.type)
    }
}

export default Textarea;