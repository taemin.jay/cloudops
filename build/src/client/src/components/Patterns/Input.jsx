import React, { Component } from 'react';

class Input extends Component {

    preventDefaultFunc = (e) => {

        e.preventDefault(); // Let's stop this event.
        e.stopPropagation(); // Really this time.

    }

    inputElement(label, value, placeholder, datakey, datakey2, className) {

        return (

            <input className={`default ${className}`}
                   value={value}
                   placeholder={placeholder}
                   onClick={this.preventDefaultFunc.bind(this)}
                   onChange={(e) => this.props.onChange(e, label, datakey, datakey2)}
                   data-key={datakey}
                   data-key2={datakey2}
            />

        )

    }

    showElement(index, label, placeholders, type, value, datakey, datakey2, className) {

        let placeholder, input, data_key, data_key2

        (value) ? input = value : input = "";
        (placeholders) ? placeholder = placeholders[label] : placeholder = label;
        (datakey) ? data_key = datakey : data_key = "default";
        (datakey2) ? data_key2 = datakey2 : data_key2 = "default";
        (!className) ? className = label : null;

        switch(type) {

            case "NO_TAG_LABEL":

                return (
                    <div key={index} className="options-input d-flex flex-column align-items-center">
                        {this.inputElement(label, input, placeholder, data_key, data_key2, className)}
                    </div>
                )

            case "DEPLOY":

                return (
                    <div key={index} className="options-input d-flex flex-column align-items-center">
                        <label>{label}</label>
                        <input className={`default`}
                               value={value}
                               placeholder={placeholders[label]}
                               onClick={this.preventDefaultFunc.bind(this)}
                               onChange={(e) => this.props.onChange(e, label, data_key, data_key2)}
                               data-key={data_key}
                               data-key2={data_key2}
                        />
                    </div>
                )

            default:

                return (
                    <div key={index} className="options-input d-flex flex-column align-items-center">
                        <label>{label}</label>
                        {this.inputElement(label, input, placeholders, data_key, data_key2, className)}
                    </div>
                )

        }

    }

    choosefile = () => {

        this.refs.fileUploader.click();

    }

    showInput = (labels, placeholders, type, specifyProps, className) => {

        let value = "", values, datakey, datakey2

        if (specifyProps) {

            (specifyProps.value) ? value = specifyProps.value : null ;

            (specifyProps.values) ? values = specifyProps.values : null ;

            (specifyProps.dataInputKey) ? datakey = specifyProps.dataInputKey : null ;

            (specifyProps.dataInputKey2) ? datakey2 = specifyProps.dataInputKey2 : null ;

        }

        if (Array.isArray(labels)) {

            switch(type) {

                case "NO_TAG_LABEL":

                    return labels.map((label, index) => {
                        return this.showElement(index, label, placeholders, type, value, datakey, datakey2, className)
                    })

                case "SWITCH_TOGGLE":

                    return labels.map((label, index) => {
                            return (
                                <div key={index} className="d-flex checkbox flex-column align-items-center">
                                    <label>{label}</label>
                                    <label className="switch">
                                        <input checked={value}
                                               type="checkbox"
                                               data-key={datakey}
                                               data-key2={datakey2}
                                               onChange={(e) => this.props.onChange(e, value, datakey, datakey2)}/>
                                        <span className="slider round"></span>
                                    </label>
                                </div>
                            )
                        })

                case "DEPLOY":

                    return labels.map((label, index) => {
                        if (values) {
                            value =  values[label]
                        }
                        return this.showElement(index, label, placeholders, type, value, datakey, datakey2)
                    })

                case "SETTINGS":

                    return labels.map((label, index) => {
                        return this.showElement(index, label, placeholders, type, value, datakey, datakey2)
                    })

            }
        }

        switch(type) {

            case "NO_TAG_LABEL":

                return this.showElement('withoutlabel', labels, placeholders, type, value, datakey, datakey2, className)

            case "SWITCH_TOGGLE":

                return (
                    <div>
                        <label className="switch">
                            <input checked={value}
                                   type="checkbox"
                                   data-key={datakey}
                                   data-key2={datakey2}
                                   onChange={(e) => this.props.onChange(e, value, datakey, datakey2)}/>
                            <span className="slider round"></span>
                        </label>
                    </div>

                )

            case 'FILE':

                return (
                    <input type="file"
                           ref="fileUploader"
                           data-key={datakey}
                           className={`default ${labels}`}
                           onChange={this.props.onChange.bind(this)}/>
                )

            case "FILE-SSH":

                return (
                    <div className="options-input d-flex flex-column align-items-center">
                        <input type="file"
                               style={{display:'none'}}
                               ref="fileUploader"
                               data-key={datakey}
                               onChange={(e) => this.props.onChange(e,datakey)}/>
                        <button type="file"
                                className={`default ${labels} ${ datakey2 ? 'active' : ''}`}
                                onClick={this.choosefile.bind(this)}>
                            {value}
                            <i className="key icon"></i>
                        </button>
                    </div>
                )

            default:
                return (
                    <div className="options-input d-flex flex-column align-items-center">
                        No data
                    </div>
                )

        }

    }

    render() {
        return this.showInput(this.props.labels, this.props.placeholders, this.props.type, this.props.specifyProps, this.props.className)

    }
}

export default Input;