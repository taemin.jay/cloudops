import React, { Component } from 'react'

class Status extends Component {

    showStatusTask(status) {

        if (status === "SUCCESS" || status === "FINISHED") {

            return <i className="check icon success"></i>

        } else if (status === "FAILED" || status === "NOT FOUND" || status === "FAILURE" || status === "ENOTFOUND" || status === "ETIMEDOUT") {

            return <i className="ban icon error"></i>

        } else if (status === "WARNING") {

            return <i className="exclamation triangle icon warning"></i>

        }  else if (status === "STARTING" || status === "RUNNING") {

            return <div className="lds-dual-ring"></div>

        }  else if (status === "IN QUEUE" || status === "PENDING" || status === "in queue" || status === "pending") {

            return <div className="lds-ellipsis">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>

        } else if (status === "STOPPED") {

            return <i className="stop circle outline icon warning"></i>

        } else if (status === "PAUSED") {

            return <i className="pause circle outline icon warning"></i>

        } else if (status === "NON CONSISTENT") {

            return <i className="question circle outline icon" style={{color:"var(--color-text)"}}></i>

        }

        return null

    }

    render() {
        return (
            <div className="job-status">
                {this.showStatusTask(this.props.status)}
            </div>
        )
    }
}

export default Status

