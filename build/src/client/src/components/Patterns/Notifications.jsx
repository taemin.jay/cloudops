import React, { Component } from 'react';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import 'react-notifications/lib/notifications.css';
import {connect} from "react-redux";


class Notifications extends Component {

    constructor () {
        super()
        this.state = {
            browser: false
        }
    }

    componentDidMount() {

        window.addEventListener("focus", this.onFocus)
        window.addEventListener("blur", this.onBLur)

        if (!('Notification' in window)) {
            alert('This browser does not support system notifications');
            return;
        }

        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }

        return this.setState({
            browser: !document.hasFocus()
        })
    }

    componentWillUnmount() {

        window.removeEventListener("focus", this.onFocus)
        window.removeEventListener("blur", this.onBLur)

    }

    onFocus = () => {

        return this.setState({
            browser: false
        })
    }

    onBLur = () => {

        return this.setState({
            browser: true
        })
    }

    componentDidUpdate(prevProps) {

        if (prevProps.notification !== this.props.notification) {

            if (Array.isArray(this.props.notification)) {

                this.props.notification.forEach( data => {
                    this.createNotification(data)
                })

            }

            this.createNotification(this.props.notification)

        }

    }

    browserNotifyMe(message) {

        const notification = new Notification("", {
            requireInteraction: true,
            silent: false,
            body: message,
        })
        notification.onclick = function () {
            window.focus();
        };

    }




    createNotification = (data) => {

        console.log(data.browserNotify)

        const type = data.type,
            message = data.message,
            title = data.title,
            browser = this.state.browser

        let browserNotify

        data.browserNotify ? browserNotify = data.browserNotify : browserNotify = true

        console.log(browserNotify)

        switch (type) {
            case 'info':

                if (browser && browserNotify) this.browserNotifyMe(message)

                NotificationManager.info(message)

                break

            case 'success':

                if (browser && browserNotify) this.browserNotifyMe(message)

                NotificationManager.success(message, title)

                break

            case 'warning':

                if (browser && browserNotify) this.browserNotifyMe(message)

                NotificationManager.warning(message, title)

                break

            case 'error':

                if (browser && browserNotify) this.browserNotifyMe(message)

                NotificationManager.error(message, title)

                break

        }

        return false

    };


    render() {

        return (
            <NotificationContainer/>
        )

    }
}

function mapStateToProps (state) {
    return {
        notification: state.notification.data
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        cleanNotify: () => {
            dispatch({type: 'CLEAN_NOTIFY'})
        }
    }))(Notifications)
