import React, { Component } from 'react';
import HeaderAppName from './HeaderAppName'
import HeaderVariableOptions from './HeaderVariableOptions'
import HeaderOptions from './HeaderOptions'

class Header extends Component {


    render() {
        return (
            <header className="header">
                <div className="contain">
                    <div className="block d-flex flex-row align-items-center justify-content-between">
                        <HeaderAppName/>
                        <HeaderVariableOptions />
                        <HeaderOptions />
                    </div>
                </div>

            </header>
        );
    }
}

export default Header;