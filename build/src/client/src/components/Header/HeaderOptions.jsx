import React, { Component } from 'react';
import {connect} from 'react-redux';
import HeaderOptionsList from '../../constructor/header-options'

class HeaderOptions extends Component {

    render() {
        return (
            <div className="header-options d-flex flex-row align-items-center justify-content-between">
                <HeaderOptionsList />
            </div>
        );
    }
}

export default HeaderOptions;