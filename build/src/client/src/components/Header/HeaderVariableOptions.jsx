import React, { Component } from 'react';
import HeaderVariableOptionsList from '../../constructor/header-variable-options'

class HeaderVariableOptions extends Component {

    render() {
        return (
            <div className="header-variable-options d-flex flex-row align-items-center justify-content-between">
                <HeaderVariableOptionsList />
            </div>
        );
    }
}

export default HeaderVariableOptions;