import React, { Component } from 'react';
import {connect} from 'react-redux';

class HeaderAppName extends Component {

    showName(name) {
        return <h1>{name}</h1>
        //return null
    }

    render() {
        return (
            <div className="header-app-name d-flex flex-row align-items-center justify-content-between">
                <div className="app-name d-flex flex-row justify-content-between">
                    <div className="name1">{this.showName(this.props.appinfo.name1)}</div>
                    <div className="name2">{this.showName(this.props.appinfo.name2)}</div>
                </div>
            </div>
        );
    }
}

export default connect(state => ({
    appinfo: state.appinfo
}))(HeaderAppName);