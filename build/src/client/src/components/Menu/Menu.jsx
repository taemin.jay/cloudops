import React, { Component } from 'react';
import RouterList from '../../constructor/menu-router'


class Menu extends Component {


    render() {
        return (
            <menu className="menu">
                    <div className="d-flex flex-column align-items-center">
                        <RouterList />
                    </div>
            </menu>
        );
    }
}

export default Menu;