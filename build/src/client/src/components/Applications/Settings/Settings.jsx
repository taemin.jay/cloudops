import React, { Component } from 'react';
import SettingsAWS from "./AWS/SettingsAWS"
import SettingsSPLUNK from './Splunk/SettingsSPLUNK'
import SettingsDashboard from './Home/SettingsDashboard'

class Settings extends Component {

    render() {
        return (
            <div className="application">
                <div className="d-flex flex-row">
                    <SettingsAWS />
                </div>
                <div className="d-flex flex-row">
                    <SettingsSPLUNK />
                </div>
            </div>
        )
    }
}

export default Settings;