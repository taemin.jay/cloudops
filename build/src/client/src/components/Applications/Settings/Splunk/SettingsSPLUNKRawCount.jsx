import React, { Component } from 'react';
import Input from '../../../Patterns/Input'
import {connect} from "react-redux";

class SettingsSPLUNKRawCount extends Component {

    applyInputValue = (e) => {

        const value = e.target.value
        return this.props.applyInputValue('SPLUNK_COUNT_RAW', value)

    }

    render() {
        return (
            <div className="settings-list flex-2 d-flex flex-column">
                <h5>Logs count on page</h5>
                <div className="d-flex settings-block flex-row flex-wrap justify-content-around align-items-center">
                    <Input key="settings-ssh-username"
                           labels={["count"]}
                           specifyProps={{
                               value: this.props.settings.splunk.raw_count}}
                           type="NO_TAG_LABEL"
                           onChange={this.applyInputValue}/>
                </div>
            </div>
        )

    }
}


function mapStateToProps (state) {
    return {
        settings: state.settings
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        applyInputValue: (type, value) => {
            dispatch({type: type, value:value})
        }
    }))(SettingsSPLUNKRawCount)


