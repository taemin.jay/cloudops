import React, { Component } from 'react';
import Input from '../../../Patterns/Input'
import {connect} from "react-redux";

class SettingsSPLUNKDomains extends Component {

    applyInputValue = (e, label, datakey) => {

        const value = e.target.value

        return this.props.applyInputValue('SPLUNK_DOMAINS', value, datakey)

    }

    showInput = (urls) => {

        console.log(urls)
        return urls.list.map( (url, index) => {
            return (
                <Input key={index} labels={[url.type+" ("+url.name+")"]}
                       specifyProps={{
                           value: url.domain,
                           dataInputKey: url.name}}
                       onChange={this.applyInputValue}
                type="SETTINGS"/>
            )
        })

    }

    render() {
        return (
            <div className="settings-list flex-4 d-flex flex-column">
                <h5>Hosts</h5>
                <div className="splunk-hosts settings-block d-flex flex-row flex-wrap justify-content-around align-items-center">
                    {this.showInput(this.props.settings.splunk.urls)}
                </div>
            </div>
        )

    }
}


function mapStateToProps (state) {
    return {
        settings: state.settings
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        applyInputValue: (type, value, datakey) => {
            dispatch({type: type, domain:value, name: datakey})
        }
    }))(SettingsSPLUNKDomains)


