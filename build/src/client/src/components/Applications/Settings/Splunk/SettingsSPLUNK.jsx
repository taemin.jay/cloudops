import React, { Component } from 'react';
import SettingsSPLUNKRawCount from './SettingsSPLUNKRawCount'
import SettingsSPLUNKDomains from './SettingsSPLUNKDomains'

class SettingsSPLUNK extends Component {

    render() {
        return (
            <div className="settings">
                <h4>SPLUNK Settings</h4>
                <div className="d-flex flex-row">
                    <SettingsSPLUNKRawCount/>
                    <SettingsSPLUNKDomains />
                </div>
            </div>
        )
    }
}

export default SettingsSPLUNK;