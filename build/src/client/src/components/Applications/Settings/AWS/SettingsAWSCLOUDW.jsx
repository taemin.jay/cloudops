import React, { Component } from 'react';
import Input from '../../../Patterns/Input'
import {connect} from "react-redux";

class SettingsAWSCLOUDW extends Component {

    applyCheckbox = (e,checked) => {

        return this.props.applyInputValue('AWS_autoCLOUDW',!checked)

    }


    render() {
        return (
            <div className={`settings-list d-flex flex-column ${this.props.className}`}>
                <h5>Cloud Watch</h5>
                <div className="d-flex settings-block flex-row flex-wrap justify-content-around align-items-center">
                    <Input key="settings-ssh-autocloudw" labels={["Auto CloudWatch"]} specifyProps={{value: this.props.settings.aws.auto_cloudwatch}} type="SWITCH_TOGGLE" onChange={this.applyCheckbox}/>
                </div>
            </div>
        )

    }
}


function mapStateToProps (state) {
    return {
        settings: state.settings
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        applyInputValue: (type, value) => {
            dispatch({type: type, value:value})
        }
    }))(SettingsAWSCLOUDW)

