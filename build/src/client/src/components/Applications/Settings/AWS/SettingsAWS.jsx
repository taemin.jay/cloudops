import React, { Component } from 'react';
import SettingsAWSSSH from "./SettingsAWSSSH";
import SettingsAWSCLOUDW from "./SettingsAWSCLOUDW"

class SettingsAWS extends Component {

    render() {
        return (
            <div className="settings-aws d-flex flex-column">
                <h4>AWS Settings</h4>
                <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                        <SettingsAWSSSH className="flex-4"/>
                        <SettingsAWSCLOUDW className="flex-4"/>
                    </div>
                </div>
            </div>
        )

    }
}

export default SettingsAWS;