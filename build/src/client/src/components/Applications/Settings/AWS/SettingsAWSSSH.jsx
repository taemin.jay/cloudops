import React, { Component } from 'react';
import Input from '../../../Patterns/Input'
import {connect} from "react-redux";

class SettingsAWSSSH extends Component {

    applyInputValue = (e) => {

        const value = e.target.value
        return this.props.applyInputValue('AWS_USERNAME', value)

    }

    applyCheckbox = (e,checked) => {

        return this.props.applyInputValue('AWS_autoSSH',!checked)

    }

    uploadKey = (e, key) => {

        const file = e.target.files[0],
            reader = new FileReader()

        let data

        reader.readAsText(file)
        reader.onloadend = (e) => {
            try {
                data = reader.result

                this.props.applyKey(key,file.name,data)

            } catch (e) {

                console.log(e)

                //Notification
                this.props.notifyMe({
                    type: 'error',
                    message: `File upload failed!`,
                    title: "AWS Settings"
                })

            }
        }

    }


    render() {
        return (
            <div className={`settings-list d-flex flex-column ${this.props.className}`}>
                <h5>SSH</h5>
                <div className="d-flex settings-block flex-row flex-wrap justify-content-around align-items-center">
                    <div className="d-flex flex-column">
                        <Input key="settings-ssh-username"
                               labels={["username"]}
                               specifyProps={{value: this.props.settings.aws.username}}
                               type="NO_TAG_LABEL"
                               onChange={this.applyInputValue}/>
                        <Input key="settings-ssh-keyprod"
                               labels="ssh key-prod"
                               specifyProps={{value: this.props.settings.aws.hostAWS_key_prod.filename, dataInputKey: "hostAWS_key_prod", dataInputKey2: this.props.settings.aws.hostAWS_key_prod.disabled}}
                               type="FILE-SSH"
                               onChange={this.uploadKey}/>
                        <Input key="settings-ssh-keydev"
                               labels="ssh key-dev"
                               specifyProps={{value: this.props.settings.aws.hostAWS_key_nonprod.filename,dataInputKey: "hostAWS_key_nonprod", dataInputKey2: this.props.settings.aws.hostAWS_key_nonprod.disabled}}
                               type="FILE-SSH"
                               onChange={this.uploadKey}/>
                    </div>
                    <Input key="settings-ssh-autossh"
                           labels={["Auto SSH"]}
                           specifyProps={{value: this.props.settings.aws.auto_ssh}}
                           type="SWITCH_TOGGLE"
                           onChange={this.applyCheckbox}/>
                </div>
            </div>
        )

    }
}


function mapStateToProps (state) {
    return {
        settings: state.settings
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        applyInputValue: (type, value) => {
            dispatch({type: type, value:value})
        },
        applyKey: (key, filename, value) => {
            dispatch({type: 'AWS_KEY', key: key, filename: filename, value:value})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(SettingsAWSSSH)
