import React, { Component } from 'react'
import {connect} from "react-redux"
import library_c from "../../../../assets/js/library_c"
import { withRouter } from 'react-router-dom'
import axios from "axios/index"
import Status from '../../../Patterns/Status'

class ConsoleTasks extends Component {

    constructor (props) {
        super()
        this.state = {
            active: false,
            active_task: props.type == "deploy-tool" ? false : props.task_id || false,
            running_tasks: [],
            api: false,
        }
    }

    componentDidMount() {

        if (this.props.api) {
            return this.getRunningTasksList()
        }

    }

    componentDidUpdate(prevProps) {

        if (!this.state.api && this.props.api !== false) {
            return this.getRunningTasksList(), this.setState({
                api: true
            })
        }

        if (prevProps.runningTasks !== this.props.runningTasks) {
            return this.setState({
                running_tasks: this.props.runningTasks ? this.props.runningTasks : [],
                active_task: this.props.type == "deploy-tool" ? false : this.props.task_id || false
            })
        }

    }

    Notification(type, message) {

        switch(type) {

            case 'Console-ERROR-tasks':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of Running Tasks!",
                    title: "Console"
                })

            case 'Console-ERROR-jobs':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Console"
                })

        }

    }

    getRunningTasksList() {

        //---- API. GET Service list by Env ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'running_tasks')

        //Notification
        if (!options) return this.Notification('Console-ERROR-tasks')

        console.log('Get running tasks list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    console.log(answer)
                    return this.setState({
                        running_tasks: answer
                    }), this.props.addTaskId("ADD_TASKS_ID", answer)
                }

                return this.setState({
                    running_tasks: "Queue empty"
                })

            })
            .catch(reject => {

                console.log("ERROR: fetching running tasks list is failed: " + JSON.stringify(reject))
                return this.setState({
                    running_tasks: "Something went wrong. Check the logs"
                })


            })


    }

    getJobsList = (task) => {

        this.showActive(task)

        return this.props.history.push("/console"), this.props.addTaskId("SHOW_TASK_ID",task.task_id)

        /*
        //---- API. GET jobs list by Env ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_jobs', [task.task_id])

        //Notification
        if (!options) return this.Notification('Console-ERROR-jobs', `Can't to get list of JObs for #${task.task_id}`)

        console.log('Get jobs list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    console.log(answer, task.task_id)
                    this.props.updateOutput("UPDATE_OUTPUT", answer)
                    if (this.props.type === "deploy-tool") {
                        return this.props.history.push("/console"), this.props.addTaskId("SHOW_TASK_ID",task.task_id)
                    }
                    return this.props.addTaskId("SHOW_TASK_ID",task.task_id)
                }

                console.log("ERROR: ConsoleTasks.jsx: getJobsList: response is "+ JSON.stringify(response))
                return this.props.updateOutput("UPDATE_OUTPUT","...something went wrong. Check browser console.")

            })
            .catch(reject => {

                console.log("ERROR: fetching jobs list failed: " + JSON.stringify(reject))
                return this.props.updateOutput("UPDATE_OUTPUT","...something went wrong. Check browser console.")

            })
            */


    }

    showActive = (task) => {
        return this.setState({
            active_task: task.task_id
        })
    }

    showQueueTasks(running_tasks, active_task) {

        if (Array.isArray(running_tasks) && running_tasks.length > 0) {

           return running_tasks.map((task, index) => {

               return (
                   <div key={index} onClick={this.getJobsList.bind(this, task)}
                        className={`console-task d-flex flex-row justify-content-between align-items-center ${active_task === task.task_id ? "active-task" : null}`}>
                       <div className="task-short-info d-flex flex-column">
                           <div className="task-id">{task.task_id}</div>
                           <div className="task-env">{task.environment}</div>
                       </div>
                       <div className="task-status">
                           <Status status={task.status} />
                       </div>
                   </div>

               )
           })
        }

        if (typeof running_tasks === "string" || running_tasks.length === 0) {

            return (

                <div className="d-flex flex-column justify-content-between align-items-center">
                    <h5></h5>
                </div>

            )

        }

        return (

            <div className="lds-dual-ring onload"></div>

        )

    }

    render() {
        return (
            <div className="application task-queue d-flex flex-column">
                <div className="default-header-block">
                    <h5>Tasks Queue</h5>
                </div>
                <div className="list-item">
                    {this.showQueueTasks(this.state.running_tasks, this.state.active_task)}
                </div>
            </div>
        )

    }
}

function mapStateToProps (state) {
    return {
        cart: state.cart,
        server: state.appinfo.server,
        api: state.appinfo.api,
        application: state.appinfo,
        task_id: state.building.task_id,
        runningTasks: state.building.runningTasks,
        nameconvention: state.nameconvention,
        rules: state.rules,
    };
}

export default withRouter(connect(mapStateToProps,
    dispatch => ({
        addTaskId: (type, id) => {
            dispatch({type: type, data: id})
        },
        updateOutput: (type, output) => {
            dispatch({type: type, output: output})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(ConsoleTasks))