import React, { Component } from 'react';
import {connect} from "react-redux";
import library from '../../../../assets/js/library'
import axios from "axios/index";
import library_c from "../../../../assets/js/library_c";

class ConsoleOutput extends Component {

    constructor() {
        super()
        this.status = {
            error: [
                "FAILED", "NOT FOUND", "FAILURE", "ENOTFOUND", "ETIMEDOUT"
            ],
            success: [
                "SUCCESS"
            ],
            process: [
                "PENDING", "IN QUEUE", "RUNNING", "STARTING"
            ],
            stoppaused: [
                "PAUSED", "STOPPED"
            ]
        }
    }

    Notification(type, message) {

        switch (type) {

            case 'Logs-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get log!",
                    title: "Console"
                })
        }
    }

    showOutput = (output, classname) => {
        return output.map( (out, index) => {
            return <h5 key={index} className={classname}>{out}</h5>
        } )
    }

    getLog = (job) => {
        const data = {job:{build_id:job.build_id,fullname:job.job.fullname}}
        //---- API. Start Build --------------
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_log', data)

        //Notification
        if (!options) return this.Notification('Logs-Error')

        console.log('Getting logs: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                const stream = response.data;
                stream.on('data', (chunk /* chunk is an ArrayBuffer */) => {
                    console.log(chunk);
                });
                stream.on('end', () => {
                   console.log("end")
                });
                stream.on('error', err => {
                    console.log(err)
                })

            })
            .catch( (err) => {
                console.log("ERROR: ConsoleOutput.jsx: getLog: the try to get log failed: " + JSON.stringify(err))
            })

    }

    getOutput = (job) => {

        const servicename = library.getValueJson(job, this.props.nameconvention.keys, "task_status", "Service")

        if (this.status.process.includes(job.status)) {
            return (
                <React.Fragment>
                    <div className="d-flex flex-row justify-content-between">
                        <h5 className="running">{servicename}</h5>
                    </div>
                    <h5>Status: <pre>{job.status}</pre></h5>
                </React.Fragment>
            )
        }
        if (this.status.success.includes(job.status)) {
            return (
                <React.Fragment>
                    <div className="d-flex flex-row justify-content-between">
                        <h5 className="success">{servicename}</h5>
                    </div>
                    {this.showOutput(job.output, "success")}
                </React.Fragment>
            )
        }
        if (this.status.stoppaused.includes(job.status)) {
            return (
                <React.Fragment>
                    <div className="d-flex flex-row justify-content-between">
                        <h5 className="warning">{servicename}</h5>
                    </div>
                    <h5>Status: <pre className="warning">{job.status}</pre></h5>
                </React.Fragment>
            )
        }
        if (this.status.error.includes(job.status)) {
            return (
                <React.Fragment>
                    <div className="d-flex flex-row justify-content-between">
                        <h5 className="error">{servicename}</h5>
                    </div>
                    <h5>Status: <pre className="error">{job.status}</pre></h5>
                    {this.showOutput(job.output, "error")}
                </React.Fragment>
            )
        }

        return null

    }

    showList = (jobs) => {

        if (jobs instanceof Object) {

            /*let buff = [], element

            for (let i=0; i<output.jobs.length; i++) {

                let job = this.props.jobs.jobs.find( x => x.job_id.split("/")[1] == i+1),
                    servicename = library.getValueJson(job, this.props.nameconvention.keys, "task_status", "Service") || library.getValueJson(job, this.props.nameconvention.keys, "task_status", "fullname"),
                    output = job.output,
                    url_console = job.urlConsole,
                    status = job.status

                element =
                    <div key={i+1} className="console-job d-flex flex-column">
                        <h5 className="success">{servicename}</h5>
                        <h5>{output}</h5>
                        <h5><a href={url_console} target="_blank">{url_console}</a></h5>
                        <h5>Status: {status}</h5>
                    </div>


                buff.push(element)

            }

            return buff*/

            return jobs.jobs.map((job, index) => {

                if (job.url_console !== "null") {
                    return (
                        <div key={index} className="console-job d-flex flex-column" onClick={this.getLog.bind(this, job)}>
                            {this.getOutput(job)}
                            <h5><a href={job.url_console} target="_blank">{job.url_console}</a></h5>
                        </div>
                    )
                } else {
                    return (
                        <div key={index} className="console-job d-flex flex-column">
                            {this.getOutput(job)}
                        </div>
                    )
                }


            })
        }

        if (this.props.task_id && this.props.timer) {
            return (
                    <div className="lds-dual-ring onload"></div>
            )
        }

        return null

    }

    copyConsoleOutput = () => {
        console.log("copy")
        var elm = document.getElementById("console-output");
        // for Internet Explorer

        if(document.body.createTextRange) {
            var range = document.body.createTextRange();
            range.moveToElementText(elm);
            range.select();
            document.execCommand("Copy");
        }
        else if(window.getSelection) {
            // other browsers

            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents(elm);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("Copy");
        }
    }

    showTaskID = () => {
        if (this.props.task_id && this.props.type !== "history") {
            return (
                <pre>{this.props.task_id}</pre>
            )
        }
        return null
    }

    render() {
        return (
            <div className="application console console-jobs-queue d-flex flex-column">
                <div className="default-header-block d-flex flex-row align-items-center justify-content-between">
                    <h5>Console</h5>
                    {this.showTaskID()}
                    <a className="button-copy" onClick={this.copyConsoleOutput.bind()}>
                        <i className="copy outline icon"></i>
                    </a>
                </div>
                <div id="console-output" className="list-item">
                    {this.showList(this.props.jobs)}
                </div>
            </div>
        )

    }
}

function mapStateToProps (state) {
    return {
        nameconvention: state.nameconvention,
        task_id: state.building.task_id,
        timer: state.building.timer,
        api: state.appinfo.api
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(ConsoleOutput)