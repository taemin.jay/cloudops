import React, { Component } from 'react';
import {connect} from "react-redux";
import Status from "../../../Patterns/Status"

class ConsoleJob extends Component {

    constructor () {
        super()
        this.state = {
            active: false
        }
    }

    optionsJob(options) {

        return options.keys.map( (key, index) => {

            const value = options.params[key]

            return (
                <div key={index} className="console-option d-flex flex-column">
                    <h6>{key}:</h6>
                    <h5>{value}</h5>
                </div>
            )

        })

    }

    showButton = () => {

        return this.setState({
            active: !this.state.active
        })

    }

    addToCart = (job) => {

        console.log(job)

    }

    showConsoleJob(job, callback) {

        switch(callback.type) {

            case "history":

               let className = ""

                if (callback.selected === job.job) {
                    className = "active-job"
                }

                return (
                    <div onClick={callback.callbackFun.bind(this, job.job)} onMouseOver={this.showButton} onMouseOut={this.showButton} className={`console-job d-flex flex-row justify-content-between align-items-center ${className}`}>
                        <div className="console-servicename">
                            <h5>{job.servicename}</h5>
                        </div>
                        <div className={`deploy-action d-flex align-items-center justify-content-center active`}>
                            <h6>{job.action}</h6>
                        </div>
                        <div className="console-options d-flex flex-row flex-wrap align-items-center">
                            {this.optionsJob(job.options)}
                        </div>
                        <Status status={job.job.status}/>
                    </div>
                )

            default :

                return (
                    <div className="console-job d-flex flex-row justify-content-between align-items-center">
                        <div className="console-servicename">
                            <h5>{job.servicename}</h5>
                        </div>
                        <div className={`deploy-action d-flex align-items-center justify-content-center active`}>
                            <h6>{job.action}</h6>
                        </div>
                        <div className="console-options d-flex flex-row flex-wrap align-items-center">
                            {this.optionsJob(job.options)}
                        </div>
                        <Status status={job.job.status}/>
                    </div>
                )
        }
    }

    render() {
        return this.showConsoleJob(this.props.job, this.props.callback)

    }
}


function mapStateToProps (state) {
    return {
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(ConsoleJob)
