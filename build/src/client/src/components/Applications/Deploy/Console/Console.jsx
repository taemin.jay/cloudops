import React, { Component } from 'react';
import {connect} from "react-redux";
import ConsoleTasks from './ConsoleTasks'
import ConsoleJobList from './ConsoleJobList'
import ConsoleOutput from './ConsoleOutput'
import library_c from "../../../../assets/js/library_c";
import axios from "axios/index";

class Console extends Component {

    Notification(type, message) {

        switch(type) {

            case 'Console-ERROR':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of the running tasks!",
                    title: "Console"
                })

        }

    }

    componentDidMount() {

        console.log(this.props.task_id)

        if (!this.props.task_id) {

            return console.log("notification: id is not set")

        }

        //---- API. GET Template Service list ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_task_status', [this.props.task_id])

        //Notification
        if (!options) return this.Notification('Console-ERROR')

        console.log('Get services list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                console.log(answer)

                if (answer) {
                    return this.props.updateOutput("UPDATE_OUTPUT", answer.task),
                        this.props.addTaskId("ADD_TASKS_ID", answer.runningTasks)
                }
            })
            .catch( () => {

                console.log("Couldn't get output. Please, check, that Task ID is correct or errors above")
                return false

            })


    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex flex-row flex-wrap">
                    <ConsoleOutput jobs={this.props.output}/>
                    <div className="console-monitoring d-flex flex-column">
                        <div className="console-tasks d-flex flex-column">
                            <ConsoleTasks/>
                        </div>
                        <div className="console-jobs d-flex flex-column">
                            <ConsoleJobList callback={{type:"default"}} jobs={this.props.output}/>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )

    }
}

function mapStateToProps (state) {
    return {
        task_id: state.building.task_id,
        api: state.appinfo.api,
        output: state.building.output
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        updateOutput: (type, output) => {
            dispatch({type: type, output: output})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(Console)
