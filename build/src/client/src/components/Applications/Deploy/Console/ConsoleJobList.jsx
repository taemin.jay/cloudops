import React, { Component } from 'react';
import {connect} from "react-redux";
import library from '../../../../assets/js/library'
import library_c from '../../../../assets/js/library_c'
import ConsoleJob from './ConsoleJob'

class ConsoleJobList extends Component {

    showListJobs(jobs, callback) {

        if (jobs instanceof Object) {

            return jobs.jobs.map((job, index) => {

                const nameconvention = this.props.nameconvention.keys,
                    replace_rules = this.props.rules.params.replace,
                    excludes_rules = this.props.rules.params.excludes,
                    servicename = library.getValueJson(job, nameconvention, "task_status", "Service") || library.getValueJson(job, nameconvention, "task_status", "fullname"),
                    action = library.getValueJson(job, nameconvention, "task_status", "Action"),
                    options = library_c.getServiceDefaultParams(job, nameconvention, "task_status", "params", excludes_rules, replace_rules)

                return <ConsoleJob key={index} callback={callback} job={{servicename: servicename, action:action, options:options, job:job}}/>

            })
        }

        if (this.props.task_id && this.props.timer) {
            return (
                <div className="lds-dual-ring onload"></div>
            )
        }

        return null

    }

    render() {

        return (
            <div className="application console-jobs-queue d-flex flex-column">
                <div className="default-header-block">
                    <h5>Jobs Queue</h5>
                </div>
                <div className="list-item">
                {this.showListJobs(this.props.jobs, this.props.callback)}
                </div>
            </div>
        )

    }
}

function mapStateToProps (state) {
    return {
        nameconvention: state.nameconvention,
        rules: state.rules,
        api: state.appinfo.api,
        task_id: state.building.task_id,
        timer: state.building.timer
    };
}

export default connect(mapStateToProps)(ConsoleJobList)