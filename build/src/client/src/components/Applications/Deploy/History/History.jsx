import React, { Component } from 'react';
import HistoryTasksList from './HistoryTasksList'

import axios from "axios/index";
import {connect} from "react-redux";
import HistoryFullInfo from "./HistoryFullInfo";
import library_c from "../../../../assets/js/library_c";

class History extends Component {

    constructor () {
        super()
        this.state = {
            history_tasks: [],
            history_enable: false,
            history_jobs: 'No data',
            history_loading: true,
        }
    }

    componentDidMount() {
        return this.getTasks()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api) {
            return this.getTasks()
        }
    }

    Notification(type, message) {

        switch (type) {

            case 'HISTORY-error':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of History!",
                    title: "History"
                })

        }
    }

    getTasks() {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Env list --------------
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_tasks')

        //Notification
        if (!options) return this.Notification('HISTORY-error')

        console.log('Get environments from: '+ JSON.stringify(options))

        let answer
        axios(options)

        //Необходимо заменить\перенести\переписать код для того, чтобы привести получаемые данные в соответствующий формат
        //Сейчас форматирование даты идет на стороне web-server
        /*const url = this.props.server + '/jenkins-tool/getTasks'
        let answer

        console.log('Get history from: '+ url)

        axios.get(url)*/
            .then(response => {

                answer = response.data
                answer.reverse()

                if (answer) {
                    return this.setState({
                        history_tasks: answer,
                        history_enable: true,
                        history_loading: false
                    })
                }

                return this.setState({
                    history_tasks: ['No data'],
                    history_enable: false,
                    history_loading: false
                })

            })
            .catch( () => {

                answer = ["Service is not available. Or something went wrong..."]

                this.setState({
                    history_tasks: answer,
                    history_enable: false,
                    history_loading: false
                })

            })


    }

    getJobs(task) {
        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Env list --------------
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_jobs', [task.task_id])

        //Notification
        if (!options) return this.Notification('HISTORY-error')

        console.log('Get jobs list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    return this.setState({
                        history_jobs: {jobs:answer, task:task}
                    })
                }

                return this.setState({
                    history_jobs: "Something went wrong"
                })

            })
            .catch( (err) => {

                console.log(err)
                return this.setState({
                    history_jobs: "Something went wrong"
                })

            })
    }

    showFullOutput = (task) => {

        return this.getJobs(task)

    }


    render() {
        return (
            <React.Fragment>
                <div className="history d-flex flex-row">
                    <div className="application history">
                        <HistoryTasksList history={this.state} callback={this.showFullOutput}/>
                    </div>
                    <div className="application history-console">
                        <HistoryFullInfo fullinfo={this.state.history_jobs}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }


}

function mapStateToProps (state) {
    return {
        server: state.appinfo.server,
        api: state.appinfo.api
    };
}
export default connect(mapStateToProps)(History)