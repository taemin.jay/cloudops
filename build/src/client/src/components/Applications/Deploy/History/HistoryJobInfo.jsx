import React, { Component } from 'react';

class HistoryFullInfo extends Component {

    constructor () {
        super()
        this.state = {
            keys: [
                "job_id",
                "status",
                "username",
                "url_console",
                "output",
                "start_date",
                "update_date",
                "environment",
                "description"
            ]
        }
    }

    showLine(data,key) {

        if (key) {

                return (
                    <React.Fragment>
                        <label>{key}</label>
                        <pre>{data}</pre>
                    </React.Fragment>
                )

            }

        if (data instanceof Object) {

            return (
                <React.Fragment>
                    <pre>{JSON.stringify(data.job, null, 4)}</pre>
                </React.Fragment>
            )

        }

        return (
            <React.Fragment>
                <pre>{data}</pre>
            </React.Fragment>
        )

    }

    showFullInfo(jobinfo,task) {

        if (jobinfo instanceof Object) {

            return this.state.keys.map( (key, index) => {

                return (
                    <div key={index} className="history-job-item-info d-flex flex-row justify-content-between">
                        {this.showLine(JSON.stringify(jobinfo[key] || task[key], null, 4), key)}
                    </div>

                )

            })

        }

        return <h5>{jobinfo}</h5>

    }

    render() {
        return (
            <div className="history-job-info d-flex flex-row flex-wrap align-items-center">
                <div className="history-job-fullinfo d-flex flex-row flex-wrap">
                    {this.showFullInfo(this.props.jobinfo, this.props.task)}
                </div>
                <div className="history-job-fullinfo d-flex flex-row justify-content-between">
                    <div className="history-job-item-json d-flex flex-row justify-content-between">
                        {this.showLine(this.props.jobinfo)}
                    </div>
                </div>
            </div>
        )
    }


}

export default HistoryFullInfo;