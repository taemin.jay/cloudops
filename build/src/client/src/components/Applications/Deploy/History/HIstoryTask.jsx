import React, { Component } from 'react';
import {connect} from "react-redux";
import Status from '../../../Patterns/Status'

class HistoryTask extends Component {

    constructor () {
        super()
        this.state = {
            active: false
        }
    }

    addToCart = (task) => {

        const buff = JSON.parse(JSON.stringify(task))
        let jobs = []

        buff.jobs.forEach(job => {

            delete job.job.params.token
            jobs.push(job.job)

        })

        //Notification
        this.props.notifyMe({
            type: 'info',
            message: `#${task.id} added to Cart.`,
            title: "History"
        })

        return this.props.addToCart(jobs)

    }

    showButton = () => {

        return this.setState({
            active: !this.state.active
        })

    }

    showTask(task) {

        return (
            <div onClick={this.props.callback.bind(this, task)} onMouseOver={this.showButton} onMouseOut={this.showButton} className={`console-task d-flex flex-column justify-content-between align-items-center ${this.props.active ? "active-task" : null}`}>
                <div className="task-short-info d-flex flex-row justify-content-between align-items-center w-100">
                    <div className="task-id"><h4>{task.task_id}</h4></div>
                    <div className="task-status">
                        <Status status={task.status} />
                    </div>
                </div>
                <div className="task-short-info d-flex flex-row justify-content-between align-items-center w-100">
                    <div className="task-desc">{task.description}</div>
                    <div className="task-env">{task.environment}</div>
                </div>
            </div>
        );
    }

    render() {
        return this.showTask(this.props.task)
    }


}

function mapStateToProps (state) {
    return {
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        addToCart: (jobs) => {
            dispatch({type: 'ADD_TO_CART_FROM_HISTORY', jobs: jobs})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(HistoryTask);
