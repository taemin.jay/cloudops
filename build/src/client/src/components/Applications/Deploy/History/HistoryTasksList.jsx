import React, { Component } from 'react';
import HistoryTask from './HIstoryTask'
import {connect} from "react-redux";

class HistoryTasksList extends Component {

    constructor () {
        super()
        this.state = {
            active_task: null,
            start: 0,
            end: 0,
            divHeight: 0
        }

        this.element = React.createRef()

        this.scrollList = {
            divsOnPage: 20,
            delta: 10,
            prevStart: 0,
            prevEnd: 0,
            currentStart: 0,
            currentEnd: 0,
            nextStart: 0,
            nextEnd: 0,
            deltaHeight: 0,
            divHeightList: 0,
            divHeightGrid: 0,
            DeployServiceHeight: 100,
            DeployServiceWidth: 260,
            BlockHeight: 0,
            BlockWidth: 0
        }
    }

    componentDidMount(){
        this.element.current.addEventListener("scroll", this.handleScroll)
        this.preCalculate()
    }

    handleScroll = (e) => {
        const scrollTop = e.target.scrollTop,
            divHeight = this.state.divHeight,
            deltaHeight = this.scrollList.deltaHeight
        let prevStart = this.scrollList.prevStart,
            prevEnd = this.scrollList.prevEnd,
            nextStart = this.scrollList.nextStart,
            nextEnd = this.scrollList.nextEnd

        console.log(scrollTop, deltaHeight, divHeight)
        if (scrollTop + 80 >= deltaHeight + divHeight) {
            console.log("=====> scrollTop >= deltaHeight + divHeight")

            this.calculateSteps(nextStart,nextEnd)

            return this.setState({
                divHeight: divHeight + deltaHeight
            })
        }
        if (scrollTop + 80 < divHeight) {
            console.log("=====> scrollTop <= divHeight")

            this.calculateSteps(prevStart,prevEnd)

            return this.setState({
                divHeight: divHeight - deltaHeight
            })
        }
    }

    preCalculate() {
        const divsOnPage = this.scrollList.divsOnPage,
            currentStart = 0,
            currentEnd = currentStart + divsOnPage

        this.calculateDelta()
        this.calculateSteps(currentStart, currentEnd)
        return this.setState({
            divHeight: 0
        })

    }

    calculateDelta() {

        const width = this.element.current.clientWidth,
            countInWidth = Math.floor(width/this.scrollList.DeployServiceWidth),
            lines = Math.floor(this.scrollList.delta/countInWidth),
            deltaHeight = lines * this.scrollList.DeployServiceHeight
        let delta = lines * countInWidth,
            divsOnPage = this.scrollList.divsOnPage

        divsOnPage = Math.floor(divsOnPage/delta)*delta

        return this.scrollList.deltaHeight = deltaHeight, this.scrollList.delta = delta, this.scrollList.divsOnPage = divsOnPage
    }

    calculateSteps(start,end) {
        const delta = this.scrollList.delta,
            divsOnPage = this.scrollList.divsOnPage,
            currentStart = this.scrollList.currentStart = start,
            currentEnd = this.scrollList.currentEnd = end,
            prevStart = this.scrollList.prevStart = currentStart - delta,
            prevEnd = this.scrollList.prevEnd = currentEnd - delta,
            nextStart = this.scrollList.nextStart = currentStart + delta,
            nextEnd = this.scrollList.nextEnd = nextStart + divsOnPage

        return this.setState({
            start: currentStart,
            end: currentEnd
        })
    }

    showActiveTask = (task) => {

        if (this.state.active_task === task) {
            return this.setState({
                active_task: null
            })
        }
        return this.setState({
            active_task: task
        }), this.props.callback(task)

    }


    /*componentWillReceiveProps(nextProps) {

        if (this.state.timeout_enable) {

            this.state.history_tasks = []

            console.log("HISTORY", nextProps)
            return this.setState({
                timeout_enable: false
            }), this.showItemByTimeout('history_tasks', nextProps.history.history_tasks, 0, 50)

        }

    }

    showItemByTimeout(state, list, index, delay) {

        if (!delay) {

            delay = this.props.application.design.show.delay

        }

        let buff = list

        if (index < list.length) {

            buff = this.state[state]
            buff.push(list[index])
            index++
            setTimeout( () => { this.showItemByTimeout(state, list, index, delay) }, delay );

        }

        return this.setState({
            [state]: buff
        })

    }*/

    showHistoryList(history_tasks, loading, enable, active_task) {

        if (loading) {

            return (<div className="lds-dual-ring onload"></div>)

        }

        if (enable) {

            return history_tasks.map((task, index) => {

                let status = false
                if (active_task === task) {
                    status = true
                }

                return (
                    <HistoryTask key={index} task={task} active={status} callback={this.showActiveTask}/>
                )
            }).slice(this.state.start, this.state.end)

        }

        return history_tasks.map((task, index) => {
            return (
                <div key={index} className="deploy-service-no-data d-flex flex-column">
                    <h5>{task}</h5>
                </div>
            )
        })
    }


    render() {

        const style = {height:this.state.divHeight}

        return (
            <div className="d-flex flex-column align-items-center deploy-service-list" ref={this.element}>
                <div dataheight={this.state.divHeight} style={style}></div>
                {this.showHistoryList(this.props.history.history_tasks, this.props.history.history_loading,this.props.history.history_enable, this.state.active_task)}
            </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        application: state.appinfo
    };
}
export default connect(mapStateToProps)(HistoryTasksList)
