import React, { Component } from 'react';
import ConsoleJobList from '../Console/ConsoleJobList'
import ConsoleOutput from '../Console/ConsoleOutput'
import HistoryJobInfo from './HistoryJobInfo'

class HistoryFullInfo extends Component {

    constructor () {
        super()
        this.state = {
            jobinfo: 'Choose the job'
        }
    }

    showJobInfo = (job) => {

        return this.setState({
            jobinfo: job
        })

    }

    render() {
        return (
            <div className="d-flex flex-column justify-content-center h-100">
                <div className="d-flex flex-row">
                    <ConsoleJobList key="history" callback={{type:"history", callbackFun: this.showJobInfo, selected:this.state.jobinfo}} jobs={this.props.fullinfo.jobs} />
                    <ConsoleOutput jobs={this.props.fullinfo.jobs} type="history"/>
                </div>
                <HistoryJobInfo jobinfo={this.state.jobinfo} task={this.props.fullinfo.task}/>
            </div>
        )
    }


}


export default HistoryFullInfo;