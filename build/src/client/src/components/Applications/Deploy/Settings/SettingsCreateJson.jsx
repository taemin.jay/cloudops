import React, { Component } from 'react';
import ChooseEnv from "../Services/DeployEnv"
import axios from "axios/index";
import {connect} from "react-redux";
import Textarea from "../../../Patterns/Textarea";
import library_c from "../../../../assets/js/library_c";

class SettingsCreateJson extends Component {

    constructor (props) {
        super(props)
        this.state = {
            excludes: props.settings.services.excludes,
            simpleIncludes: props.settings.services.simpleIncludes,
            env: props.settings.services.env,
            disabled: props.settings.services.updating
        }
        this.callbackFun = {
            getExInCludes: this.getExInCludes
        }
    }

    componentDidUpdate(prevProps) {

        if (prevProps.settings.services.updating !== this.props.settings.services.updating) {

            return this.setState({
                disabled: this.props.settings.services.updating
            })

        }

    }

    Notification(type, message) {

        switch (type) {

            case 'Excludes-Includes-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Settings"
                })
            case 'Download':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get Service List for download.",
                    title: "Settings"
                })
            case 'Choose-Env-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: `Please, choose the environment!`,
                    title: "Settings"
                })

            case 'Download-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Download"
                })
        }
    }

    getExInCludes = (env) => {

        //checking
        if (env === "Environment") {

            return this.setState({
                excludes: null,
                simpleIncludes: null
            })

        }

        //---- API. GET Excludes list for choosen Env ----
        let options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'exclude', [env])

        //Notification
        if (!options) return this.Notification('Excludes-Includes-Error', `Can't to get Excludes for ${env}`)

        console.log('Get excludes: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    this.setState({
                        excludes: answer
                    })
                } else {
                    console.log("ERROR: SettingsCreateJson.jsx: getExIncludes: response.data is not available: " + JSON.stringify(response))
                    this.setState({
                        excludes: ["Error. Can't to get Excludes list."]
                    })
                }

            })
            .catch(err => {
                console.log("ERROR: SettingsCreateJson.jsx: getExIncludes: try to fetching excludes list is failed: " + JSON.stringify(err))
                this.setState({
                    excludes: ["Error. Can't to get Excludes list."]
                })
            })

        //---- API. GET Includes list for choosen Env ----
        options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'simple_include')

        //Notification
        if (!options) return this.Notification('Excludes-Includes-Error', `Can't to get Includes for ${env}`)

        console.log('Get includes: '+ JSON.stringify(options))

        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    this.setState({
                        simpleIncludes: answer
                    })
                } else {
                    console.log("ERROR: SettingsCreateJson.jsx: getExIncludes: response.data is not available: " + JSON.stringify(response))
                    this.setState({
                        simpleIncludes: ["Error. Can't to get includes list."]
                    })
                }

            })
            .catch(err => {
                console.log("ERROR: SettingsCreateJson.jsx: getExIncludes: try to fetching includes list is failed: " + JSON.stringify(err))
                this.setState({
                    simpleIncludes: ["Error. Can't to get includes list."]
                })
            })

        return this.setState({
            env
        })

    }

    getTextareaExclInput = (input) => {

        return this.state.excludes.push(input), console.log(this.state.excludes)

    }

    getTextareaInclInput = (input) => {

        return this.state.simpleIncludes.push(input), console.log(this.state.simpleIncludes)

    }

    deleteExcludes = (item) => {
        return this.setState({
            excludes: this.state.excludes.filter(x => x !== item)
        }), console.log(this.state.excludes)
    }

    deleteIncludes = (item) => {
        return this.setState({
            simpleIncludes: this.state.simpleIncludes.filter(x => x !== item)
        }), console.log(this.state.simpleIncludes)
    }

    update = () => {

        const buff = {excludes:this.state.excludes, simpleIncludes:this.state.simpleIncludes, env:this.state.env}
        if (this.state.env === null) {
            //Notification
            return this.Notification('Choose-Env-Warning'), false
        }

        return this.props.showAdditionalBlock("update-servicedb"), this.props.setData(buff)

    }

    download = (env) => {

        if (env === "Environment" || env === null) {
            return false
        }

        //---- API. GET Service list by Env ----
        const options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'cache', {env: env})

        //Notification
        if (!options) return this.Notification('Download')

        console.log('Get service list to download: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data.services

                if (answer) {
                    const hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(answer, null, 4));
                    hiddenElement.target = '_self';
                    hiddenElement.download = env + '.json';
                    hiddenElement.dispatchEvent(new MouseEvent(`click`, {
                        bubbles: true,
                        cancelable: true,
                        view: window
                    }));

                    return true
                }

                console.log("ERROR: SettingsCreateJson.jsx: download: response.data.services is not available: " + JSON.stringify(response))
                return this.Notification('Download-Error', `${env}.json download failed!`), false

            })
            .catch(err => {
                console.log("ERROR: SettingsCreateJson.jsx: download: try to download list of services is failed: " + JSON.stringify(err))
                //Notification
                return this.Notification('Download-Error', `${env}.json download failed!`), false

            })


    }

    render() {
        return (
            <div className={`settings-deploy-block createJS d-flex flex-column ${this.props.className}`}>
                <h4>Update Service List</h4>
                <div className="d-flex flex-row align-items-center">
                    <ChooseEnv key="Settings" type="Settings" callbackFun={this.callbackFun.getExInCludes} />
                    <div className="settings-createJS-options">
                        {this.state.disabled ? <button className="settings-update-btn animate-rotate" disabled><i className='sync alternate icon'></i></button> : <button onClick={this.update.bind(this)} className="settings-update-btn"><i className='sync alternate icon'></i></button>}
                        <button className="settings-update-btn" onClick={this.download.bind(this, this.state.env)}><i className='cloud download icon'></i></button>
                    </div>
                </div>
                <div className="settings-deploy-exincludes d-flex flex-row flex-wrap align-items-baseline">
                    <div className="like-as-textarea  d-flex flex-column">
                        <h5>Excludes</h5>
                        <Textarea data={this.state.excludes} type="excludes" callback={{get:this.getTextareaExclInput,delete:this.deleteExcludes}} disabled={this.state.disabled}/>
                    </div>
                    <div className="like-as-textarea d-flex flex-column">
                        <h5>Includes</h5>
                        <Textarea data={this.state.simpleIncludes} type="includes" callback={{get:this.getTextareaInclInput, delete:this.deleteIncludes}} disabled={this.state.disabled}/>
                    </div>
                    {/*<div className="d-flex flex-column add-new-service">
                        <h5>Add New Service</h5>
                        <SettingsCJAddNewService />
                    </div>*/}
                </div>
            </div>
        )
    }


}

function mapStateToProps (state) {
    return {
        server: state.appinfo.server,
        api: state.appinfo.api,
        settings: state.settings.deploy
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showAdditionalBlock: (type) => {
            dispatch({type: "SHOW_ADDITIONAL_BLOCK", typeDataBlock: type})
        },
        setData: (data) => {
            dispatch({type: 'SET_DATA_ADDITIONAL_BLOCK', data: data})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(SettingsCreateJson)
