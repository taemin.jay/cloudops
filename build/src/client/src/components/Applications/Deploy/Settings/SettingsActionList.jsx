import React, { Component } from 'react';
import {connect} from "react-redux";

class SettingsActionList extends Component {

    constructor () {
        super()
        this.state = {

        }
    }

    enableAction = (action) => {

        return this.props.enableAction(action)

    }

    showActions() {

        return this.props.actions.actions.map( (action, index) => {

            let className = ""

            if (action.disabled) {

                className = "active"

            }

            return (
                <div key={index} onClick={this.enableAction.bind(this, action.name)} name={action.name}
                        className={`deploy-action d-flex align-items-center justify-content-center ${className}`}>
                    <h6>{action.name}</h6>
                </div>
            )

        })

    }


    render() {
        return (
            <div className={`settings-deploy-block d-flex flex-column ${this.props.className}`}>
                <h4>Action List</h4>
                <div className="d-flex flex-row flex-wrap justify-content-around">
                    {this.showActions()}
                </div>
            </div>
        )
    }


}

function mapStateToProps (state) {
    return {
        actions: state.actions
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        enableAction: (action) => {
            dispatch({type: 'ENABLE_DISABLE_ACTION', action: action})
        }
    }))(SettingsActionList)
