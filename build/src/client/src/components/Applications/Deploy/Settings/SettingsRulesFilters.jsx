import React, { Component } from 'react'
import {connect} from "react-redux";
import Input from '../../../Patterns/Input'
import MaterialIcon from 'material-icons-react';

class SettingsRulesFilters extends Component {

    enableFilter = (e, checked, filterName) => {
        return this.props.applyInputValue('CHANGE_FILTER_ENABLE', filterName, null, !checked, null)
    }

    enableRule = (e, checked, idRule, filterName) => {
        return this.props.applyInputValue('CHANGE_RULE_ENABLE', filterName, idRule, !checked, null, null)
    }

    changeRule = (e, idRule, filterName) => {
        return this.props.applyInputValue('CHANGE_RULE', filterName, idRule, null, e.target.value, null)
    }

    applyRegexp = (e, label, idRule, filterName) => {
        return this.props.applyInputValue('CHANGE_VALUE', filterName, idRule, null, null, e.target.value)

    }

    applyPath = (e, label, idRule, filterName) => {
        return this.props.applyInputValue('CHANGE_KEY', filterName, idRule, null, null, e.target.value)
    }

    applyName = (e, label, idRule, filterName) => {
        return this.props.applyInputValue('CHANGE_FILTER_NAME', filterName, null, null, null, e.target.value)
    }

    addNewRule = (filterName) => {
        return this.props.addOneNew('ADD_RULE', filterName)

    }

    addNewFilter = () => {
        return this.props.addOneNew('ADD_FILTER')
    }
    
    showOptions(rules) {
        return rules.map( (rule,index) => {
            return <option key={index} value={rule}>{rule}</option>
        })
    }

    showMultipleRules(rules) {

        return rules.filters.map( (rule, index) => {

            return (
                <div key={index} className="rules d-flex flex-column align-items-center">
                    <div className="braces">
                        <div className="curly">
                            <Input key={index}
                                   type="NO_TAG_LABEL"
                                   className="settings-rules-path"
                                   specifyProps={
                                       {
                                           value: rule.key.toString(),
                                           dataInputKey: rule.id,
                                           dataInputKey2: rules.name
                                       }
                                   }
                                   onChange={this.applyPath}
                            />
                        </div>
                    </div>
                    <Input specifyProps={
                                {
                                    value: rule.disabled,
                                    dataInputKey: rule.id,
                                    dataInputKey2: rules.name
                                }
                            }
                           type="SWITCH_TOGGLE"
                           onChange={this.enableRule}
                    />
                    <div className="select">
                        <select value={rule.rule}
                                onChange={(e) => this.changeRule(e,rule.id,rules.name)}>
                            {this.showOptions(this.props.rules.rules)}
                        </select>
                    </div>
                    <Input type="NO_TAG_LABEL"
                           labels={[""]}
                           specifyProps={
                               {
                                   value: rule.value,
                                   dataInputKey: rule.id,
                                   dataInputKey2: rules.name
                               }
                           }
                           onChange={this.applyRegexp}/>
                    <h4>#{index+1}</h4>
                </div>
            )

        })

    }

    showRules(rules) {

        return rules.filters.map( (rule, index) => {

            return (
                <div key={index} className="settings-deploy-rules d-flex flex-column align-items-baseline">
                    <div className="title d-flex flex-row flex-wrap align-items-center justify-content-between">
                        <Input type="NO_TAG_LABEL"
                               labels={["settings-filter-name"]}
                               specifyProps={
                                   {
                                       value: rule.name,
                                       dataInputKey2: rule.name
                                   }
                               }
                               onChange={this.applyName}
                        />
                        <Input specifyProps={
                                    {
                                        value: rule.disabled,
                                        dataInputKey: rule.name
                                    }
                                }
                               type="SWITCH_TOGGLE"
                               onChange={this.enableFilter}
                        />
                    </div>
                    <div className="settings-options d-flex flex-row flex-wrap">
                        {this.showMultipleRules(rule)}
                        <div key={"add-new-rule"} className="add-rules d-flex flex-column align-items-center">
                            <div className="add-new-rule"
                                 onClick={this.addNewRule.bind(this,rule.name)}>
                                <MaterialIcon icon="add_circle_outline" color="inherit" size="50"/>
                            </div>
                        </div>
                    </div>
                </div>
            )

        })

    }

    render() {
        return (
            <div className={`settings-deploy-block d-flex flex-column ${this.props.className ? this.props.className : ''}`}>
                <h4>Filters</h4>
                <div className="d-flex flex-row flex-wrap justify-content-baseline">
                    {this.showRules(this.props.rules)}
                    <div key={"add-new-filter"} className="add-filter d-flex flex-column align-items-baseline">
                        <div className="add-new-filter"
                             onClick={this.addNewFilter.bind()}>
                            <MaterialIcon icon="add_circle_outline" color="inherit" size="75"/>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        rules: state.rules.servicelist
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        applyInputValue: (type, filterName, idRule, disabled, rule, value) => {
            dispatch({type: type, filterName:filterName,idRule:idRule,disabled:disabled,rule:rule,value:value})
        },
        addOneNew: (type, filterName) => {
            dispatch({type: type, filterName: filterName})
        }
    }))(SettingsRulesFilters)
