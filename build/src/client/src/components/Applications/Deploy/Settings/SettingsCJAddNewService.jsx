import React, {Component} from 'react';
import SettingsCJNewService from "./SettingsCJNewService";

class SettingsCJAddNewService extends Component {

    constructor () {
        super()
        this.state = {
            scale: 1
        }
    }

    deleteItem = () => {

        return this.setState({
            scale: this.state.scale - 1
        })

    }

    showItem(deep) {

        let buff = []

        for (let i=0; i<deep; i++) {
            buff.push(
                <SettingsCJNewService key={i} callback={this.deleteItem}/>
            )
        }

        return buff

    }

    addOneMoreItem = () => {

        return this.setState({
            scale: this.state.scale + 1
        })

    }

    render () {
        return (
            <div className="d-flex flex-row flex-wrap align-items-baseline">
                {this.showItem(this.state.scale)}
                <button onClick={this.addOneMoreItem}>+</button>
            </div>
        )
    }
}

export default SettingsCJAddNewService;