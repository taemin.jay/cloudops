import React, { Component } from 'react';
import {connect} from "react-redux";

class SettingsActionRules extends Component {

    constructor () {
        super()
        this.state = {

        }
    }

    deleteItem = (action,fullname) => {
        console.log(action,fullname)
    }

    showPatterns(action) {

        return action.fullname.map( (fullname, index) => {

            return (
                <a key={index} className={`ui tag label`}
                   onClick={this.deleteItem.bind(this, action, fullname)}>{fullname}</a>
            )

        })

    }

    showRules() {

        return this.props.actions.actions.map( (action, index) => {

            return (
                <div key={index} className="d-flex flex-row align-items-center">
                    <div key={index} name={action.name}
                         className={`deploy-action d-flex flex-row align-items-center justify-content-center`}>
                        <h6>{action.name}</h6>
                    </div>
                    <div className="d-flex flex-column">
                        {this.showPatterns(action)}
                    </div>
                </div>
            )

        })

    }


    render() {
        return (
            <div className={`settings-deploy-block d-flex flex-column ${this.props.className}`}>
                <h4>Action Rules</h4>
                <div className="d-flex flex-row flex-wrap">
                    {this.showRules()}
                </div>
            </div>
        )
    }


}

function mapStateToProps (state) {
    return {
        actions: state.actions
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        enableAction: (action) => {
            dispatch({type: 'ENABLE_DISABLE_ACTION', action: action})
        }
    }))(SettingsActionRules)
