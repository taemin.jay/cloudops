import React, { Component } from 'react';
import SettingsCreateJson from "./SettingsCreateJson"
import SettingsActionList from './SettingsActionList'
import SettingsRulesFilters from './SettingsRulesFilters'
import SettingsActionRules from './SettingsActionRules'

class Settings extends Component {

    render() {
        return (
            <div className="application">
                <div className="d-flex flex-row flex-wrap">
                    <SettingsCreateJson className="flex-2"/>
                    <SettingsActionList className="flex-1"/>
                </div>
                <div className="d-flex flex-row flex-wrap">
                    <SettingsActionRules className="flex-1"/>
                </div>
                <div className="d-flex flex-row flex-wrap">
                    <SettingsRulesFilters className="flex-1"/>
                </div>
            </div>
        )
    }
}

export default Settings;