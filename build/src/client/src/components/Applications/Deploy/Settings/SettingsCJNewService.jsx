import React, {Component} from 'react';

class SettingsCJNewService extends Component {

    constructor () {
        super()
        this.state = {
            params_deep: 1,
            main_deep: 0,
        }
    }

    showParams(deep) {

        let buff = []

        for (let i=0; i<deep; i++) {

            buff.push(<div key={i} className="d-flex flex-row justify-content-around align-items-start">
                <input/>
                :
                <input/>
            </div>)

        }

        return buff

    }

    showItem(deep_params, deep_main) {

        return (
                <div
                    className="d-flex flex-column align-items-baseline justify-content-between">
                    <div className="d-flex flex-row justify-content-around align-items-start">
                        <label>{`{`}</label>
                    </div>
                    <div className="d-flex flex-row justify-content-around align-items-start">
                        <label>fullname:</label>
                        <input/>
                    </div>
                    <div className="d-flex flex-row justify-content-around align-items-start">
                        <label>params:</label>
                        <div className="d-flex flex-column">
                            <label>{`{`}</label>
                            {this.showParams(deep_params)}
                            <button onClick={this.addNewParams}>+</button>
                            <label>{`}`}</label>
                        </div>
                    </div>
                    {this.showParams(deep_main)}
                    <button onClick={this.addNewItem}>+</button>
                    <label>{`}`}</label>
                </div>
            )

    }

    addNewParams = () => {

        return this.setState({
            params_deep: this.state.params_deep + 1
        })

    }

    addNewItem = () => {

        return this.setState({
            main_deep: this.state.main_deep + 1
        })

    }

    render () {
        return (
            <div className="settings-deploy-addService d-flex flex-row flex-wrap align-items-baseline">
                {this.showItem(this.state.params_deep,this.state.main_deep)}
                <button onClick={this.props.callback}>-</button>
            </div>
        )
    }
}

export default SettingsCJNewService;