import React, { Component } from 'react';
import CartJob from "./CartJob";

class CartJobList extends Component {

    constructor () {
        super()
        this.state = {
            start: 0,
            end: 0,
            divHeight: 0
        }

        this.element = React.createRef()

        this.scrollList = {
            divsOnPage: 20,
            delta: 4,
            prevStart: 0,
            prevEnd: 0,
            currentStart: 0,
            currentEnd: 0,
            nextStart: 0,
            nextEnd: 0,
            deltaHeight: 0,
            divHeightList: 0,
            divHeightGrid: 0,
            DeployServiceHeight: 100,
            DeployServiceWidth: 230,
            BlockHeight: 0,
            BlockWidth: 0
        }
    }

    componentDidMount(){
        this.element.current.addEventListener("scroll", this.handleScroll)
        this.preCalculate()
    }

    handleScroll = (e) => {
        const scrollTop = e.target.scrollTop,
            divHeight = this.state.divHeight,
            deltaHeight = this.scrollList.deltaHeight
        let prevStart = this.scrollList.prevStart,
            prevEnd = this.scrollList.prevEnd,
            nextStart = this.scrollList.nextStart,
            nextEnd = this.scrollList.nextEnd

        console.log(scrollTop, deltaHeight, divHeight)
        if (scrollTop + 80 >= deltaHeight + divHeight) {
            console.log("=====> scrollTop >= deltaHeight + divHeight")

            this.calculateSteps(nextStart,nextEnd)

            return this.setState({
                divHeight: divHeight + deltaHeight
            })
        }
        if (scrollTop + 80 < divHeight) {
            console.log("=====> scrollTop <= divHeight")

            this.calculateSteps(prevStart,prevEnd)

            return this.setState({
                divHeight: divHeight - deltaHeight
            })
        }
    }

    preCalculate() {
        const divsOnPage = this.scrollList.divsOnPage,
            currentStart = 0,
            currentEnd = currentStart + divsOnPage

        this.calculateDelta()
        this.calculateSteps(currentStart, currentEnd)
        return this.setState({
            divHeight: 0
        })

    }

    calculateDelta() {

        const width = this.element.current.clientWidth,
            countInWidth = Math.floor(width/this.scrollList.DeployServiceWidth),
            lines = Math.floor(this.scrollList.delta/countInWidth),
            deltaHeight = lines * this.scrollList.DeployServiceHeight
        let delta = lines * countInWidth,
            divsOnPage = this.scrollList.divsOnPage

        divsOnPage = Math.floor(divsOnPage/delta)*delta

        return this.scrollList.deltaHeight = deltaHeight, this.scrollList.delta = delta, this.scrollList.divsOnPage = divsOnPage
    }

    calculateSteps(start,end) {
        const delta = this.scrollList.delta,
            divsOnPage = this.scrollList.divsOnPage,
            currentStart = this.scrollList.currentStart = start,
            currentEnd = this.scrollList.currentEnd = end,
            prevStart = this.scrollList.prevStart = currentStart - delta,
            prevEnd = this.scrollList.prevEnd = currentEnd - delta,
            nextStart = this.scrollList.nextStart = currentStart + delta,
            nextEnd = this.scrollList.nextEnd = nextStart + divsOnPage

        return this.setState({
            start: currentStart,
            end: currentEnd
        })
    }

    showJobList(jobs) {

        if (jobs.length > 0) {

            return jobs.map((job, index) => {

                return (
                    <CartJob key={index} job={job} style_li={this.props.style_li}/>
                )
            }).slice(this.state.start,this.state.end)
        } else {

            return (
                    <h5 className="d-flex align-items-center"></h5>
            )

        }
    }

    render() {

        const style = {height:this.state.divHeight, width:"100%"}

        return (
            <ul ref={this.element} className='cart-ul-job d-flex flex-row flex-wrap align-items-center'>
                <div dataheight={this.state.divHeight} style={style}></div>
                {this.showJobList(this.props.jobs)}
            </ul>
        )
    }


}

export default CartJobList;
