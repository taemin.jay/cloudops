import React, { Component } from 'react';

class CartEnv extends Component {


    showEnv(env) {

        return (
            <div className='cart-env'>
                {env}
            </div>
        );
    }

    render() {
        return this.showEnv(this.props.env)
    }


}

export default CartEnv;

