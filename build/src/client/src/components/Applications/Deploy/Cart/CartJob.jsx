import React, { Component } from 'react';
import library from '../../../../assets/js/library'
import library_c from '../../../../assets/js/library_c'
import {connect} from "react-redux";
import MaterialIcon from 'material-icons-react';

class CartJob extends Component {

    showParams(params) {

        return params.keys.map( (param, index) => {
            return (
                <p key={index}>
                    {params.params[param]}
                </p>
            )
        })
    }

    deleteJob = (job) => {

        return this.props.removeFromCart(job, this.props.nameconvention)

    }

    showJob(job) {

        let style_li
        if (this.props.style_li) {
            style_li = this.props.style_li
        } else {
            style_li = "cart-li-job"
        }

        const servicename = library.getValueJson(job, this.props.nameconvention.keys, "microservice", "Service"),
            action = library.getValueJson(job, this.props.nameconvention.keys, "microservice", "Action"),
            params = library_c.getJobParamsJson(job.params, this.props.rules.params.excludes)

        return (
            <li className={`${style_li} list-item d-flex flex-grow-1 flex-row flex-wrap justify-content-between align-items-center`}>
                <div className='cart-name-param d-flex flex-column justify-content-between'>
                    <div className="cart-name-job">
                        <h5>{servicename}</h5>
                    </div>
                    <div className="cart-param-job d-flex flex-column">
                        {this.showParams(params)}
                    </div>
                </div>
                <div className="cart-action-job d-flex align-items-center justify-content-center active">
                    <h6>{action}</h6>
                </div>
                <div className="cart-delete-job" onClick={() => this.deleteJob(job)}>
                    <MaterialIcon icon="close" color="inherit"/>
                </div>
            </li>
        );
    }

    render() {
        return this.showJob(this.props.job)
    }


}

function mapStateToProps (state) {
    return {
        nameconvention: state.nameconvention,
        rules: state.rules,
        cart: state.cart
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        removeFromCart: (job, nameconvention) => {
            dispatch({type: 'REMOVE_FROM_CART', payload: job, keys: nameconvention})
        }
    }))(CartJob)

