import React, { Component } from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import CartEnv from "./CartEnv";
import CartJobList from "./CartJobList"
import Input from '../../../Patterns/Input'
import axios from "axios/index";
import library_c from "../../../../assets/js/library_c";

class Cart extends Component {

    constructor (props) {
        super(props)
        this.state = {
            showCart: props.showCart,
            value: "",
            error: false
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.showCart !== this.props.showCart) {
            return this.setState({
                showCart: this.props.showCart
            })
        }
    }

    Notification(type, message) {

        switch (type) {

            case 'Start-Task-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to start Task!",
                    title: "Cart"
                })
            case 'Start-Task-Paused':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Worker on PAUSE. Please, resume first.",
                    title: "Cart"
                })

            case 'Start-Task-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: `Cart is empty.`,
                    title: "Cart"
                })

            case 'Cart-Upload-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: `File upload failed!`,
                    title: "Cart"
                })

            case 'TEMPLATE-WARN':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Please, name the template!",
                    title: "Template"
                })

            case 'TEMPLATE-ERROR':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Template"
                })

            case 'TEMPLATE-SUCCESS':
                return this.props.notifyMe({
                    type: 'success',
                    message: message,
                    title: "Template"
                })

        }
    }


    showCart() {

        return this.setState({
            showCart: !this.state.showCart
        })
    }

    onChange = (e) => {

        return this.setState({
            value: e.target.value,
            error: false
        })

    }

    delete_all = () => {

        this.props.removeAllFromCart()

    }

    save_temp = () => {

        if (!this.state.value) {
            return this.setState({
                error: true
            }), this.Notification('TEMPLATE-WARN')
        }

        const cart = this.props.cart.cart
        let buff = [], data

        cart.forEach( item => {

            buff.push({Service: item.params.Service || item.fullname})

        })

        data = {name:this.state.value,count:cart.length,services: JSON.stringify(buff)};

        console.log(data)

        //---- API. GET Template Service list ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_save', data)

        //Notification
        if (!options) return this.Notification('TEMPLATE-ERROR',`Can't to save template "${this.state.value}".`)

        console.log('Save the template: '+ JSON.stringify(options))

        axios(options)
            .then(response => {

                const answer = response.data

                console.log(answer)

                return this.Notification('TEMPLATE-SUCCESS',`Template "${this.state.value}" saved.`), this.setState({
                    value: ""
                })

            })
            .catch(reject => {

                console.log(reject)
                return this.Notification('TEMPLATE-ERROR',`Catch. Can't to save template "${this.state.value}".`)

            })


    }

    start = () => {

        const jobs = this.props.cart.cart,
            environment = this.props.cart.env,
            description = this.state.value

        if (jobs.length <= 0) {
            //Notification
            return this.Notification('Start-Task-Warning')
        }

        let task = {
            environment,
            description,
            jobs
        }

        //---- API. Start Build --------------
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'start', task)

        //Notification
        if (!options) return this.Notification('Start-Task-Error')

        console.log('Starting Task: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {

                    console.log(answer)

                    if (answer.status === "PAUSED") {
                        return this.Notification('Start-Task-Paused')
                    }

                    this.props.removeAllFromCart()
                    this.props.showCartFunc()
                    return this.props.addTaskId("ADD_TASK_ID", answer), this.setState({
                        value: ""
                    })//, this.props.history.push("/console")
                }

                this.Notification('Start-Task-Error')
                return console.log("ERROR: Cart.jsx: start: response.data is not available: "+ JSON.stringify(response))

            })
            .catch( (err) => {
                console.log("ERROR: Cart.jsx: start: try to start task is failed: " + JSON.stringify(err))
            })


    }

    upload = (e) => {

        const file = e.target.files[0],
            reader = new FileReader()

        let data, environment

        reader.readAsText(file)
        reader.onloadend = (e) => {
            try {
                data = JSON.parse(reader.result)
                console.log(data)
                environment = data.environment || data.task[0].fullname.split("/")[2] || data.task[0].params.environment
                return this.props.setEnvToCart(environment), this.props.addUploadedSrvsToCart(data.task)
            } catch (err) {
                console.log("ERROR: Cart.jsx: upload: try to upload task is failed: " + JSON.stringify(err))
                //Notification
                return this.Notification('Cart-Upload-Error')
            }
        }

    }

    upload_choosefile = () => {

        this.refs.fileUploader.click();

    }

    download = () => {

        const environment = this.props.cart.env,
            task = this.props.cart.cart,
            hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify({environment,task},null, 4));
        hiddenElement.target = '_self';
        hiddenElement.download = environment+'.json';
        hiddenElement.dispatchEvent(new MouseEvent(`click`, {bubbles: true, cancelable: true, view: window}));

        return true

    }

    showOptions(options) {

        return options.filter( x => x.disabled).map( (option, index) => {

            if (option.name === "upload") {

                return (
                   <React.Fragment key={index}>
                        <input type="file" style={{display:'none'}} ref="fileUploader" accept="application/json" onChange={this[option.name].bind(this)}/>
                        <button key={index} type="file" title={option.desc} onClick={this[option.name+"_choosefile"].bind(this)} ><i className={option.icon}></i></button>
                   </React.Fragment>
                )

            }

            return (
                <button key={index} type="button" title={option.desc} onClick={this[option.name].bind(this)}><i className={option.icon}></i></button>
            )

        })

    }

    showList(cart) {

        return (
            <div className='cart-list d-flex flex-column align-items-center justify-content-center'>
                <CartEnv env={cart.env}/>
                <CartJobList jobs={cart.cart}/>
            </div>
        );
    }

    render() {
        return (
            <div className={this.state.showCart ? "cart active" : "cart"}>
                <div className="cart-precart-data d-flex flex-column align-items-center">
                    <CartEnv env={this.props.cart.env}/>
                    <Input labels="Ticket/Template"
                           type="NO_TAG_LABEL"
                           onChange={this.onChange}
                           specifyProps={{value:this.state.value}}
                           className={ this.state.error ? "error" : null}
                    />
                    <div className="cart-btn-options">
                        {this.showOptions(this.props.cart.options)}
                    </div>
                </div>
                {this.showList(this.props.cart)}
            </div>
        )
    }


}


function mapStateToProps (state) {
    return {
        cart: state.cart,
        showCart: state.cart.showCart,
        api: state.appinfo.api
    };
}

export default withRouter(connect(mapStateToProps,
    dispatch => ({
        removeAllFromCart: () => {
            dispatch({type: 'REMOVE_ALL_FROM_CART'})
        },
        addTaskId: (type, data) => {
            dispatch({type: type, data: data})
        },
        showAdditionalBlock: (type) => {
            dispatch({type: "SHOW_ADDITIONAL_BLOCK", typeDataBlock: type})
        },
        setEnvToCart: (env) => {
            dispatch({type: 'SET_ENV_TO_CART', payload: env})
        },
        addUploadedSrvsToCart: (services) => {
            dispatch({type: 'ADD_UPLOADED_SRVS_CART', payload: services})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        },
        showCartFunc: () => {
            dispatch({type: "SHOW_CART"})
        },
    }))(Cart))
