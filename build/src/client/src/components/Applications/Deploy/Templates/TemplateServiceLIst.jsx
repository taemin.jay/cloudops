import React, { Component } from 'react';
import TemplateService from './TemplateService'

class TemplatesList extends Component {

    showTemplatesList(service_list) {

        return service_list.map((service, index) => {
            return (
                <TemplateService key={index} service={service}/>
            )
        })

    }

    render() {
        return (
            <div className="d-flex flex-row flex-wrap align-items-center deploy-service-list">
                {this.showTemplatesList(this.props.templates_service_list)}
            </div>
        );
    }


}

export default TemplatesList