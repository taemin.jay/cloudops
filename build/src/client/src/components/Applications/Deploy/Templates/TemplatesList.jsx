import React, { Component } from 'react';
import TemplatesListScroll from './TemplatesListScroll'

class TemplatesList extends Component {

    showHeader = (type) => {

        if (type === "deploy-tool") {
            return (
                <div className="default-header-block">
                    <h5>Templates List</h5>
                </div>
            )
        }

    }

    render() {

        return (
            <div className="application d-flex flex-column align-items-center template-list">
                {this.showHeader(this.props.type)}
                <TemplatesListScroll
                    callbackFun={this.props.callbackFun}
                    removeTemplate={this.props.removeTemplate}
                    type={this.props.type}
                />
            </div>
        );
    }


}

export default TemplatesList
