import React, { Component } from 'react';
import TemplatesList from './TemplatesList'
import {connect} from "react-redux";
import TemplateServiceList from "./TemplateServiceLIst"
import library_c from "../../../../assets/js/library_c"

import axios from "axios/index";

class Templates extends Component {

    constructor () {
        super()
        this.state = {
            templates: ['No data'],
            templates_enable: false,
            templates_service_list: []
        }
    }

    componentWillMount() {

        if (this.props.api) {
            return this.getTemplatesTitleList()
        }

    }

    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api) {
            return this.getTemplatesTitleList()
        }
    }
    showItemByTimeout(state, list, index, delay) {

        if (!delay) {

            delay = this.props.design.show.delay

        }

        let buff = list

        if (index < list.length) {

            buff = this.state[state]
            buff.push(list[index])
            index++
            setTimeout( () => { this.showItemByTimeout(state, list, index, delay) }, delay );

        }

        return this.setState({
            [state]: buff
        })

    }

    Notification(type, message) {

        switch(type) {

            case 'Template-Delete-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Can't to delete template! Create options failed.",
                    title: "Templates"
                })

            case 'Template-Delete-Success':
                return this.props.notifyMe({
                    type: 'success',
                    message: message,
                    title: "Templates"
                })

            case 'Template-Delete-Error':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Templates"
                })


        }

    }

    removeTemplate = (e, id) => {

        e.preventDefault()
        e.stopPropagation()

        //---- API. Delete Template ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_delete', [id])

        //Notification
        if (!options) return this.Notification('Template-Delete-Warning')

        console.log('Delete template: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    //Notification
                    return this.Notification('Template-Delete-Success', `Template #${id} deleted.`), this.getTemplatesTitleList()
                }


                console.log(`ERROR: Template.jsx: removeTemplate: response.data is not available : ` + JSON.stringify(response))
                //Notification
                return this.Notification('Template-Delete-Warning')

            })
            .catch( (err) => {

                console.log(`ERROR: Template.jsx: removeTemplate: try to delete ${id}: ` + JSON.stringify(err))
                //Notification
                return this.Notification('Template-Delete-Error', `Can't to delete #${id}. Check browser console`)

            })


    }

    getTemplatesTitleList() {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Templates list ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_get_titles')

        //Notification
        if (!options) return this.Notification('Templates')

        console.log('Get TemplatesTitle list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer.length > 0) {
                    return this.props.addTemplates(answer, true), this.getTemplateServicesList(answer[0].id)
                }

                console.log("ERROR: TemplatesList.jsx: componentDidMount: response.data is not available: "+ JSON.stringify(response))
                return this.props.addTemplates(["no data"], false)

            })
            .catch( (err) => {

                console.log("ERROR: TemplatesList.jsx: componentDidMount: try to fetching templates list is failed: " + JSON.stringify(err))
                return this.props.addTemplates(["...service is no available"], false)

            })

    }

    getTemplateServicesList = (id) => {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Template Service list ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_get_srvs', [id])

        //Notification
        if (!options) return this.Notification('Templates')

        console.log('Get TemplateServices list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                console.log(response)

                if (answer) {
                    return this.setState({
                        templates_service_list: []
                    }), this.showItemByTimeout('templates_service_list', answer, 0, 50)
                }

                return this.setState({
                    templates_service_list: []
                })

            })
            .catch( (err) => {
                console.log("ERROR: Templates.jsx: getTemplateServicesList: try to fetching templates list is failed: " + JSON.stringify(err))
                return false
            })



        this.props.changeTemplateID(id)

    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex flex-row">
                    <div className="templates">
                        <TemplatesList callbackFun={this.getTemplateServicesList} removeTemplate={this.removeTemplate} />
                    </div>
                    <div className="application template-service-list">
                        <TemplateServiceList templates_service_list={this.state.templates_service_list}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }


}

function mapStateToProps (state) {
    return {
        design: state.appinfo.design,
        api: state.appinfo.api,
        templates: state.templates
    }
}
export default connect(mapStateToProps,
    dispatch => ({
        addTemplates: (templates, enable) => {
            dispatch({type: 'ADD_TEMPLATES', templates: templates, enable: enable})
        },
        changeTemplateID: (id) => {
            dispatch({type: 'CHANGE_TEMPLATE_ID', id: id})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(Templates)