import React, { Component } from 'react';
import {connect} from "react-redux";

class Template extends Component {

    showList = (template, type) => {

        let active = false,
            style

        switch(type) {

            case "deploy-tool":

                if (this.props.templates.search_template_ids.includes(template.id)) {

                    active = true

                }

                style = {minHeight:"50px"}

                return (
                    <div style={style} className={`template-task-block list-item d-flex flex-column justify-content-between align-items-center ${ active ? 'active' : null}`}
                         onClick={this.props.callbackFun.bind(this, template.id)}>
                        <h5>{template.name}</h5>
                    </div>
                );

            default:

                if (this.props.templates.template_id === template.id) {

                    active = true

                }

                style = {minHeight:"135px"}

                return (
                    <div style={style} className={`template-task-block list-item d-flex flex-column justify-content-between align-items-center ${ active ? 'active' : null}`}
                         onClick={this.props.callbackFun.bind(this, template.id)}>
                        <h5>{template.name}</h5>
                        <h4>{template.count}</h4>
                        <button type="button" onClick={(e) => this.props.removeTemplate(e, template.id)}><i
                            className="trash alternate outline icon"></i></button>
                    </div>
                );
        }
    }

    render() {
        return this.showList(this.props.template, this.props.type)
    }


}

function mapStateToProps (state) {
    return {
        templates: state.templates
    };
}

export default connect(mapStateToProps)(Template)
