import React, { Component } from 'react';
import Template from './Template'
import {connect} from 'react-redux'
import axios from 'axios/index';
import library_c from "../../../../assets/js/library_c";


class TemplatesListScroll extends Component {

    constructor () {
        super()
        this.state = {
            templates: [],
            start: 0,
            end: 15,
            divHeight: 0
        }

        this.element = React.createRef()

        this.scrollList = {
            divsOnPage: 15,
            delta: 5,
            prevStart: 0,
            prevEnd: 0,
            currentStart: 0,
            currentEnd: 0,
            nextStart: 0,
            nextEnd: 0,
            deltaHeight: 0,
            divHeightList: 0,
            divHeightGrid: 0,
            DeployServiceHeight_deploytool: 60,
            DeployServiceHeight: 145,
            DeployServiceWidth: 200,
            BlockHeight: 0,
            BlockWidth: 0
        }
    }

    componentWillMount() {

        if (this.props.api) {
            return this.getTemplatesList()
        }

    }

    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api) {
            return this.getTemplatesList()
        }
    }

    componentDidMount(){
        this.element.current.addEventListener("scroll", this.handleScroll, false)
    }

    handleScroll = (e) => {
        const scrollTop = e.target.scrollTop,
            divHeight = this.state.divHeight,
            deltaHeight = this.scrollList.deltaHeight
        let prevStart = this.scrollList.prevStart,
            prevEnd = this.scrollList.prevEnd,
            nextStart = this.scrollList.nextStart,
            nextEnd = this.scrollList.nextEnd

        console.log(scrollTop, deltaHeight, divHeight)
        if (scrollTop + 80 >= deltaHeight + divHeight) {
            console.log("=====> scrollTop >= deltaHeight + divHeight")

            this.calculateSteps(nextStart,nextEnd)

            return this.setState({
                divHeight: divHeight + deltaHeight
            })
        }
        if (scrollTop + 80 < divHeight) {
            console.log("=====> scrollTop <= divHeight")

            this.calculateSteps(prevStart,prevEnd)

            return this.setState({
                divHeight: divHeight - deltaHeight
            })
        }
    }

    preCalculate() {
        const divsOnPage = this.scrollList.divsOnPage,
            currentStart = 0,
            currentEnd = currentStart + divsOnPage

        this.calculateDelta()
        this.calculateSteps(currentStart, currentEnd)
        return this.setState({
            divHeight: 0
        })

    }

    calculateDelta() {

        const width = this.element.current.clientWidth,
            countInWidth = Math.floor(width/this.scrollList.DeployServiceWidth),
            lines = Math.floor(this.scrollList.delta/countInWidth),
            deltaHeight = this.props.type === "deploy-tool" ? lines * this.scrollList.DeployServiceHeight_deploytool : lines * this.scrollList.DeployServiceHeight
        let delta = lines * countInWidth,
            divsOnPage = this.scrollList.divsOnPage

        console.log(width, countInWidth, lines, deltaHeight)

        divsOnPage = Math.floor(divsOnPage/delta)*delta

        return this.scrollList.deltaHeight = deltaHeight, this.scrollList.delta = delta, this.scrollList.divsOnPage = divsOnPage
    }

    calculateSteps(start,end) {
        const delta = this.scrollList.delta,
            divsOnPage = this.scrollList.divsOnPage,
            currentStart = this.scrollList.currentStart = start,
            currentEnd = this.scrollList.currentEnd = end,
            prevStart = this.scrollList.prevStart = currentStart - delta,
            prevEnd = this.scrollList.prevEnd = currentEnd - delta,
            nextStart = this.scrollList.nextStart = currentStart + delta,
            nextEnd = this.scrollList.nextEnd = nextStart + divsOnPage

        return this.setState({
            start: currentStart,
            end: currentEnd
        })
    }

    Notification(type, message) {

        switch(type) {

            case 'Templates':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of Templates!",
                    title: "Templates"
                })

        }

    }


    /*showItemByTimeout(state, list, index, delay) {

        if (!delay) {

            delay = this.props.design.show.delay

        }

        let buff = list

        if (index < list.length) {

            buff = this.state[state]
            buff.push(list[index])
            index++
            setTimeout( () => { this.showItemByTimeout(state, list, index, delay) }, delay );

        }

        return this.setState({
            [state]: buff
        })

    }*/

    getTemplatesList() {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Templates list ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_get_titles')

        //Notification
        if (!options) return this.Notification('Templates')

        console.log('Get list of templates from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer.length > 0) {

                    return this.setState({
                        templates: answer
                    }), this.props.addTemplates(answer, true), this.preCalculate()
                }

                console.log("ERROR: TemplatesList.jsx: componentDidMount: response.data is not available: "+ JSON.stringify(response))
                return this.props.addTemplates(["no data"], false)

            })
            .catch( (err) => {

                console.log("ERROR: TemplatesList.jsx: componentDidMount: try to fetching templates list is failed: " + JSON.stringify(err))
                return this.props.addTemplates(["...service is no available"], false)

            })

    }

    showTemplatesList = (templates, enable, type) => {

        if (enable) {

            return templates.map( (template,index) => {

                return (
                    <Template key={index}
                              template={template}
                              callbackFun={this.props.callbackFun}
                              removeTemplate={this.props.removeTemplate}
                              getTemplatesList={this.getTemplatesList}
                              type={type}/>
                )
            }).splice(this.state.start, this.state.end)

        }
        return templates.map((template, index) => {
            return (
                <div key={index} className="deploy-service-no-data d-flex flex-column">
                    <h5>{template}</h5>
                </div>
            )
        })

    }

    render() {

        const style = {minHeight:this.state.divHeight}

        return (
                <div className="list-item d-flex flex-column" ref={this.element} >
                    <div dataheight={this.state.divHeight} style={style}></div>
                    {this.showTemplatesList(this.props.templates, this.props.templates_enable, this.props.type)}
                </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        templates: state.templates.templates,
        templates_enable: state.templates.templates_enable,
        api: state.appinfo.api,
        design: state.appinfo.design
    }
}
export default connect(mapStateToProps,
    dispatch => ({
        addTemplates: (templates, enable) => {
            dispatch({type: 'ADD_TEMPLATES', templates: templates, enable: enable})
        }
    }))(TemplatesListScroll)

