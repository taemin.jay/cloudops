import React, { Component } from 'react';
import {connect} from "react-redux";

class DeployFilters extends Component {

    constructor (props) {
        super(props)
        this.state = {
            filterName: props.filters[0].name || '',
        }
    }

    filteringSrvsList = (filter) => {

        this.setState({
            filterName: filter.name
        })

        return this.props.callbackFun.filteringSrvsList(filter)

    }

    showFilters(filters) {

        if (filters.length > 0) {

            return filters.filter(x => x.disabled).map((filter, index) => {

                return (
                    <a key={index}
                       onClick={this.filteringSrvsList.bind(this, filter)}
                       className={this.state.filterName === filter.name ? 'active' : ''}
                    >{filter.name}</a>
                )
            })
        }

        return null

    }


    render() {

        return (
                <div className="deploy-filters">
                    {this.showFilters(this.props.filters)}
                </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        filters: state.rules.servicelist.filters
    };
}

export default connect(mapStateToProps)(DeployFilters);