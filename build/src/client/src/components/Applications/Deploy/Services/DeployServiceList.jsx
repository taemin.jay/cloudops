import React, { Component } from 'react';
import DeployService from './DeployService'
import {connect} from "react-redux"
import library_c from '../../../../assets/js/library_c'

class DeployServiceList extends Component {

    constructor (props) {
        super()
        this.state = {
            selected_action_choosen: false,
            selected_action: null,
            founded_srvs: [],
            ids_founded_srvs: [],
            selected_srvs: [],
            ids_selected_srvs: [],
            ctrlA: false,
            ctrlA_searching: false,
            shift: false,
            shift_start: null,

            tableView: props.tableView,

            start: 0,
            end: 0,
            divHeight: 0
        }

        this.element = React.createRef()

        this.scrollList = {
            divsOnPage: 30,
            delta: 10,
            prevStart: 0,
            prevEnd: 0,
            currentStart: 0,
            currentEnd: 0,
            nextStart: 0,
            nextEnd: 0,
            deltaHeight: 0,
            divHeightList: 0,
            divHeightGrid: 0,
            list_DeployServiceHeight: 85,
            list_DeployServiceWidth: 696,
            grid_DeployServiceHeight: 270,
            grid_DeployServiceWidth: 215,
            BlockHeight: 0,
            BlockWidth: 0
        }
    }

    componentDidMount(){
        window.addEventListener("keydown", this.detectKeyFunction, false)
        window.addEventListener("keyup", this.checkingKeyFunction, false)

        this.element.current.addEventListener("scroll", this.handleScroll)

        this.preCalculate()

    }

    componentWillUnmount(){
        window.removeEventListener("keydown", this.detectKeyFunction, false)
        window.addEventListener("keyup", this.checkingKeyFunction, false)
        return this.props.cleanSearch()
    }

    componentDidUpdate(prevProps) {

        if (prevProps.tableView !== this.props.tableView) {
            this.state.tableView = this.props.tableView
            this.preCalculate()
        }
        if (prevProps.deploy.services !== this.props.deploy.services) {
            this.preCalculate()
        }

    }

    detectKeyFunction = (e) => {

        if ((e.ctrlKey || e.metaKey) && e.keyCode === 65) {

            return this.setState({
                ctrlA: true
            }), this.selectingServices()

        } else if (e.shiftKey) {

            console.log(e.shiftKey)

            return this.setState({
                shift: e.shiftKey
            })
        }
    }

    checkingKeyFunction = (e) => {

        if (!e.shiftKey) {

            return this.setState({
                shift: e.shiftKey,
                shift_start: null
            })
        }

    }

    //-----------------------------------------
    // Optimization view of long list in DOM --
    //-----------------------------------------

    handleScroll = (e) => {
        const scrollTop = e.target.scrollTop,
            divHeight = this.state.divHeight,
            deltaHeight = this.scrollList.deltaHeight
        let prevStart = this.scrollList.prevStart,
            prevEnd = this.scrollList.prevEnd,
            nextStart = this.scrollList.nextStart,
            nextEnd = this.scrollList.nextEnd

        console.log(scrollTop, deltaHeight, divHeight)
        if (scrollTop + 80 >= deltaHeight + divHeight) {
            console.log("=====> scrollTop >= deltaHeight + divHeight")

            this.calculateSteps(nextStart,nextEnd)

            return this.setState({
                divHeight: divHeight + deltaHeight
            })
        }
        if (scrollTop + 80 < divHeight) {
            console.log("=====> scrollTop <= divHeight")

            this.calculateSteps(prevStart,prevEnd)

            return this.setState({
                divHeight: divHeight - deltaHeight
            })
        }
    }

    preCalculate() {
        const divsOnPage = this.scrollList.divsOnPage,
            currentStart = 0,
            currentEnd = currentStart + divsOnPage

        this.calculateDelta()
        this.calculateSteps(currentStart, currentEnd)
        return this.setState({
            divHeight: 0
        })

    }

    calculateDelta() {

        const width = this.element.current.clientWidth,
            tableView = this.state.tableView,
            countInWidth = Math.floor(width/this.scrollList[tableView+"_DeployServiceWidth"]),
            lines = Math.floor(this.scrollList.delta/countInWidth),
            deltaHeight = lines * this.scrollList[tableView+"_DeployServiceHeight"]
        let delta = lines * countInWidth,
            divsOnPage = this.scrollList.divsOnPage

        console.log(width, countInWidth, lines, deltaHeight)

        divsOnPage = Math.floor(divsOnPage/delta)*delta

        return this.scrollList.deltaHeight = deltaHeight, this.scrollList.delta = delta, this.scrollList.divsOnPage = divsOnPage
    }

    calculateSteps(start,end) {
        const delta = this.scrollList.delta,
            divsOnPage = this.scrollList.divsOnPage,
            currentStart = this.scrollList.currentStart = start,
            currentEnd = this.scrollList.currentEnd = end,
            prevStart = this.scrollList.prevStart = currentStart - delta,
            prevEnd = this.scrollList.prevEnd = currentEnd - delta,
            nextStart = this.scrollList.nextStart = currentStart + delta,
            nextEnd = this.scrollList.nextEnd = nextStart + divsOnPage
        console.log("prevStart:"+prevStart,"prevEnd:"+prevEnd,"currentStart:"+currentStart,"currentEnd:"+currentEnd,
            "nextStart:"+nextStart,"nextEnd:"+nextEnd)

        return this.setState({
            start: currentStart,
            end: currentEnd
        })
    }

    //------------------------------------------
    // END.Optimization view of long list in DOM
    //------------------------------------------

    selectingServices = (action_for_selecting_srv, service, deploySrv_id) => {

        let ss = this.state.selected_srvs,
            i_ss = this.state.ids_selected_srvs,
            key

        console.log("shift:",this.state.shift, "ctrlA:",this.state.ctrlA, action_for_selecting_srv)
        if (this.state.shift) {
            key = "SHIFT_SRVS"
        } else if (this.state.ctrlA) {
            key = "CTRLA_SRVS"
        } else {
            key = "SINGLE_SRV"
        }

        switch(key) {

            case "CTRLA_SRVS":

                console.log("CTRLA_SRVS && SEARCHING")
                console.log(this.state.ctrlA, this.state.ctrlA_searching)

                if (this.state.ctrlA_searching) {

                    console.log("------------- Remove Selected Srvs -------------")
                    console.log("Count founded_srvs:",this.state.founded_srvs.length)
                    this.state.founded_srvs.forEach( service => {
                        ss = ss.filter(x => x === service)
                        i_ss = i_ss.filter( x => x === service.id)
                    })

                } else {

                    console.log("--------------- Add Selected Srvs ---------------")
                    console.log("Count founded_srvs:",this.state.founded_srvs.length)
                    ss = ss.concat(this.state.founded_srvs)
                    i_ss = i_ss.concat(this.state.ids_founded_srvs)

                }

                return this.setState({
                    ctrlA: !this.state.ctrlA,
                    ctrlA_searching: !this.state.ctrlA_searching,
                    selected_srvs: ss,
                    ids_selected_srvs: i_ss
                })


            case "SINGLE_SRV":

                console.log("SINGLE_SRV")

                if (action_for_selecting_srv === "remove") {

                    console.log("----------- Remove single Selected Srv -----------")
                    console.log(service)

                    ss = ss.filter( x => x !== service)
                    i_ss = i_ss.filter( x => x !== deploySrv_id)

                    return this.setState({
                        selected_srvs: ss,
                        ids_selected_srvs: i_ss,
                        shift_start: deploySrv_id,
                        ctrlA: false,
                    })

                } else if (action_for_selecting_srv === "add") {

                    console.log("----------- Add single Selected Srv -----------")

                    ss = ss.concat(service)

                    i_ss = i_ss.concat(deploySrv_id)

                    return this.setState({
                        selected_srvs: ss,
                        ids_selected_srvs: i_ss,
                        shift_start: deploySrv_id
                    })
                }

            case "SHIFT_SRVS":

                console.log("SHIFT_SRVS")

                let start = deploySrv_id,
                    end = null, buff

                if (this.state.shift_start === null) {

                    start = deploySrv_id

                } else {

                    start = this.state.shift_start
                    end = deploySrv_id

                }

                //sort
                if (end !== null && start !== null && end<start) {

                    buff = start
                    start = end
                    end = buff

                }

                if (action_for_selecting_srv === "remove") {

                    console.log("----------- Remove Selected Srv -----------")

                    for (let i=start; i<=end; i++) {
                        i_ss = i_ss.filter( x => x !== i)
                        ss = ss.filter( x => x !== this.props.deploy.services[i])
                    }

                } else if (action_for_selecting_srv === "add") {

                    console.log("----------- Add Selected Srv -----------")

                    if (end !== null) {
                        for (let i = start; i <= end; i++) {
                            if (!i_ss.includes(i)) {
                                ss = ss.concat(this.props.deploy.services[i])
                                i_ss = i_ss.concat(i)
                            }
                        }
                    } else {
                        if (!i_ss.includes(start)) {
                            ss = ss.concat(this.props.deploy.services[start])
                            i_ss = i_ss.concat(start)

                        }
                    }

                }

                return this.setState({
                    selected_srvs: ss,
                    ids_selected_srvs: i_ss,
                    shift_start: start
                })

            default:
                return false
        }


    }


    addSelectedSrvsToCart = (action) => {

        //true - если action у всех selected_srvs одинаковый, false - если хотя бы у одного нет.
        //для переключения экшана и вновь добавленного сервиса к списку выделенных
        const checkAction = library_c.checkSrvsAction(this.state.selected_srvs, this.props.cart.cart, this.props.nameconvention, action)

        console.log("Check that all the selected services are have different action: "+checkAction)

        if (this.state.selected_action_choosen && this.state.selected_action === action && checkAction) {

            console.log("------ removeSelectedSrvsFromCart ------")

            this.props.removeSelectedSrvsFromCart(this.state.selected_srvs, this.props.nameconvention)

            this.setState({
                selected_action_choosen: false
            })

        } else if (this.state.selected_action_choosen && this.state.selected_action !== action && checkAction) {

            console.log("------ changeActionSelectedSrvsInCart ------")

            console.log(this.state.selected_srvs)

            this.props.changeActionSelectedSrvsInCart(this.state.selected_srvs, this.props.nameconvention, action)

            this.setState({
                selected_action_choosen: true,
                selected_action: action
            })

        } else {

            console.log("--------- addSelectedSrvsToCart ---------")

            console.log(this.state.selected_srvs)

            this.props.addSelectedSrvsToCart(this.state.selected_srvs, this.props.nameconvention, action, this.props.rules)

            this.setState({
                selected_action_choosen: true,
                selected_action: action
            })

        }

        return this.setState({
            selected: false
        })

    }

    ifSearchIfTemplatesFind(services) {
        const search = this.props.searching.search,
            regexp = new RegExp(search, 'i'),
            templates = this.props.templates,
            founded_srvs = [],
            ids_founded_srvs = []

        if (services[0].params) {
            if (templates.length > 0) {
                services = services.filter(x => {return templates.includes(x.params.Service) || templates.includes(x.fullname)})
            }
            services = services.filter(x => {return regexp.test(x.params.Service) || regexp.test(x.fullname)})
        }

        this.state.founded_srvs = services
        this.state.ids_founded_srvs = Array.from(Array(services.length).keys())

        return services

    }

    showServiceList(services, services_enable, loading) {

        services = this.ifSearchIfTemplatesFind(services)

        if (loading) {

            return <div className="lds-dual-ring onload"></div>

        }

        if (services_enable) {

            return services.map( (service, index) => {
                return <DeployService key={services.length + index}
                                      id={index}
                                      service={service}
                                      ids_selected_srvs={this.state.ids_selected_srvs}
                                      selected_action={this.state.selected_action}
                                      selected_action_choosen={this.state.selected_action_choosen}
                                      fun_selectingServices={this.selectingServices}
                                      fun_addToCartSelected={this.addSelectedSrvsToCart}
                />
            }).slice(this.scrollList.currentStart, this.scrollList.currentEnd)

        }

        return services.map((service, index) => {
            return (
                <div key={index} className="deploy-service-no-data d-flex flex-column">
                    <h5>{services}</h5>
                </div>
            )
        })

    }

    showVisual() {

        const tableView = this.state.tableView,
            services = this.props.deploy.services,
            isServices = this.props.deploy.services_enable,
            isLoading = this.props.deploy.loading

        switch(tableView) {

            case "grid":
                return (
                    <div className={`d-flex flex-row flex-wrap deploy-service-list ${tableView}`} onScroll={(e) => this.handleScroll(e)}>
                        {this.showServiceList(services, isServices, isLoading)}
                    </div>
                )

            case "list":

                return (
                    <div className={`d-flex flex-column align-items-center deploy-service-list ${tableView}`}>
                        {this.showServiceList(services, isServices, isLoading)}
                    </div>
                )

        }

    }

    render() {

        const style = {height:this.state.divHeight}

        return (
            <div className="application service-list" ref={this.element}>
                <div dataheight={this.state.divHeight} style={style}></div>
                {this.showVisual()}
            </div>
        )
    }

}

export default connect(
    state => ({
        searching: state.searching,
        cart: state.cart,
        nameconvention: state.nameconvention,
        actions: state.actions,
        rules: state.rules,
        templates: state.templates.searchTemplateServices,
        tableView: state.options.view
    }),
    dispatch => ({
        changeActionSelectedSrvsInCart: (services, nameconvention, action) => {
            dispatch({type: 'CHANGE_ACTION_SELECTED_SRVS_CART', payload: services, keys: nameconvention, action: action})
        },
        addSelectedSrvsToCart: (services, nameconvention, action, rules) => {
            dispatch({type: 'ADD_SELECTED_SRVS_CART', payload: services, keys: nameconvention, action: action, rules: rules})
        },
        removeSelectedSrvsFromCart: (services, nameconvention) => {
            dispatch({type: 'REMOVE_SELECTED_FROM_CART', payload: services, keys: nameconvention})
        },
        cleanSearch: () => {
            dispatch({type: "CLEAN_SEARCH"})
        }
    }))(DeployServiceList);
