import React, { Component } from 'react';
import DeployServiceAction from './DeployServiceAction'
import DeployServiceName from './DeployServiceName'
import DeployServiceParams from './DeployServiceParams'
import {connect} from "react-redux";

class DeployService extends Component {

    constructor (props) {
        super(props)
        this.state = {
            selected: false, //selected service
            action_choosen: false,
            action: null,

            viewTable: props.tableView,

        }
    }

    componentDidMount() {

        const ids_selected_srvs = this.props.ids_selected_srvs
        let choosen_action = false,
            action = false,
            tech = []

        if (ids_selected_srvs.includes(this.props.id)) {

            this.setState({
                selected: true,
                action: this.props.selected_action,
                action_choosen: this.props.selected_action_choosen
            })

        } else {

            this.setState({
                selected: false,
            })

        }

        this.props.cart.cart.forEach(cart => {

            if (cart.params.Service) {
                tech.push(cart.params.Service)
            } else {
                tech.push(cart.fullname)
            }
        })

        if (tech.indexOf(this.props.service.params.Service) !== -1) {
            choosen_action = this.props.cart.cart.find(x => x.params.Service === this.props.service.params.Service).params.Action
            action = true
        } else if (tech.indexOf(this.props.service.fullname) !== -1) {
            choosen_action = this.props.cart.cart.find(x => x.fullname === this.props.service.fullname).params.Action
            action = true
        }

        this.setState({
            action_choosen: action,
            action: choosen_action
        })
    }

    componentDidUpdate(prevProps) {

        if (prevProps.ids_selected_srvs !== this.props.ids_selected_srvs) {

            const ids_selected_srvs = this.props.ids_selected_srvs

            if (ids_selected_srvs.includes(this.props.id)) {

                this.setState({
                    selected: true,
                    action: this.props.selected_action,
                    action_choosen: this.props.selected_action_choosen
                })

            } else {

                this.setState({
                    selected: false,
                })

            }
        }

        if (this.props.tableView !== prevProps.tableView) {
            this.setState({
                viewTable: this.props.tableView
            })
        }

    }

    chooseAction = (e, service, action) => {

        console.log(this.state.action)

        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();

        if (this.state.selected) {

            console.log("--------- Class Selected Services ---------")

            this.state.action_choosen = true,
            this.state.action = action

            return console.log(this.state),this.props.fun_addToCartSelected(action)

        } else {

            console.log("----------- Class Single Service -----------")

            console.log(this.state)

            if (this.state.action_choosen && this.state.action === action) {

                console.log("Remove")

                this.props.removeFromCart(service, this.props.nameconvention)

                return this.setState({
                    action_choosen: false,
                    action: null
                })

            } else if (this.state.action_choosen && this.state.action !== action) {

                this.props.changeActionInCart(service, this.props.nameconvention, action)

                return this.setState({
                    action_choosen: true,
                    action: action
                })

            } else {

                this.props.addToCart(service, this.props.nameconvention, action, this.props.rules)

                return this.setState({
                    action_choosen: true,
                    action: action
                })

            }

        }

    }

    selectingService = (service, id) => {

        if (this.state.selected) {
            this.props.fun_selectingServices("remove", service, id)
        } else {
            this.props.fun_selectingServices("add", service, id)
        }

    }


    render() {

        const service = this.props.service,
            selected = this.state.selected,
            id = this.props.id,
            viewTable = this.state.viewTable

        let active = false

        if (selected) {

            active = true

        }

        switch (viewTable) {
            case "grid":
                return (
                    <div className={`deploy-service-block ${viewTable} list-item d-flex flex-column justify-content-between align-items-center ${ active ? 'active' : null}`} onClick={this.selectingService.bind(this,service, id)}>
                        <DeployServiceName service={service}/>
                        <DeployServiceAction service={service} chooseAction={this.chooseAction}/>
                        <DeployServiceParams service={service}/>
                    </div>
                )

            case "list":
                return (
                    <div className={`deploy-service-block ${viewTable} d-flex flex-raw flex-wrap align-items-center ${ active ? 'active' : ''}`} onClick={this.selectingService.bind(this,service, id)}>
                        <DeployServiceName service={service}/>
                        <DeployServiceAction service={service} chooseAction={this.chooseAction}/>
                        <DeployServiceParams service={service}/>
                    </div>
                )
        }
    }

}


export default connect(
    state => ({
        cart: state.cart,
        nameconvention: state.nameconvention,
        actions: state.actions,
        rules: state.rules,
        tableView: state.options.view
    }),
    dispatch => ({
        addToCart: (service, nameconvention, action, rules) => {
            dispatch({type: 'ADD_TO_CART', payload: service, keys: nameconvention, action: action, rules: rules})
        },
        removeFromCart: (service, nameconvention) => {
            dispatch({type: 'REMOVE_FROM_CART', payload: service, keys: nameconvention})
        },
        changeActionInCart: (service, nameconvention, action) => {
            dispatch({type: 'CHANGE_ACTION_CART', payload: service, keys: nameconvention, action: action})
        }
    }))(DeployService);