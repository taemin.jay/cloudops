import React, { Component } from 'react';
import TemplatesList from "../Templates/TemplatesList"
import ConsoleTasks from "../Console/ConsoleTasks"

class DeployAddBlock extends Component {

    render() {
        return (
           <div className="deploy-service-list d-flex flex-column">
                <TemplatesList callbackFun={this.props.callbackFun} type="deploy-tool"/>
                <ConsoleTasks type="deploy-tool"/>
            </div>
        );
    }

}

export default DeployAddBlock;

