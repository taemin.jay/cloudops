import React, { Component } from 'react';
import Input from '../../../Patterns/Input'
import library from '../../../../assets/js/library_c'
import {connect} from "react-redux";

class DeployServiceParams extends Component {

    onChange = (e, label) => {

        return this.props.changeParamsInCart(e.target.value, label, this.props.service, this.props.nameconvention)

    }

    showParams(service) {

        const replace_rules = this.props.rules.params.replace,
            excludes_rules = this.props.rules.params.excludes,
            nameconvention = this.props.nameconvention.keys,
            serviceCart = this.props.cart.cart.find(x => x.params.Service === service.params.Service)

        let specifyProps

        const options = library.getServiceDefaultParams(service, nameconvention, "microservice", "params", excludes_rules, replace_rules)
        if (serviceCart) {
            const optionsCart = library.getServiceDefaultParams(serviceCart, nameconvention, "microservice", "params", excludes_rules, replace_rules)
            specifyProps = {values:optionsCart.params}
        }

        return (
            <Input labels={options.keys}
                   placeholders={options.params}
                   type="DEPLOY"
                   specifyProps={specifyProps}
                   onChange={this.onChange}
            />
        )
    }

    render() {
        return (
            <div className={`deploy-service-options d-flex`}>
                <div className='deploy-options d-flex'>
                    {this.showParams(this.props.service)}
                </div>
            </div>
    )

    }
}

function mapStateToProps (state) {
    return {
        rules: state.rules,
        cart: state.cart,
        nameconvention: state.nameconvention,
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        changeParamsInCart: (value, param, service, keys) => {
            dispatch({type: 'CHANGE_PARAM_CART', payload: service, value: value, param: param, keys: keys})
        }
    }))(DeployServiceParams);
