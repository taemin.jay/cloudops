import React, { Component } from 'react';
import {connect} from "react-redux";

class DeployServiceAction extends Component {

    showAction(service) {

        let tech = [], choosen_action, array_filterDisabled, array_filter

        //Выделить в общем списке Action у сервиса, добавленного в корзину
        this.props.cart.cart.forEach(cart => {

            if (cart.params.Service) {
                tech.push(cart.params.Service)
            } else {
                tech.push(cart.fullname)
            }
        })

        if (tech.indexOf(service.params.Service) !== -1) {
            choosen_action = this.props.cart.cart.find(x => x.params.Service === service.params.Service).params.Action
        } else if (tech.indexOf(service.fullname) !== -1) {
            choosen_action = this.props.cart.cart.find(x => x.fullname === service.fullname).params.Action
        }

        //Фильтр от не активных Action's
        array_filterDisabled = this.props.actions.actions.filter( x => x.disabled )
        //Фильтр по fullname
        array_filter = array_filterDisabled.filter( x => x.fullname.some(path => service.fullname.includes(path)))
        if (array_filter.length === 0) array_filter = array_filterDisabled.filter( x => x.fullname.includes("Other"))

        return array_filter.map( (action, index) => {

                let classChoose = ""

                if (choosen_action == action.name) {

                    classChoose = "active"

                }

                return (

                    <div key={index} onClick={(e) => this.props.chooseAction(e, service, action.name)}
                         name={action.name}
                         className={`deploy-action d-flex align-items-center justify-content-center ${classChoose}`}>
                        <h6>{action.name}</h6>
                    </div>
                )

            })
    }



    render() {
        return (
            <div className={`deploy-service-action d-flex flex-row flex-wrap justify-content-around`}>
                {this.showAction(this.props.service)}
            </div>
            )
    }
}


export default connect(
    state => ({
        actions: state.actions,
        cart: state.cart,
    }))(DeployServiceAction);