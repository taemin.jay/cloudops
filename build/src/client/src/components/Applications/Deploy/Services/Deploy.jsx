import React, { Component } from 'react';
import DeployFilters from './DeployFilters'
import DeployEnv from './DeployEnv'
import DeployServiceList from "./DeployServiceList";
import DeployAddBlock from "./DeployAddBlock";
import axios from "axios/index";

import {connect} from "react-redux";
import library_c from "../../../../assets/js/library_c";
import library from "../../../../assets/js/library";

class Deploy extends Component {

    constructor(props) {
        super(props)
        this.state = {
            services_all: null,
            services: ['Please, choose the Environment...'],
            services_enable: false,
            filter: props.filters[0] || {},
            loading: false,
        }
        this.callbackFun = {
            filteringSrvsList: this.filteringSrvsList,
            applyTemplate: this.applyTemplate,
            changeEnv: this.changeEnv
        }
    }

    Notification(type, message) {

        switch(type) {

            case 'Services':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of Services!",
                    title: "Deploy"
                })

            case 'Templates':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of Templates!",
                    title: "Deploy"
                })

            case 'Template-List':
                return this.props.notifyMe({
                    type: 'error',
                    message: JSON.stringify(message),
                    title: "Deploy"
                })
        }

    }

    changeEnv = (env) => {
        this.props.setEnvToCart(env)
        return this.getServiceList(env)
    }

    getServiceList(env) {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Service list by Env ----
        const options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'cache', {env: env})

        //Notification
        if (!options) return this.Notification('Services')

        console.log('Get services list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data.services

                if (answer) {
                    return this.setState({
                        services_all: answer,
                        services_enable: true,
                    }), this.filteringSrvsList(this.state.filter)
                }

                console.log("ERROR: Deploy.jsx: getServiceList: response.data.services is not available: "+ JSON.stringify(response))
                return this.setState({
                    services_all: ["...something went wrong. Check browser console."],
                    services_enable: true,
                })
            })
            .catch(reject => {

                console.log("ERROR: fetching service list is failed: " + JSON.stringify(reject))
                return this.setState({
                    services: ["Error. Can't to get services list."],
                    services_enable: true,
                    loading: false
                })

            })

        return this.setState({
           loading: true
        })

    }

    applyTemplate = (id) => {

        //---- API. GET Template Service list ----
        const options = library_c.constructorRequestOption(this.props.api.web_tool, 'temp_get_srvs', [id])

        //Notification
        if (!options) return this.Notification('Templates')

        console.log('Get services list from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data
                if (answer) {
                    return this.props.setTemplateSrvsList(answer, id), this.props.enableSearch()
                }

                console.log("ERROR: Deploy.jsx: applyTemplate: response.data is not available: "+ JSON.stringify(response))
                return this.Notification('Template-List', response)

            })
            .catch( (err) => {

                console.log("ERROR: Deploy.jsx: applyTemplate: fetching templates services list is failed: " + JSON.stringify(err))
                return this.Notification('Template-List', err)

            })

    }

    filteringSrvsList = (filter) => {

        if (!this.state.services_enable) {

            return this.setState({
                filter: filter
            })

        }

        let services_filter = JSON.parse(JSON.stringify(this.state.services_all))
        filter.filters.filter( x => x.disabled ).forEach( filter => {

            switch (filter.rule) {
                case "include":
                    return services_filter = services_filter.filter(x => {
                        return library.getValueJsonByKey(filter.key,x) && (library.getValueJsonByKey(filter.key,x)).includes(filter.value)
                    })

                case "exclude":
                    return services_filter = services_filter.filter(x => {
                        return !(library.getValueJsonByKey(filter.key,x)).includes(filter.value)
                    })

                default:
                    break


            }
        })

        return this.setState({
            services: services_filter,
            filter: filter,
            loading: false
        })

    }

    render() {
        return (
            <React.Fragment>
                <div className="deploy-header-block d-flex flex-row justify-content-baseline">
                    <DeployEnv key="Deploy" type="Deploy" callbackFun={this.callbackFun.changeEnv} />
                    <DeployFilters callbackFun={this.callbackFun}/>
                </div>
                <div className="deploy-tool-block d-flex flex-row justify-content-around">
                    <DeployServiceList deploy={{services: this.state.services, services_enable: this.state.services_enable, filter: this.state.filter, loading: this.state.loading}} />
                    <DeployAddBlock callbackFun={this.applyTemplate}/>
                </div>
            </React.Fragment>
        );
    }

}

function mapStateToProps (state) {
    return {
        server: state.appinfo.server,
        api: state.appinfo.api,
        env: state.cart.env,
        filters: state.rules.servicelist.filters,
        showCart: state.cart.showCart
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        setEnvToCart: (env) => {
            dispatch({type: 'SET_ENV_TO_CART', payload: env})
        },
        setTemplateSrvsList: (template, id) => {
            dispatch({type: 'SEARCH_TEMPLATE', template: template, id: id})
        },
        enableSearch: () => {
            dispatch({type: 'ENABLE_SEARCH'})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(Deploy);

