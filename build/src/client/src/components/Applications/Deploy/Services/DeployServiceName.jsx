import React, { Component } from 'react';
import {connect} from 'react-redux';

class DeployServiceName extends Component {

    showServiceName(service) {

        let name = service.params.Service

        if (!name) {
            name = this.applyRules(service)
        }

        return (
            <h5>{name}</h5>
        )

    }

    //Temporary--------------------------------------------------
    applyRules(service) {

        //--- If the service name IS NOT exist, use the fullname ---

        if (!service.params.Service) {

            return this.rulesNameByFullname(service.fullname)

        }

    }
    //-----------------------------------------------------------


    rulesNameByFullname(service) {

        const name = this.props.rules.servicename.find(x => x.fullname === service ).name

        if (name) {
            return name
        }

        return service
    }

    render() {

        return (
            <div className={`deploy-service-name`}>
                {this.showServiceName(this.props.service)}
            </div>
        )

    }


}

function mapStateToProps (state) {
    return {
        rules: state.rules
    };
}

export default connect(mapStateToProps)(DeployServiceName);