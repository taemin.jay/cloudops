import React, { Component } from 'react';
import {connect} from "react-redux";
import library_c from "../../../../assets/js/library_c";
import axios from "axios/index";

class DeployEnv extends Component {

    constructor () {
        super()
        this.state = {
            environment: "Environment",
            environments: ['...service is not available'],
            environments_enable: false,
            show_envs: false
        }
    }

    componentDidMount() {
        return this.checkTypeAndGetEnvs()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api || prevProps.env !== this.props.env) {
            return this.checkTypeAndGetEnvs()
        }
    }

    checkTypeAndGetEnvs() {
        if (this.props.type === "Deploy") {
            if (this.props.env) this.changeEnv(this.props.env)
        }
        return this.getEnvsList()
    }

    Notification(type, message) {

        switch (type) {

            case 'Environments':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of Environments!",
                    title: "Deploy"
                })
        }
    }

    getEnvsList() {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET Env list --------------
        const options = library_c.constructorRequestOption(this.props.api.create_json_tool, 'get_envs')

        //Notification
        if (!options) return this.Notification('Environments')

        console.log('Get environments from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then( response => {

                answer = response.data.environment
                if (answer) {
                    return this.setState({
                        environments: answer,
                        environments_enable: true
                    })
                }

                console.log("ERROR: DeployEnv.jsx: getEnvList: response.data.environment is not available: "+ JSON.stringify(response))
                return this.setState({
                    environments_enable: false
                })

            })
            .catch( (err) => {

                console.log("ERROR: DeployEnv.jsx: getEnvList: try to fetching envs list is failed: " + JSON.stringify(err))
                return this.setState({
                    environments_enable: false
                })

            })

    }

    changeEnv = (env) => {

        return this.props.callbackFun(env),
            this.state.show_envs = false,
            this.setState({
                environment: env
            })

    }

    showEnvenable = () => {

        return this.setState({
            show_envs: true
        })

    }

    hideEnvenable = () => {

        return this.setState({
            show_envs: false
        })

    }

    showEnv(environments, enable) {

        if (enable) {

            return environments.map((env, index) => {

                return <div key={index} className="brk-btn" onClick={this.changeEnv.bind(this,env.name)}>{env.name}</div>
            })

        }

        return environments.map((env, index) => {
            return <div className="brk-btn" key={index} >{env}</div>
        })

    }

    render() {

        return (
            <div className="deploy-env">
                <div className="choose-env" onMouseOver={this.showEnvenable.bind()} onMouseOut={this.hideEnvenable.bind()}>
                    <div className="env">
                        <a className={`brk-btn ${this.state.show_envs ? 'active' : ''}`}>
                            {this.state.environment}
                        </a>
                    </div>
                    <div className={`envs-list d-flex flex-column ${this.state.show_envs ? 'show' : 'hidden'}`} onMouseOver={this.showEnvenable.bind()} onMouseOut={this.hideEnvenable.bind()}>
                        {this.showEnv(this.state.environments, this.state.environments_enable)}
                    </div>
                </div>

            </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        env: state.cart.env,
        api: state.appinfo.api
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(DeployEnv);