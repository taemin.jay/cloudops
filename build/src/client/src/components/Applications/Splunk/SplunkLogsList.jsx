import React, {Component} from 'react';
import {connect} from "react-redux";
import SplunkLog from "./SplunkLog"

class SplunkLogsList extends Component {

    constructor () {
        super()
        this.state = {
            logs: [],
            last: false
        }
    }

    showLogs(logs) {


        if (logs instanceof Array) {

            return logs.map( (item,index) => {

                return <SplunkLog key={index} log={item} />

            })

        } else if (logs instanceof Object) {

            if (logs.status === "FAILED") {

                return (
                    <div className="loading d-flex flex-column align-items-center w-100">
                        <div className="error">
                            <i className="exclamation triangle icon" style={{color:"var(--color1-active)"}}></i>
                        </div>
                        <p>{logs.status}...</p>
                        <pre>{logs.result}</pre>
                    </div>
                )

            }

            return (
                <div className="loading d-flex flex-column align-items-center w-100">
                    <div className="lds-dual-ring"></div>
                    <p>{logs.status}...</p>
                </div>

            )


        } else {
            return (
                <div>
                    {logs}
                </div>
            )
        }

    }

    render () {
        return (
            <div className="splunk-logs d-flex flex-column align-items-center">
                {this.showLogs(this.state.logs)}
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        application: state.appinfo
    };
}

export default connect(mapStateToProps)(SplunkLogsList);