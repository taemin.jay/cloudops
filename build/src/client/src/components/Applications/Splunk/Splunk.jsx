import React, {Component} from 'react';
import SplunkLogsList from './SplunkLogsList'
import MaterialIcon from 'material-icons-react';

import ReactPaginate from 'react-paginate';
import axios from "axios/index";
import {connect} from "react-redux";
import library_c from "../../../assets/js/library_c"

class Splunk extends Component {

    constructor (props) {
        super(props)
        this.state = {
            active: false,

            answer: [],
            //answer: "",
            answer_enable: false,
            answer_length: null,
            raw_count: props.splunk.raw_count,
            start: 1,

            paginate: false,
            page_count: 1,

            request_id: "",
            query: "",
            time: "",
            host_domain: props.splunk.urls.list[0].domain || null
        }
    }

    componentWillMount() {

        const state = JSON.parse(library_c.getItemLocalStorage("splunk"))

        if (state) {

            return this.setState({
                active: state.active,
                answer: state.answer,
                answer_enable: state.answer_enable,
                answer_length: state.answer_length,
                raw_count: state.raw_count,
                start: state.start,
                paginate: state.paginate,
                page_count: state.page_count,
                request_id: state.request_id,
                query: state.query,
                time: state.time,
                host_domain: state.host_domain

            })

        }

        return false

    }

    componentWillUnmount() {

        const data = this.state
        return library_c.saveToLocalStorage("splunk",JSON.stringify(data))

    }


    submitForm = (event) => {

        event.preventDefault()

        //---- API. Request to Splunk --------------
        const url = this.props.server + '/splunk-tool/execute'
        let answer, id, data

        data = {query:this.state.query,splunkhostname:this.state.host_domain,earliest_time:this.state.time}
        console.log(data)

        console.log('Send request to Splunk: '+ url)

        axios.post(url, data)
            .then(response => {

                answer = response
                id = response.data.requestid

                console.log(answer)

                if (id) {
                    return this.setState({
                        request_id: id,
                        answer: {status: "Sending", progress: 0, result: "Starting"},
                        answer_enable: true,
                        paginate: false
                    }), this.getSplunkRequest_status(id)
                }

                return this.setState({
                    answer: answer,
                    answer_enable: true,
                    paginate: false
                })

            })
            .catch( (err) => {

                this.setState({
                    answer: err,
                    answer_enable: true,
                    paginate: false
                })

            })

    }

    splunkRequest_status = (id) => {

        //---- API. Get Splunk Request status --------------
        const url = this.props.server + '/splunk-tool/status/' + id
        let answer, progress, status

        console.log('Get Splunk Request status: '+ url)

        axios.get(url)
            .then(response => {

                answer = response.data

                progress = response.data.progress
                status = response.data.status

                if (progress === 100 && status === "DONE") {

                    let page_count = Math.ceil(answer.length/this.state.raw_count)


                    return clearInterval(this.splunk_status), this.setState({
                        answer_count: answer.length,
                        page_count: page_count
                    }), this.SplunkRequest_result(this.state.start,this.state.raw_count)

                } else if (status === "FAILED") {

                    return clearInterval(this.splunk_status), this.setState({
                        answer: answer,
                        answer_enable: true
                    })

                }

                return this.setState({
                    answer: answer,
                    answer_enable: true
                })

            })
            .catch( (err) => {

                this.setState({
                    answer: err,
                    answer_enable: true
                })

            })

    }

    getSplunkRequest_status = (id) => {

        console.log(id)

        this.splunk_status = setInterval(
            () => this.splunkRequest_status(id),
            5000
        );

    }

    SplunkRequest_result = (start, raw_count) => {

        //---- API. Get Splunk Request status --------------
        const url = this.props.server + '/splunk-tool/get/result/' + start + "/" + raw_count
        let answer

        console.log('Get Splunk Request result: '+ url)

        axios.get(url)
            .then(response => {

                answer = response.data

                console.log(answer)

                if (answer && !answer[0].error) {
                    return this.setState({
                        answer: answer,
                        answer_enable: true,
                        paginate: true
                    })
                }

                return this.setState({
                    answer: answer[0],
                    answer_enable: true
                })

            })
            .catch( (error) => {

                console.log(error)

                this.setState({
                    answer: error,
                    answer_enable: true
                })


            })
    }

    handleQuery = (e) => {

        console.log(e.target.value)

        return this.setState({
            query: e.target.value
        })

    }

    handleTime = (e) => {

        console.log(e.target.value)

        return this.setState({
            time: e.target.value
        })

    }

    handleHost = (e, domain) => {

        e.preventDefault(); // Let's stop this event.
        e.stopPropagation(); // Really this time.

        return this.setState({
            host_domain: domain
        })

    }

    pageChange = (e) => {

        console.log(e)

        let page = e.selected,
            start = page*this.state.raw_count

        return this.SplunkRequest_result(start, this.state.raw_count)

    }

    showDomains(list) {

        let host_active

        if (list instanceof Array) {

            return list.map((domain, index) => {

                host_active = false

                if (this.state.host_domain === domain.domain) {

                    host_active = true

                }

                return (
                    <div key={index} onClick={(e) => this.handleHost(e, domain.domain)}
                       title={domain.domain}
                       className={`deploy-action d-flex align-items-center justify-content-center ${ host_active ? 'active' : null}`}
                    >
                        <h6>{domain.name}</h6>
                    </div>
                )
            })
        }

        return (
            <div>No domains found</div>
        )

    }

    render () {
        return (
            <div className="splunk">
                <form method="" onSubmit={(e)=>this.submitForm(e)}>

                    <div className="d-flex flex-row flex-wrap align-items-center">
                        <div className="splunk-domains-button d-flex flex-raw flex-1">
                            {this.showDomains(this.props.splunk.urls.list)}
                        </div>
                        <input value={this.state.query}
                               placeholder="query"
                               onChange={this.handleQuery.bind(this)}
                               required
                               className="default flex-8"/>
                        <input value={this.state.time}
                               placeholder="earliest time"
                               onChange={this.handleTime.bind(this)}
                               required
                               className="default flex-1"/>
                        <button type="submit">
                            <MaterialIcon icon="search" size={40} color="inherit"/>
                        </button>
                    </div>

                </form>
                { this.state.paginate ? <ReactPaginate pageCount={this.state.page_count}
                                                       onPageChange={this.pageChange}
                                                       containerClassName="d-flex flex-row align-items-center justify-content-between"
                /> : null}
                <SplunkLogsList logs={this.state.answer} />
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        server: state.appinfo.server,
        api: state.appinfo.api,
        splunk: state.settings.splunk
    };
}

export default connect(mapStateToProps)(Splunk);