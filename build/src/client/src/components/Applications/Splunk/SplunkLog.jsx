import React, {Component} from 'react';
import {connect} from "react-redux";

class SplunkLog extends Component {

    constructor () {
        super()
        this.state = {

        }
    }

    showLog(log) {

        if (log.lastrow) return (<div>No more data</div>)

        let date = new Date(log.result._time)

        return (
            <div className="splunk-log list-item">
                <div className="d-flex flex-row">
                    <div className="splunk-log-date">
                        <p>
                            {`${date.toDateString()}\n\n${date.toLocaleTimeString()}`}
                        </p>
                    </div>
                    <div className="splunk-log-log d-flex flex-column">
                        <div className="result-raw">
                            <pre>
                                {log.result._raw}
                            </pre>
                        </div>
                        <div className="splunk-log-additional d-flex flex-row">
                            <div className="splunk-log-host">
                                <p>
                                    host = {log.result.host}
                                </p>
                            </div>
                            <div className="splunk-log-source">
                                <p>
                                    source-log = {log.result.source}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )

    }

    render () {
        return (
            <React.Fragment>
                {this.showLog(this.props.log)}
            </React.Fragment>
        )
    }
}

function mapStateToProps (state) {
    return {
    };
}

export default connect(mapStateToProps)(SplunkLog);