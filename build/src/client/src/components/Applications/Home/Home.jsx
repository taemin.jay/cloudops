import React, { Component } from 'react';
import HomeCheckTools from './HomeCheckTools'
import HomeCart from './HomeCart'
import ConsoleTasks from '../Deploy/Console/ConsoleTasks'

class Home extends Component {

    render() {
        return (
                <div className="application">
                    <HomeCheckTools />
                    <div className="w-100 d-flex flex-row">
                        <HomeCart />
                        <ConsoleTasks type="deploy-tool"/>
                    </div>
                </div>
        );
    }


}

export default Home