import React, { Component } from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import CartEnv from "../Deploy/Cart/CartEnv";
import CartJobList from "../Deploy/Cart/CartJobList"


class HomeCart extends Component {

    showCart(cart) {

        if (cart.cart.length > 0) {

            return (
                <div className="home-cart-list d-flex flex-column align-items-center">
                    <CartEnv env={cart.env}/>
                    <CartJobList jobs={cart.cart}/>
                </div>
            )

            /*return cart.map( (cart, index) => {

                return (
                    <div key={index} className={`cart d-flex flex-column ${this.state[tool] ? 'active' : 'checking'}`}>
                        <h5>{tool}</h5>
                        <p>{api[tool].hostname + ":" + api[tool].port}</p>
                        <p>{api[tool].hosttype}</p>
                    </div>
                )

            })*/

        }

        return (
            <div className="d-flex flex-column align-items-center">
            </div>
        )

    }

    render() {
        return (
            <div className="home home-cart home-list">
                <div className="d-flex flex-column">
                    <div className="default-header-block">
                        <h5>Cart</h5>
                    </div>
                    {this.showCart(this.props.cart)}
                </div>
            </div>
        )
    }


}


function mapStateToProps (state) {
    return {
        cart: state.cart
    };
}

export default withRouter(connect(mapStateToProps)(HomeCart))
