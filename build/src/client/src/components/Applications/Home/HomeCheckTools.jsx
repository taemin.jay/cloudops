import React, { Component } from 'react';
import {connect} from "react-redux";
import axios from "axios/index";

class HomeCheckTools extends Component {

    constructor() {
        super()
        this.state = {
            tools_available: []
        }
    }

    componentDidUpdate(prevProps) {

        if (prevProps.api !== this.props.api && this.props.enable) {
            return this.constructorToolsForChecking(this.props.api), this.setIntervalCheckingTool(this.props.api)
        }

    }

    componentDidMount() {

        if (this.props.api) {
            return this.constructorToolsForChecking(this.props.api), this.setIntervalCheckingTool(this.props.api)
        }

    }

    componentWillUnmount() {

        clearInterval(this.tools);

    }

    setIntervalCheckingTool(api) {

        if (!api) {

            //Notification
            return null

        }

        this.tools = setInterval(
            () => this.constructorToolsForChecking(api),
            this.props.delay
        );

    }

    constructorToolsForChecking(api) {

        const tools = Object.keys(api)
        tools.forEach( (tool) => {
            this.checkToolAvailable(tool, api)
        })

    }

    checkToolAvailable(tool,api) {

        //---- API. checking --------------
        let options, protocol, url

        if (api[tool].protocol && api[tool].protocol === "https") {
            protocol = api[tool].protocol
            url = protocol+"://" + api[tool].hostname
        } else {
            protocol = "http"
            url = protocol+"://" + api[tool].hostname + ":" + api[tool].port
        }
        options = {
            url,
            method: "GET"
        }

        axios(options)
            .then( response => {
                console.log(response.status)
                if (response.status === 200) {
                    return this.setState({
                        [tool]: true
                    })
                }
                return this.setState({
                    [tool]: false
                })

            })
            .catch( (err) => {
                console.log(err)
                return this.setState({
                    [tool]: false
                })
            })
    }

    //Не забыть добавить кнопку для обновления
    update = () => {

        return this.constructorToolsForChecking(this.props.api)

    }


    showTools(api) {

        if (api) {

            const tools = Object.keys(api)

            return tools.map( (tool, index) => {

                return (
                    <div key={index} className={`tool d-flex flex-column ${this.state[tool] ? 'active' : 'checking'}`}>
                        <h5>{tool}</h5>
                        <p>{api[tool].hostname}{api[tool].port  ?  ":" + api[tool].port : null}</p>
                        <p>{api[tool].hosttype}</p>
                    </div>
                )

            })

        }

        return (
            <div className="lds-dual-ring onload"></div>
        )

    }

    render() {
        return (
            <div className="home home-check-tools home-list">
                <div className="d-flex flex-column">
                    <div className="default-header-block">
                        <h5>Tools</h5>
                    </div>
                    <div className="tools d-flex flex-row flex-wrap">
                        {this.showTools(this.props.api)}
                    </div>
                </div>
            </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        api: state.appinfo.api,
        enable: state.settings.homepage.auto_check_tools,
        delay: state.settings.homepage.timeout
    };
}

export default connect(mapStateToProps)(HomeCheckTools)
