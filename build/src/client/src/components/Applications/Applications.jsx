import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from './Home/Home'
import Deploy from './Deploy/Services/Deploy'
import History from './Deploy/History/History'
import Templates from './Deploy/Templates/Templates'
import Console from "./Deploy/Console/Console"
import AWS from "./AWS/AWS";
import Splunk from "./Splunk/Splunk"
import SettingsDeploy from "./Deploy/Settings/Settings"
import Settings from "./Settings/Settings"

class Applications extends Component {

    render() {

        return (
            <section>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/deploy' component={Deploy}/>
                    <Route path='/deploy-tool' component={Deploy}/>
                    <Route path='/console' component={Console}/>
                    <Route path='/history' component={History}/>
                    <Route path='/templates' component={Templates}/>
                    <Route path='/aws' component={AWS}/>
                    <Route path='/splunk' component={Splunk}/>
                    <Route path='/settings-deploy' component={SettingsDeploy}/>
                    <Route path='/settings' component={Settings}/>
                </Switch>
            </section>
        )
    }

}


export default Applications
