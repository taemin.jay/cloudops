import React, { Component } from 'react';
import AWSList from "./AWSList"
import AWShostInfo from "./AWShostInfo";

import library_c from "../../../assets/js/library_c"

import axios from "axios/index";

import {connect} from "react-redux";

class AWS extends Component {

    constructor () {
        super()
        this.state = {
            hosts: [],
            hosts_enable: false,
            answer_enable: false,
            hosts_for_search: "",
            hosts_search: [],
            info: "",
            active_hostInfo: false,
            cloudwatch_info: null,
            cw_enable: false,
            SSH_enable: false
        }
        this.callbackFun = {
            getShortInfo: this.getShortInfo,
            getCloudWatch: this.getCloudWatch,
            getCloudWatchHostInfo: this.getCloudWatchHostInfo,
            checkSSHConnection: this.checkSSHConnection,
            disconnectFromHost:this.disconnectFromHost
        }
    }

    componentDidMount() {
        return this.getAWSList()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api) {
            this.getAWSList()
        }

        if (prevProps.searching !== this.props.searching) {
            this.searching(this.props.searching)
        }

    }

    searching(searching) {

        const filter = searching.toUpperCase(),
            rx = new RegExp('"([^"]*'+filter+'[^"]*)"','gi'),
            rx2 = '\\[([^\\[]+)\\]'
        let results = [], result, key

        while (result = rx.exec(this.state.hosts_for_search)) {
            key = result[1].match(rx2);
            results.push(key[1]);
        }

        return this.setState({
            hosts_search: results
        })

    }

    componentWillUnmount() {

        return this.disconnectFromHost()

    }

    Notification(type, message) {

        switch (type) {

            case 'AWS-error':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to get list of AWS!",
                    title: "AWS"
                })

            case 'SSH-Connection-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: message,
                    title: "SSH connection"
                })

            case 'AWS-CloudWatch-Error':
                return this.props.notifyMe({
                    type: 'warning',
                    message: message,
                    title: "AWS"
                })

            case 'AWS-CloudWatch-Warning':
                return this.props.notifyMe({
                    type: 'warning',
                    message: `Can't to get Cloud Watch info. Options failed.`,
                    title: "AWS"
                })

        }
    }

    getAWSList() {

        //Validation
        if (!library_c.validationFunction(this.props.api)) return false

        //---- API. GET AWS list --------------
        const options = library_c.constructorRequestOption(this.props.api.aws_tool, 'ec2_info')

        //Notification
        if (!options) return this.Notification('AWS-Error')

        console.log('Get AWS list from: '+ JSON.stringify(options))

        let answer, answerSearch
        axios(options)
            .then(response => {

                answer = response.data.prod.concat(response.data.nonprod)
                answerSearch = library_c.formateAWSJsonToString(answer)

                if (answer) {
                    return this.setState({
                        hosts: answer,
                        hosts_enable: true,
                        answer_enable: true,
                        hosts_for_search: answerSearch
                    })
                }

                console.log("ERROR: AWS.jsx: getAWSList: response.data is not available: "+ JSON.stringify(response))
                return this.setState({
                    hosts: ['Something went wrong...'],
                    hosts_enable: false,
                    answer_enable: true
                }), this.Notification('AWS-Error')

            })
            .catch( (err) => {

                console.log("ERROR: AWS.jsx: getAWSList: try to fetching aws list is failed: " + JSON.stringify(err))
                return this.setState({
                    hosts: ['AWS-tool is not available. Or something went wrong...'],
                    hosts_enable: false,
                    answer_enable: true
                }), this.Notification('AWS-Error')

            })

    }

    showItemByTimeout(state, list, index, delay) {

        if (!delay) {

            delay = this.props.design.show.delay

        }

        let buff = list

        if (index < list.length) {

            buff = this.state[state]
            buff.push(list[index])
            index++
            setTimeout( () => { this.showItemByTimeout(state, list, index, delay) }, delay);

        }

        return this.setState({
            [state]: buff
        })

    }

    checkSSHConnection = (host) => {

        if (host.ExternalIP) {

            //---- API. Checking SSH connect to host ----
            const ip = host.ExternalIP,
                options = library_c.constructorRequestOption(this.props.api.aws_tool, 'ec2_check_instance', [ip])

            //Notification
            if (!options) return this.Notification('AWS-Error')

            this.setvalueToPutty(ip)
            console.log('Check SSH connect: '+ JSON.stringify(options))

            let answer
            axios(options)
                .then(response => {

                    answer = response.data

                    if (answer.connected !== "false") {

                        return this.connectToHost(ip, host.Name), this.setState({
                            SSH_enable: true
                        })

                    }

                    return this.setState({
                        SSH_enable: false
                    })

                })
                .catch(err => {
                    console.log("ERROR: AWS.jsx: checkSSH: try to checking is failed: " + JSON.stringify(err))
                })

        }

    }

    setvalueToPutty(ip) {
        return document.getElementById('console').contentWindow.postMessage({setvalue:{username:this.props.settings.username,hostname:ip}}, '*');
    }

    disconnectFromHost() {
        return document.getElementById('console').contentWindow.postMessage({command:"exit"}, '*');
    }

    connectToHost = (ip, name) => {

        if (!this.props.settings.auto_ssh) return false

        let privatekey, options

        if (name.indexOf("prod") !== -1 || name.indexOf("beta") !== -1 || name.indexOf("infra") !== -1) {

            if (!this.props.settings.hostAWS_key_prod.disabled) {

                return this.Notification('AWS-Error',`ssh-key isn't set for PROD`)

            }

            privatekey = this.props.settings.hostAWS_key_prod.key

        } else {

            if (!this.props.settings.hostAWS_key_nonprod.disabled) {

                return this.Notification('AWS-Error',`ssh-key isn't set for DEV`)

            }

            privatekey = this.props.settings.hostAWS_key_nonprod.key

        }

        options = {
            hostname: ip,
            port: this.props.settings.port,
            username: this.props.settings.username,
            password: this.props.settings.password,
            privatekey: privatekey
        }

        return document.getElementById('console').contentWindow.postMessage(JSON.stringify(options), '*');


    }

    getShortInfo = (host) => {

        return this.setState({
            active_hostInfo: true,
            info: host
        })


    }

    getCloudWatch = (host) => {

        if (this.props.settings.auto_cloudwatch) {
            return this.disableCW(), this.getCloudWatchHostInfo(host)
        }

    }


    disableCW() {

        return this.setState({
            cw_enable: false
        })

    }

    getCloudWatchHostInfo = (host) => {

        console.log(host)
        //---- API. GET CloudWatch data for the instance ----
        const id= host.InstanceId,
            options = library_c.constructorRequestOption(this.props.api.aws_tool, 'cloudwatch_alarm_history', [id])

        //Notification
        if (!options) return this.Notification('AWS-CloudWatch-Warning')

        console.log('Get CloudWatch from: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer.code === "ECONNRESET") {

                    console.log("ERROR: AWS.jsx: getCloudWatch: response.data. code ECONNRESET: "+ JSON.stringify(response))
                    return this.setState({
                        cloudwatch_info: ["Something went wrong. Please, try again..."],
                        cw_enable: true
                    })

                }

                return this.setState({
                    cloudwatch_info: answer,
                    cw_enable: true
                }), this.Notification('AWS-CloudWatch-Error', `Can't to get Cloud Watch info for ${id}`)

            })
            .catch(err => {

                console.log("ERROR: AWS.jsx: getCloudWatch: try to fetching cloudwatch info is failed: " + JSON.stringify(err))
                return this.setState({
                    cloudwatch_info: "Error. Can't to get alarm history.",
                    cw_enable: true
                }), this.Notification('AWS-CloudWatch-Error', `Can't to get Cloud Watch info for ${id}`)

            })


    }

    showAWSData(enable) {

        if (enable) {

            return (
                <div className="d-flex flex-wrap flex-row">
                    <AWSList hosts={this.state.hosts} hosts_enable={this.state.hosts_enable} hosts_search={this.state.hosts_search} callbackFun={this.callbackFun}/>
                    <AWShostInfo {...this.state} callbackFun={this.callbackFun}/>
                </div>
            )

        }

        return (
            <div className="d-flex flex-wrap flex-row justify-content-center h-100">
                <div className="lds-dual-ring onload"></div>
            </div>
        )

    }

    render() {
        return this.showAWSData(this.state.answer_enable)


    }


}

function mapStateToProps (state) {
    return {
        api: state.appinfo.api,
        design: state.appinfo.design,
        searching: state.searching.search,
        settings: state.settings.aws
    }
}

export default connect(mapStateToProps,
    dispatch => ({
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        }
    }))(AWS);

