import React, { Component } from 'react';
import AWShost from './AWShost'
import {connect} from "react-redux";

class AWSList extends Component {

    constructor (props) {
        super(props)
        this.element = React.createRef()
        this.state = {
            start: 0,
            end: 20,
            divHeight: 0
        }

        this.element = React.createRef()

        this.scrollList = {
            divsOnPage: 20,
            delta: 5,
            prevStart: 0,
            prevEnd: 0,
            currentStart: 0,
            currentEnd: 0,
            nextStart: 0,
            nextEnd: 0,
            deltaHeight: 0,
            divHeightList: 0,
            divHeightGrid: 0,
            DeployServiceHeight: 77,
            DeployServiceWidth: 200,
            BlockHeight: 0,
            BlockWidth: 0
        }
    }

    componentDidMount(){
        this.element.current.addEventListener("scroll", this.handleScroll, false)
        this.preCalculate()
    }

    handleScroll = (e) => {
        const scrollTop = e.target.scrollTop,
            divHeight = this.state.divHeight,
            deltaHeight = this.scrollList.deltaHeight
        let prevStart = this.scrollList.prevStart,
            prevEnd = this.scrollList.prevEnd,
            nextStart = this.scrollList.nextStart,
            nextEnd = this.scrollList.nextEnd

        console.log(scrollTop, deltaHeight, divHeight)
        if (scrollTop + 80 >= deltaHeight + divHeight) {
            console.log("=====> scrollTop >= deltaHeight + divHeight")

            this.calculateSteps(nextStart,nextEnd)

            return this.setState({
                divHeight: divHeight + deltaHeight
            })
        }
        if (scrollTop + 80 < divHeight) {
            console.log("=====> scrollTop <= divHeight")

            this.calculateSteps(prevStart,prevEnd)

            return this.setState({
                divHeight: divHeight - deltaHeight
            })
        }
    }

    preCalculate() {
        const divsOnPage = this.scrollList.divsOnPage,
            currentStart = 0,
            currentEnd = currentStart + divsOnPage

        this.calculateDelta()
        this.calculateSteps(currentStart, currentEnd)
        return this.setState({
            divHeight: 0
        })

    }

    calculateDelta() {

        const width = this.element.current.clientWidth,
            countInWidth = Math.floor(width/this.scrollList.DeployServiceWidth),
            lines = Math.floor(this.scrollList.delta/countInWidth),
            deltaHeight = lines * this.scrollList.DeployServiceHeight
        let delta = lines * countInWidth,
            divsOnPage = this.scrollList.divsOnPage

        console.log(width, countInWidth, lines, deltaHeight)

        divsOnPage = Math.floor(divsOnPage/delta)*delta

        return this.scrollList.deltaHeight = deltaHeight, this.scrollList.delta = delta, this.scrollList.divsOnPage = divsOnPage
    }

    calculateSteps(start,end) {
        const delta = this.scrollList.delta,
            divsOnPage = this.scrollList.divsOnPage,
            currentStart = this.scrollList.currentStart = start,
            currentEnd = this.scrollList.currentEnd = end,
            prevStart = this.scrollList.prevStart = currentStart - delta,
            prevEnd = this.scrollList.prevEnd = currentEnd - delta,
            nextStart = this.scrollList.nextStart = currentStart + delta,
            nextEnd = this.scrollList.nextEnd = nextStart + divsOnPage

        return this.setState({
            start: currentStart,
            end: currentEnd
        })
    }

    showAWSList(hosts, enable, array_search) {

        if (enable) {
            return hosts.map((host, index) => {

                if (this.props.searching.searchingStringAWS) {

                    if (array_search.includes(host.InstanceId)) {
                        return (
                            <AWShost key={index} host={host} callbackFun={this.props.callbackFun}/>
                        )
                    }

                    return null
                }

                return (
                    <AWShost key={index} host={host} callbackFun={this.props.callbackFun}/>
                )

            }).slice(this.state.start, this.state.end)
        }

        return hosts.map((host, index) => {
            return (
                <div key={index} className="deploy-service-no-data d-flex flex-column">
                    <h5>{host}</h5>
                </div>
            )
        })

    }

    constructorList = () => {

        return (
                <div className="height-current aws-hosts-list d-flex flex-column align-items-center">
                    {this.showAWSList(this.props.hosts, this.props.hosts_enable, this.props.hosts_search)}
                </div>
        )
    }

    render() {

        const style = {minHeight:this.state.divHeight}

        return (
            <div ref={this.element} className="application d-flex flex-column">
                <div style={style} dataheight={this.state.divHeight}></div>
                {this.constructorList()}
            </div>
        );
    }


}

function mapStateToProps (state) {
    return {
        searching: state.searching
    }
}

export default connect(mapStateToProps)(AWSList);
