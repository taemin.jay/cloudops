import React, { Component } from 'react';
import Iframe from 'react-iframe'

class AWSHostConsole extends Component {

    constructor () {
        super()
        this.state = {

        }
    }

    showHost() {

        return (
            <div className={`list-item aws-host-shortInfo d-flex flex-column justify-content-between align-items-center`}>
                <Iframe url="https://0.0.0.0:8787/"
                        width="100%"
                        height="450px"
                        id="console"
                        display="initial"
                        position="relative"
                        allow="payment"
                        referrerPolicy="unsafe-url"/>

            </div>
        );
    }

    render() {
        return this.showHost()
    }

}

export default AWSHostConsole;