import React, { Component } from 'react';
import library from '../../../assets/js/library'

class AWSHostShortInfo extends Component {

    showLine(data,key) {

        if (key) {

            return (
                <React.Fragment>
                    <label>{key}</label>
                    <pre>{data}</pre>
                </React.Fragment>
            )

        }

        return (
            <React.Fragment>
                <pre>{data}</pre>
            </React.Fragment>
        )


    }

    showShortInfo(info) {

        const data = library.getValuesJson(info)

        if (data) {

            return data.keys.map( (key, index) => {

                return (
                    <div key={index} className="aws-shortInfo-item d-flex flex-row justify-content-between">
                        {this.showLine(data.json[key], key)}
                    </div>

                )

            })

        }

        return <h5>Data isn't the Object or there is no data</h5>

    }

    showHost(info) {

        return (
            <div className={`list-item aws-host-shortInfo d-flex flex-raw flex-wrap align-items-center`}>
                <h4>{info.Name}</h4>
                {this.showShortInfo(info)}
            </div>
        );
    }

    render() {
        return this.showHost(this.props.info)
    }

}


export default AWSHostShortInfo;