import React, { Component } from 'react';
import AWSHostShortInfo from "./AWSHostShortInfo";
import AWSHostCloudWatch from "./AWSHostCloudWatch"
import AWSHostConsole from "./AWSHostConsole";

class AWShostInfo extends Component {

    showHost() {

        const info = this.props.info,
            active = this.props.active_hostInfo,
            cw_enable = this.props.cw_enable,
            cw_info = this.props.cloudwatch_info

        return (
            <div className={`application aws-host-info d-flex flex-column justify-content-around align-items-baseline ${ active ? 'active' : null}`}>
                <div className="d-flex flex-row flex-wrap w-100 justify-content-around">
                    <AWSHostShortInfo info={info}/>
                    <AWSHostCloudWatch info={cw_info} host={info} enable={cw_enable} callbackFun={this.props.callbackFun}/>
                </div>
                <div className="d-flex flex-row flex-wrap w-100 justify-content-around">
                    <AWSHostConsole info={info}/>
                </div>

            </div>
        );
    }

    render() {
        return this.showHost()
    }

}


export default AWShostInfo;