import React, { Component } from 'react';

class AWSHostEvents extends Component {

    showHost(info) {

        return (
            <div className={`list-item aws-host-shortInfo d-flex flex-column justify-content-between align-items-center`}>
                <pre>{JSON.stringify(info,null,4)}</pre>
            </div>
        );
    }

    render() {
        return this.showHost(this.props.info)
    }

}


export default AWSHostEvents;