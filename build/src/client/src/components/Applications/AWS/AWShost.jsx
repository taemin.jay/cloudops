import React, { Component } from 'react';

class AWShost extends Component {

    callbackFun = (host) => {

        this.props.callbackFun.disconnectFromHost()
        this.props.callbackFun.getShortInfo(host)

        return setTimeout( () => {
            return this.props.callbackFun.checkSSHConnection(host),
                this.props.callbackFun.getCloudWatch(host)
        },1000)

    }

    showHost(host) {

        return (
            <div onClick={this.callbackFun.bind(this,host)} className={`aws-block d-flex flex-row justify-content-center align-items-center`}>
                <h5>{host.Name}</h5>

            </div>
        );
    }

    render() {
        return this.showHost(this.props.host)
    }


}


export default AWShost;