import React, { Component } from 'react';
import {connect} from "react-redux";

class AWSHostCloudWatch extends Component {

    constructor () {
        super()
        this.state = {
            strongEnable: true
        }
    }

    getCloudWatchInfo = (host) => {

        this.setState({
            strongEnable: false
        })

        console.log(host)
        return this.props.callbackFun.getCloudWatchHostInfo(host)
    }

    showHost(info, enable, strongEnable) {

        if (!this.props.settings.aws.auto_cloudwatch && strongEnable) {

            return (
                <a title="Update CloudWatch" onClick={this.getCloudWatchInfo.bind(this,this.props.host)} className={`console-options-a`}><i className='sync alternate icon'></i></a>
            )

        }

        if (enable) {

            this.setState({
                strongEnable: true
            })

            return (
                <pre>
                    {JSON.stringify(info)}
                </pre>
            )

        }

        return (<div className="lds-dual-ring onload"></div>);
    }

    render() {
        return (
            <div className={`list-item aws-host-cloudwatch d-flex flex-column justify-content-between align-items-center`}>
                <h4>Cloud Watch</h4>
                {this.showHost(this.props.info,this.props.enable, this.state.strongEnable)}
            </div>
        )
    }

}

function mapStateToProps (state) {
    return {
        application: state.appinfo,
        settings:state.settings
    };
}

export default connect(mapStateToProps)(AWSHostCloudWatch);