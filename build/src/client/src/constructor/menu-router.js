import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import {connect} from 'react-redux';

import library from '../assets/js/library'

class RouterList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            routers: this.props.router.routers,
            extMenu: false,
            routers_check: []
        }
    }

    componentDidMount() {

        return this.test()

    }

    test(link) {

        let buff = library.getArrayOfJsonFilterByJson(this.props.router.routers, [{disabled:true},{extMenu:true}])[0],
            extrouters = library.getKeyValueJson(buff.extRouters, 'link'),
            path = link || window.location.pathname,
            routers = [],
            routers_check = [buff.link]

        routers.push(buff.link)
        routers = routers.concat(extrouters)

        if (routers.includes(path)) {

            if (path === "/deploy") {
                path = "/deploy-tool"
            }

            routers_check.push(path)

            return this.setState({
                routers_check: routers_check,
                extMenu: true,
                routers: buff.extRouters
            })

        }

        return this.setState({
            routers_check: [path]
        })

    }

    showExtMenu(router) {

        if (router.extMenu) {

            this.test(router.link)

            return this.setState(
                {
                    routers: router.extRouters,
                    extMenu: true
                }
            )

        }

        return this.setState({
            routers_check: [router.link]
        })

    }

    hideExtMenu() {

        this.test()

        this.setState(
            {
                routers: this.props.router.routers,
                extMenu: false
            }
        )

    }

    showBackArrow(extMenu) {
        if (extMenu) {
            return (
                <a key="back" onClick={(() => this.hideExtMenu())}><i className="long arrow alternate left icon"></i></a>
            )
        }
        return null
    }

    showList(state) {

        let className

        return state.routers.filter(router => router.disabled).map(router => {


            if (router.icon) {

                className = ""

                if (state.routers_check.includes(router.link)) {

                    className = "active"

                }

                return (
                    <Link className={className} to={router.link} title={router.desc} key={router.id} data-menu={router.desc}
                          onClick={(() => this.showExtMenu(router))}><i className={router.icon}></i></Link>
                )

            }

            return  (
                <Link to={router.link} title={router.desc} key={router.id} data-menu={router.desc}
                      onClick={(() => this.showExtMenu(router))}>{router.name}</Link>
            )

        })

    }

    showMenu (state) {

        return (
            <nav className="navigation d-flex flex-column align-items-center">
                <div className="menu-back">
                    {this.showBackArrow(state.extMenu)}
                </div>
                {this.showList(state)}
            </nav>
        )

    }

    render () {
        return this.showMenu(this.state)
    }
}

function mapStateToProps (state) {
    return {
        router: state.router
    };
}

export default connect(mapStateToProps)(RouterList);