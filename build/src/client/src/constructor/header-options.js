import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import axios from "axios/index";
import library_c from "../assets/js/library_c";

class HeaderOptionsList extends Component {

    constructor () {
        super()
        this.state = {
            console: false,
            network: false
        }
    }



    componentDidUpdate(prevProps) {
        if (prevProps.api !== this.props.api) {
            this.checkingIPsprepare()
            this.checkIP = setInterval(
                () => this.checkingIPsprepare(),
                10000
            );
        }
        if (prevProps.timer !== this.props.timer) {
            this.setState({
                console: this.props.timer
            })
        }

        return true
    }

    checkingIPsprepare() {

        this.props.options
            .filter( option => option.type === "IP-check" )
            .filter( option => option.disabled )
            .forEach( option => {
                if (!option) {
                    return clearInterval(this.checkIP)
                }

                return this.checkIPConnection(option.desc, option.ip)

            })

    }

    checkIPConnection(name_vpn,ip) {

        //---- API. Checking VPN connection --------------
        //Validation
        if (!library_c.validationFunction(this.props.api.web_tool)) return false

        const options = library_c.constructorRequestOption(this.props.api.web_tool, "check_ip", [ip])

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    return this.setState({
                        [name_vpn]: true
                    })
                }

                return this.setState({
                    [name_vpn]: false
                })

            })
            .catch( (err) => {

                console.log("ERROR: header-options.jsx: checkIPConnection: try to checking ip failed: " + JSON.stringify(err))
                this.setState({
                    [name_vpn]: false
                })

            })


    }

    componentWillUnmount() {

        this.unlisten();
        clearInterval(this.checkIP);

    }

    enableMenuButton = (name) => {



    }

    showList(state) {

        return this.props.options.filter(option => option.disabled).map( (option, index) => {

            let className = ""

            if (option.name === "Console" && state.console) {
                className = "active-console-option"

                return (
                    <Link key={index} to={option.link} title={option.desc} className={`${className} ${option.type}`} onClick={this.enableMenuButton.bind(option.name)}><i className={option.icon}></i></Link>
                )
            } else if (option.name === "VPN") {

                if (state[option.desc]) {
                    className = "active"
                }

                return (
                    <a key={index} title={option.name +" "+ option.desc} className={`vpn ${className} ${option.type}`}><i className={option.icon}></i><span className="vpn-label">{option.desc}</span></a>
                )
            }

            return (
                <a key={index} href={option.link} title={option.desc} className={`${className} ${option.type}`}><i className={option.icon}></i></a>
            )
        })
    }

    showMenu () {

        return (
            <div className="header-options-block d-flex flex-row align-items-center justify-content-between">
                {this.showList(this.state)}
            </div>
        )

    }

    render () {
        return this.showMenu()
    }
}

export default connect(state => ({
    options: state.options.main,
    timer: state.building.timer,
    api: state.appinfo.api
}))(HeaderOptionsList);