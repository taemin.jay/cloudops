import React, {Component} from 'react';
import {connect} from 'react-redux';
import library_c from '../assets/js/library_c'
import axios from "axios/index";
import TableView from './table-view'

class HeaderVariableOptionsList extends Component {

    constructor () {
        super()
        this.state = {
            active: false
        }
    }

    Notification(type, message) {

        switch(type) {

            case 'UPDATE-TASK-ERR':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to update the task!",
                    title: "Console"
                })
            case 'PAUSE-TASK-ERR':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to pause a tasks!",
                    title: "Console"
                })
            case 'PAUSE-TASK-WARN':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Worker on PAUSE already!",
                    title: "Console"
                })
            case 'PAUSE-TASK-WARN-MESSAGE':
                return this.props.notifyMe({
                    type: 'warning',
                    message: message.message,
                    title: `Console. Task #${message.task}`
                })
            case 'RESUME-TASK-ERR':
                return this.props.notifyMe({
                    type: 'error',
                    message: "Can't to resume a tasks!",
                    title: "Console"
                })
            case 'RESUME-TASK-WARN':
                return this.props.notifyMe({
                    type: 'warning',
                    message: "Worker running already!",
                    title: "Console"
                })
            case 'STOP-TASK-ERR':
                return this.props.notifyMe({
                    type: 'error',
                    message: message,
                    title: "Console"
                })
            case 'STOP-TASK-WARN':
                return this.props.notifyMe({
                    type: 'warning',
                    message: 'Nothing to Stop!',
                    title: "Console"
                })
            case 'STOP-TASK-WARN-MESSAGE':
                return this.props.notifyMe({
                    type: 'warning',
                    message: message.message,
                    title: `Console. Task #${message.id}`
                })
            case 'SETTING-UPDATE':
                return this.props.notifyMe({
                    type: 'info',
                    message: `Updating service list...`,
                    title: "Update"
                })
            case 'AWS-UPDATE':
                return this.props.notifyMe({
                    type: 'info',
                    message: `Updating AWS...`,
                    title: "Update"
                })
            case 'ID-UPDATE-ERR':
                return this.props.notifyMe({
                    type: 'error',
                    message: `Task ID is not set`,
                    title: "Update"
                })

        }

    }

    searchingText = (e) => {

        this.props.searchingSrvs(e.target.value)

        if (e.target.value.length > 0) {
            return this.setState({
                active: true
            })
        }

        return this.setState({
            active: false
        })

    }

    play = () => {
        //---- API. Pause all Tasks ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'resume')

        //Notification
        if (!options) return this.Notification('RESUME-TASK-ERR')

        console.log('Resume all tasks: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    if (answer.status && answer.status === "RESUMED") {
                        return this.Notification('RESUME-TASK-WARN')
                    }
                    return this.props.addTaskId("ADD_TASKS_ID", answer.tasks)
                }

                return console.log("ERROR: header-variable-options.jsx.jsx: resume: response.data not available: "+ JSON.stringify(response))

            })
            .catch(reject => {

                return console.log("ERROR: resumming tasks failed: ", reject)

            })

    }

    stop = () => {
        //---- API. Pause all Tasks ----
        const id = this.props.task_id,
            options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'stop', [id])

        //Notification
        if (!options) return this.Notification('STOP-TASK-ERR', `Can't to stop ${id} task`)

        console.log(`Stopped the task ${id}: ${JSON.stringify(options)}`)

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                console.log(answer)
                if (answer) {
                    if (answer.status && answer.status === "STOPPED") {
                        return this.Notification('STOP-TASK-WARN')
                    }
                    if (answer.message) {
                        return answer.message.forEach( message => {
                            this.Notification('STOP-TASK-WARN-MESSAGE', {message,id})
                        })
                    }
                    this.Notification('STOP-TASK-WARN-MESSAGE', {message:"Stopped!",id})
                    return this.props.addTaskId("ADD_TASKS_ID", answer.runningTasks)
                }

                return console.log("ERROR: header-variable-options.jsx.jsx: stop: response.data not available: "+ JSON.stringify(response))

            })
            .catch(reject => {

                return console.log("ERROR: stopping task failed: ", reject)

            })
    }

    pause = () => {
        //---- API. Pause all Tasks ----
        const options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'pause')

        //Notification
        if (!options) return this.Notification('PAUSE-TASK-ERR')

        console.log('Pause all tasks: '+ JSON.stringify(options))

        let answer
        axios(options)
            .then(response => {

                answer = response.data

                if (answer) {
                    if (answer.status && answer.status === "PAUSED") {
                        return this.Notification('PAUSE-TASK-WARN')
                    }
                    answer.runningTasks.forEach( task => {
                        this.Notification('PAUSE-TASK-WARN-MESSAGE', task)
                    })
                    return this.props.addTaskId("ADD_TASKS_ID", answer.tasks)
                }

                return console.log("ERROR: header-variable-options.jsx.jsx: pause: response.data not available: "+ JSON.stringify(response))

            })
            .catch(reject => {

                console.log(reject)
                return console.log("ERROR: pausing task failed: ", reject)

            })
    }

    update = () => {

        switch(location.pathname) {

            case "/settings-deploy":

                return this.Notification('SETTING-UPDATE')

            case "/aws":

                return this.Notification('AWS-UPDATE')

            default:

                if (!this.props.task_id) {

                    return this.Notification('ID-UPDATE-ERR')

                }

                //---- API. Update Task status ----
                const id = this.props.task_id.id,
                    options = library_c.constructorRequestOption(this.props.api.jenkins_tool, 'get_task_status', [id])

                //Notification
                if (!options) return this.Notification('UPDATE-TASK-ERR')

                console.log('Getting task status: '+ JSON.stringify(options))

                let answer
                axios(options)
                    .then(response => {

                        answer = response.data

                        if (answer) {
                            return this.props.updateOutput("UPDATE_OUTPUT", answer.task),
                                this.props.addTaskId("ADD_TASKS_ID", answer.runningTasks)
                        }

                        return console.log("ERROR: header-variable-options.jsx.jsx: update: response.data not available: "+ JSON.stringify(response))

                    })
                    .catch(reject => {

                        return console.log("ERROR: getting task status is failed: " + JSON.stringify(reject))

                    })


        }


    }

    showCart = () => {
        return this.props.showCartFunc()
    }

    constructorObjects(option) {

        const type = option.object

        switch(type) {

            case "input-search":

                return (
                    <div key={option.id} className="options options-search">
                        <input placeholder="Search" className="default search" onChange={this.searchingText}/>
                    </div>
                )

            case "button-icon":

                return (
                    <div key={option.id} className="options">
                        <a title={option.desc} onClick={this[option.name]} className="console-options-a"><i className={option.icon}></i></a>
                    </div>
                )


            case "table-view":

                return (
                    <div key={option.id} className="options d-flex flex-row row-wrap justify-content-between">
                        <TableView view={option.view} />
                    </div>
                )

            case "cart":

                return (
                    <div key={option.id} className="cart-label-btn d-flex flex-column align-items-center" onClick={this.showCart.bind(this)}>
                        <i className={option.icon}></i>
                    </div>
                )

            default:
                return null

        }


    }

    showList() {

        let variable = this.props.options

        if (variable.length > 0) {
            return variable.filter(variable => variable.disabled && variable.show).map( variable => {
                return this.constructorObjects(variable)
            })
        }
        return null
    }

    showMenu () {

        return (
            <div className="options-block d-flex flex-row align-items-center">
                {this.showList()}
            </div>
        )

    }

    render () {
        return this.showMenu()
    }
}

export default connect(state => ({
    options: state.options.variable,
    task_id: state.building.task_id,
    server:state.appinfo.server,
    api: state.appinfo.api,
}),
    dispatch => ({
        searchingSrvs: (value) => {
            dispatch({type: 'SEARCHING', payload: value})
        },
        updateOutput: (type, output) => {
            dispatch({type: type, output: output})
        },
        notifyMe: (data) => {
            dispatch({type: 'NOTIFY_ME', data: data})
        },
        addTaskId: (type, data) => {
            dispatch({type: type, data})
        },
        showCartFunc: () => {
            dispatch({type: "SHOW_CART"})
        },
    }))(HeaderVariableOptionsList);