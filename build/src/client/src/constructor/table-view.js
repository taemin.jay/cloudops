import React, {Component} from 'react';
import {connect} from 'react-redux';


class TableView extends Component {

    constructor (props) {
        super(props)
        this.state = {
            active: props.tableView
        }
    }

    changeView = (type) => {
        return this.setState({
            active: type
        }), this.props.changeViewTable(type)
    }

    showOptions(view, active) {
        return view.map( (view, index) => {
            let classname = null
            if (view.type === active) classname="active"
            return <a key={index} onClick={this.changeView.bind(this,view.type)} className={classname}><i className={view.icon}></i></a>
        })

    }


    render () {
        return this.showOptions(this.props.view, this.state.active)
    }
}

export default connect(state => ({
        tableView: state.options.view
    }),
    dispatch => ({
        changeViewTable: (view) => {
            dispatch({type: 'CHANGE_VIEW', view})
        },
    }))(TableView);