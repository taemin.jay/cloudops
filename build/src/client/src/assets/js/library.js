//Получение ключей объекта. Есть возможность задать исключения. Уровень вложения - 1
const getParamsKeys = (json_source, array_excludes) => {

    const all_keys = Object.keys(json_source)

    let keys

    if (array_excludes) {
        keys = all_keys.filter( key => !array_excludes.includes(key))
    } else {
        keys = all_keys
    }

    return keys
}

//Получение ключей и их значений. На выходе будет массив из ключей и изначальный json

const getValuesJson = (json_source) => {

    if (json_source instanceof Object) {

        const keys = Object.keys(json_source)

        return {keys:keys, json: json_source}

    }

    return false

}

//Получение словаря (json) с default значений ключей по заданному json
const getJsonParamsDefaultValues = (array_keys, json_source) => {

    let json = {}

    array_keys.forEach( (key) => {

        json[key] = json_source[key]
    })

    return json

}

//Получение значения заданного ключа
const getValueJsonByKey = (keys, source) => {

    let value = source

    if (Array.isArray(keys)) {
        keys.forEach( key => {
            value = value[key]
        })
    } else {
        value = value[keys]
    }

    return value

}

//Фильтр значений объектов в json. Уровень вложения - 1. array_filters - значений исключений
const getJsonFilterValue = (json_source, array_filters) => {

    const keys = Object.keys(json_source)

    let json = {}

    keys.forEach( key => {
        array_filters.forEach( filter => {
            const regexp = new RegExp(filter, 'gi');
            json_source[key].match(regexp) ? (json[key] = json_source[key]) : null
        })
    })

    console.log(json)

    return json

}

//Фильтр значений объектов в json. Уровень вложения - 1. array_filters - [{'key':'value'},...]
const getArrayOfJsonFilterByJson = (arrayOfJson_source, array_filters) => {

    let filter_keys

    let json

    array_filters.forEach( filter => {
        filter_keys = Object.keys(filter)
        return filter_keys.forEach( key => {
            json = arrayOfJson_source.filter( json => {
                return(json[key] === filter[key])})
        })
    })

    return json

}

//получение значения ключа по json
const getKeyValueJson = (array_json_source, key) => {

    let values = []

    array_json_source.forEach( json => {
        values.push(json[key])
    })

    return values
}



//Замена значения ключа в json на указанное значение
const replaceKeyValue = (json_source, keys, value) => {

    if (keys instanceof Array) {

        let tech = json_source

        keys.forEach( (key,index) => {

            if (index === keys.length - 1) {
                tech[key] = value
            } else {
                tech = tech[key]
            }

        })

    } else {

        json_source[keys] = value
    }

    return json_source
}

//получение значения ключа
//получение части json из всего json по заданному пути
const getValueJson = (json_source, json_nameconvention, name_json_format, name_destination_key) => {

    const path = json_nameconvention[name_json_format][name_destination_key]
    let output = json_source

    path.forEach( index => {
        output = output[index]
    })

    return output
}

module.exports = {
    getParamsKeys,
    getJsonParamsDefaultValues,
    getJsonFilterValue,
    replaceKeyValue,
    getValueJson,
    getArrayOfJsonFilterByJson,
    getKeyValueJson,
    getValuesJson,
    getValueJsonByKey
}