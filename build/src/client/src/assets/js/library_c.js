const axios = require("axios/index");

const library = require('./library')

//Получение параметров джобы с применением фильтров: -неизменные параметры, -замена значений объектов по заданным правилам
const getServiceDefaultParams = (json_source, json_nameconvention, name_json_format, name_destination_key,  array_excludes, array_rules ) => {

    let object = library.getValueJson(json_source, json_nameconvention, name_json_format, name_destination_key),
        object_keys = library.getParamsKeys(object, array_excludes),
        json_params = library.getJsonParamsDefaultValues(object_keys, object),
        json_params_ApplyRules = getJsonReplaceParamsValuesByRules(json_params, array_rules)

    return {params:json_params_ApplyRules, keys: object_keys}

}

//Получение параметров джобы с применением фильтров: -неизменные параметры, -замена значений объектов по заданным правилам
const getJobParamsJson = (json_source, array_excludes ) => {

    let keys = library.getParamsKeys(json_source, array_excludes),
        json_params = library.getJsonParamsDefaultValues(keys,json_source)

    return {params:json_params, keys: keys}

}

//Замена значений объектов в json по заданным правилам
const getJsonReplaceParamsValuesByRules = (json_source, array_rules) => {

    const keys = Object.keys(json_source)

    keys.forEach( key => {

        json_source = getJsonReplaceParamValueByRules(json_source, key, array_rules)
    })

    return json_source

}

//Замена значения выбранного ключа в json по заданным правилам
const getJsonReplaceParamValueByRules = (json_source, key, array_rules) => {

    array_rules.forEach( rule => {
        const regexp = new RegExp(rule.regexp, 'gi');
        json_source[key].match(regexp) ? (json_source[key] = rule.value) : null
    })

    return json_source

}

//Замена значения ключа в json на указанное значение
const replaceValueInParams = (json_source, key, value) => {

    json_source = library.replaceKeyValue(json_source, key, value)

    return json_source
}

//Проверка на одинаковость action у указанного массива сервисов
const checkSrvsAction = (array_services, cart, nameconvention, action) => {

    if (cart.length === 0) return false

    for (let i=0; i<array_services.length; i++) {

        const value = library.getValueJson(array_services[i], nameconvention.keys, "microservice", "Service")

        const service = cart.find( x => library.getValueJson(x, nameconvention.keys, "microservice", "Service") === value )

        if (service === undefined) {
            console.log("false")
            return false
        }

        if (library.getValueJson(service, nameconvention.keys, "microservice", "Action") !== action) {
            console.log("false")
            return false
        }

    }

    return true

}

//Проверка на наличие объекта в указанном массиве объектов по заданному ключу, т.к. остальные значения объектов
//могут отличаться

const checkObjectInArray = (array_json, object, json_nameconvention, name_json_format, name_destination_key) => {

    let key_value1 = library.getValueJson(object, json_nameconvention, name_json_format, name_destination_key),
        key_value2 = library.getValueJson(object, json_nameconvention, name_json_format, "fullname")

    for (let i=0; i<array_json.length ; i++) {
        let value1 = library.getValueJson(array_json[i], json_nameconvention, name_json_format, name_destination_key),
            value2 = library.getValueJson(array_json[i], json_nameconvention, name_json_format, "fullname")
        if ( value1 === key_value1 && value2 === key_value2  ) return {checking: true, servicename: key_value1, fullname: key_value2}
    }

    return {checking: false}

}

//---- API. Update the console output --------------

const APIupdateConsoleLogs = (url, callback) => {

    let answer

    console.log('Updating Task Status: '+ url)

    axios.get(url)
        .then(response => {

            answer = response.data

            if (answer) {

                callback(answer)

                return answer
            }

        })
        .catch( () => {

            console.log("Couldn't get output. Please, check, that Task ID is correct or errors above")
            return false

        })

    return false

}

//---- AWS. Formate from JSON's array to String ----

const formateAWSJsonToString = (array_json) => {

    let string = ""

    const keys = library.getParamsKeys(array_json[0], ["InstanceId"])

    array_json.forEach( json => {

        string += '"['+json.InstanceId+']'

        for ( let i=0; i<keys.length; i++) {
            string += " " + json[keys[i]]
        }

        string += '"'

    })

    return string

}

//---- Formating Array to table form for Dom presentation

const formateArrayToTableString = (array) => {

    let buff = ""

    array.forEach(item => {
        buff += item + "\n"
    })

    return buff

}

//---- Formating String to Array. Separtion charaters , \n ; | space
const parseExIncludesToArrayFunction = (data) => {
    //разделителем могут быть ,;|\n\s. Для экранирования этих символов использовать \ после них
    /*var regexp = /\s*\n\s*|\s*\|\s*|\s*,\s*|\s*;\s*|\s+/g;*/
    /*var regexp = /\s*(\n|\||,|;|\s+)\s* /g;*/
    /*var regexp = /(\n|\||,|;|\s)+/g;
    excludes = excludes.replace(/(\n|,|;|\||\s)+$/g, '');
    excludes = excludes.replace(/^(\n|,|;|\||\s)+/g, '');
    excludes = excludes.replace(regexp,'","');*/
    var regexp = "(\\n|\\||,|;|\\s)+(?!\\\\)";
    data = data.replace(new RegExp(regexp+"$","gi"), '');
    data = data.replace(new RegExp("^"+regexp,"gi"), '');
    data = data.replace(new RegExp(regexp,"gi"), '","');
    data = data.replace(/\\/gi, '');
    data = data.split('","');
    return data;
}

//---- Save to localStorage

const saveToLocalStorage = (key,data_json) => {

    return localStorage.setItem(key,JSON.stringify(data_json))

}

//---- Remove item from localStorage

const clearItemLocalStorage = (key) => {

    return localStorage.removeItem(key)

}

//---- Clear all localStorage

const clearAllLocalStorage = () => {

    return localStorage.clear()

}

//---- Get item from Storage

const getItemLocalStorage = (key) => {

    return JSON.parse(localStorage.getItem(key))

}

//---- Validation Function

const validationFunction = (data) => {

    if (!data) {
        return false
    }



    return true

}

//---- API. Constructor URL --------------

const constructorRequestOption = (config, api, data) => {

    if (!config) {
        return false
    }

    if (!config.path) {
        config.path = ""
    }

    if (!config.protocol) {
        config.protocol = "http"
    }

    //Все запросы у нас в основном GET и POST. Если нужно отправить некоторую data, то это POST запрос.
    //По этому, если в эту функцию передают переменную data, то метод хардкодом(ниже) меняется на POST,
    //а метод, который был указан в api-config.yml, игнорируется.
    //Но, иногда в качесвте data могут передаваться динамические данные, на основе которых выстраивается url запроса,
    //и по факту никакой data в хидере запроса не передается. Ниже написана проверка на тип data, передаваемой в эту функцию
    //и на основе ее составляется конченый хидер и options для запроса
    let url

    if (data) {
        if (!Array.isArray(data) || (Array.isArray(data) && data[0] instanceof Object)) {
            if (config.port) {
                url = config.protocol + "://" + config.hostname + ":" + config.port + config.path + config.route[api].path
            } else {
                url = config.protocol + "://" + config.hostname + config.path + config.route[api].path
            }
            return {
                url: url,
                method: 'POST',
                data: data
            }
        } else if (Array.isArray(data)) {

            let route = config.route[api].path
            const reg = new RegExp(/(\$)[^\/](.*?)(?=\/|$)/g),
                match = route.match(reg)

            for (let i=0; i<data.length; i++) {
                const reg = new RegExp("\\"+match[i],"gi")
                route = route.replace(reg,data[i])
            }

            if (config.port) {
                url = config.protocol + "://" + config.hostname + ":" + config.port + config.path + route
            } else {
                url = config.protocol + "://" + config.hostname + config.path + route
            }
            return {
                url: url,
                method: config.route[api].method
            }
        }
    }

    if (config.port) {
        url = config.protocol + "://" + config.hostname + ":" + config.port + config.path + config.route[api].path
    } else {
        url = config.protocol + "://" + config.hostname + config.path + config.route[api].path
    }

    return {
        url: url,
        method: config.route[api].method
    }

}

module.exports = {
    getServiceDefaultParams,
    getJsonReplaceParamsValuesByRules,
    replaceValueInParams,
    getJsonReplaceParamValueByRules,
    getJobParamsJson,
    checkSrvsAction,
    checkObjectInArray,
    APIupdateConsoleLogs,
    formateAWSJsonToString,
    formateArrayToTableString,
    parseExIncludesToArrayFunction,
    saveToLocalStorage,
    clearAllLocalStorage,
    clearItemLocalStorage,
    getItemLocalStorage,
    constructorRequestOption,
    validationFunction
}