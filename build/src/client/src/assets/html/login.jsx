var React = require('react');

class LOGIN extends React.Component {

    render() {

        const style = {
            width: "195px"
        }, button = {
            background: "none",
            border: "none"
        }

        return (
            <html lang="en">
            <head>
                <meta charSet="UTF-8"/>
                <title>Cloud Ops</title>
                <link rel="icon" href="favicon.png" type="image/png"/>
                <link rel="stylesheet" href="css/main.css"/>
            </head>
            <body>
            <div className="login-form h-100">
                <div className="h-100 d-flex flex-column align-items-center justify-content-center">
                    <form className="form-validate" method="POST" action="/login">
                        <div className="form-group" style={style}>
                            <input id="login-username"
                                   name="username"
                                   required={true}
                                   data-msg="Please enter your username"
                                   className={`default`}
                                   placeholder="username"
                            />
                        </div>
                        <div className="form-group" style={style}>
                            <input id="login-api"
                                   type="password"
                                   name="password"
                                   required={true}
                                   data-msg="Please enter your password"
                                   className={`default`}
                                   placeholder="password"
                            />
                        </div>
                        <button type="submit" style={button}></button>
                    </form>
                </div>
            </div>
            </body>
            </html>
        );
    }
}

module.exports = LOGIN;
