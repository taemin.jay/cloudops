const initialState = {
    search: "",
    foundItems: [],
    searchingStringAWS: false,
    searchingTemplate: false,
    id: 1
}

export default function (state = initialState, action) {

    switch (action.type) {

        case "SEARCHING":

            console.log("--------------- Searching. Pattern ----------------")

            state.search = action.payload

            if (action.payload.length === 0) {
                state.searchingStringAWS = false
            } else {
                state.searchingStringAWS = true
            }

            console.log(state)
            console.log("----------------------------------------------------")

            return {...state}

        case "FOUNDED":

            console.log("--------------- Searching. Add Founded Item ----------------")

            state.foundItems.push(action.payload)

            console.log(state)
            console.log("-------------------------------------------------------------")

            return {...state}

        case "ENABLE_SEARCH":

            console.log("--------------- Searching. Enable/Disable Template search ----------------")

            state.searchingTemplate = !state.searchingTemplate

            console.log(state)
            console.log("--------------------------------------------------------------------------")

            return {...state}

        case "CLEAN_SEARCH":

            state = {
                search: "",
                foundItems: [],
                searchingStringAWS: false,
                searchingTemplate: false,
                id: 1
            }

            return state

        default:
            return state
    }
}