const initialState = {
    disabled: true,
    id: 1,
    name1: "Cloud",
    name2: "Ops",
    desc: "",
    additional: null,
    server: 'https://0.0.0.0:8008/api/v1',
    get_api_conf: '/api-conf',
    get_web_conf: '/web-conf',
    api: false,
    design: {
        show: {
            delay: 100 //Default timeout for the showing item of list on page
        }
    }
}

export default function (state = initialState, action) {

    const data = action.data


    switch (action.type) {

        case "APP_SET_API":

            console.log("------------------- APP. Setting API -------------------")
            state.api = data.api

            console.log(state)
            console.log("--------------------------------------------------------")

            return {...state}

        case "CHANGE_STATUS_TOOL":
            console.log("----------------- APP. Available tool ------------------")

            state.api[action.tool_name]["available"] = action.status

            console.log(state)
            console.log("--------------------------------------------------------")

            return {...state}

        default:
            return state
    }
}