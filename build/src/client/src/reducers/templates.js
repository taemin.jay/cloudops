import axios from "axios/index";

const initialState = {
    disabled: true,
    template_id: 1,
    templates: [],
    templates_enable: false,
    searchTemplateServices: [],
    search_template_ids: []
}

export default function (state = initialState, action) {

    const templates = action.templates,
        enable = action.enable,
        template = action.template,
        id = action.id


    switch (action.type) {

        case "ADD_TEMPLATES":

            console.log("--------------- TEMPLATES.ADD Templates ---------------")
            
            state.templates = templates
            state.templates_enable = enable

            console.log(state)

            return {...state}

        case "SEARCH_TEMPLATE":

            console.log("------------- TEMPLATES.Searching pattern --------------")

            if (state.search_template_ids.indexOf(id) === -1) {
                state.search_template_ids.push(id)
                template.forEach( service => {

                    state.searchTemplateServices.push(service.Service)

                })
            } else {
                state.search_template_ids.splice(state.search_template_ids.indexOf(id),1)
                template.forEach( service => {

                    state.searchTemplateServices.splice(state.searchTemplateServices.indexOf(service.Service), 1)

                })

            }

            console.log(state.searchTemplateServices, state.search_template_ids)

            return {...state}

        case "CHANGE_TEMPLATE_ID":

            console.log("------------- TEMPLATES.Change template id -------------")

            state.template_id = id

            console.log(state.template_id)

            return {...state}

        default:
            return state
    }
}