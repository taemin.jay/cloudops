const initialState = {
    disabled: true,
    show: false,
    id: 1,
    name: "",
    desc: "",
    additional: null,
    routers: [
        {
            disabled: true,
            id: 1,
            name: "Home",
            desc: "Dashboard",
            link: "/",
            icon: "home icon",
            extMenu: false,
            extRouters: []
        },
        {
            disabled: true,
            id: 2,
            name: "Deploy",
            desc: "Deploy",
            link: "/deploy",
            icon: "jenkins icon",
            extMenu: true,
            extRouters: [
                {
                    disabled: true,
                    id: 1,
                    name: "Deploy",
                    desc: "Deploy",
                    link: "/deploy-tool",
                    icon: "servicestack icon",
                    extMenu: false,
                    extRouters: []
                },
                {
                    disabled: true,
                    id: 2,
                    name: "Console",
                    desc: "Console",
                    link: "/console",
                    icon: "tv icon",
                    extMenu: false,
                    extRouters: []
                },
                {
                    disabled: true,
                    id: 3,
                    name: "Templates",
                    desc: "Templates",
                    link: "/templates",
                    icon: "save outline icon",
                    extMenu: false,
                    extRouters: []

                },
                {
                    disabled: true,
                    id: 4,
                    name: "History",
                    desc: "History",
                    link: "/history",
                    icon: "history icon",
                    extMenu: false,
                    extRouters: []
                },
                {
                    disabled: true,
                    id: 5,
                    name: "Settings",
                    desc: "Settings",
                    link: "/settings-deploy",
                    icon: "cogs icon",
                    extMenu: false,
                    extRouters: []
                }
            ]
        },
        {
            disabled: true,
            id: 3,
            name: "AWS",
            desc: "AWS",
            link: "/aws",
            icon: "aws icon",
            extMenu: false,
            extRouters: []
        },
        {
            disabled: true,
            id: 4,
            name: "Splunk",
            desc: "Splunk",
            link: "/splunk",
            icon: "terminal icon",
            extMenu: false,
            extRouters: []
        },
        {
            disabled: true,
            id: 5,
            name: "Settings",
            desc: "Settings",
            link: "/settings",
            icon: "cogs icon",
            extMenu: false,
            extRouters: []
        }
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case "SHOW_MENU":
            state.show = action.payload
            return {...state}
        default:
            return state
    }
}