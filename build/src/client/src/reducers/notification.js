const initialState = {
    disabled: true,
    id: 1,
    data: null
}

export default function (state = initialState, action) {

    switch (action.type) {

        case "NOTIFY_ME":

            console.log("------------------------ Notify -------------------------")

            state.data = action.data

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "CLEAN_NOTIFY":

            state.data = null

            return {...state}

        default:
            return state
    }
}