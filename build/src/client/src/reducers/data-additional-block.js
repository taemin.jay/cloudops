const initialState = {
    disabled: true,
    show: false,
    type: null,
    data: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case "SHOW_ADDITIONAL_BLOCK":

            state.show = true
            state.data = null
            state.type = action.typeDataBlock

            return {...state}

        case "SET_DATA_ADDITIONAL_BLOCK":

            state.data = action.data

            return {...state}

        case "HIDE_ADDITIONAL_BLOCK":

            state.show = false
            state.data = null
            state.type = null

            return {...state}

        default:
            return state
    }
}