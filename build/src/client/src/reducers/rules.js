import library_c from '../assets/js/library_c'

const initialState = {
    disabled: true,
    id: 1,
    name: "",
    desc: "",
    additional: null,
    servicename: [
        {
            disabled: true,
            fullname: "ConfigRefresh",
            name: "ConfigRefresh"

        },
        {
            disabled: true,
            fullname: "StandaloneDeployBrwHZ",
            name: "StandaloneDeployBrwHZ"

        }
    ],
    params: {
        replace: [
            {
                disabled: true,
                regexp: "\\$VERSION\\$",
                value: "latest"
            }
        ],
        excludes: [
            "Service",
            "Action",
            "token"
        ]
    },
    servicelist: {
        rules: [
            "include",
            "exclude"
        ],
        filters: [
            {
                name: "Microservices",
                disabled: true,
                filters: [
                    {
                        key: ["fullname"],
                        id: 1,
                        disabled: true,
                        rule: "include",
                        value: "AppManage.2"
                    }
                ]
            },
            {
                name: "All the rest",
                disabled: true,
                filters: [
                    {
                        key: ["fullname"],
                        id: 1,
                        disabled: true,
                        rule: "exclude",
                        value: "AppManage.2"
                    }
                ]
            }

        ]
    }
}

export default function (state = initialState, action) {

    switch (action.type) {

        case "CHANGE_FILTER_ENABLE":

            state.servicelist.filters.find( x=> x.name === action.filterName).disabled = action.disabled

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "CHANGE_RULE_ENABLE":

            state.servicelist.filters
                .find( x=> x.name === action.filterName).filters
                .find( x => x.id === action.idRule ).disabled = action.disabled

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "CHANGE_RULE":

            state.servicelist.filters
                .find( x=> x.name === action.filterName).filters
                .find( x => x.id === action.idRule ).rule = action.rule

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "CHANGE_VALUE":

            state.servicelist.filters
                .find( x => x.name === action.filterName).filters
                .find( x => x.id === action.idRule ).value = action.value

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "CHANGE_KEY":

            console.log(action.filterName,action.idRule)
            state.servicelist.filters
                .find( x => x.name === action.filterName).filters
                .find( x => x.id === action.idRule ).key = action.value.split(",")

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "CHANGE_FILTER_NAME":

            state.servicelist.filters
                .find( x => x.name === action.filterName).name = action.value

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "ADD_RULE":

            let filter = state.servicelist.filters
                .find( x => x.name === action.filterName).filters,
                newItem = {
                            key: ["Set the key/s (split by comma)"],
                            id: filter.length + 1,
                            disabled: false,
                            rule: "include",
                            value: "Set the filtering string"
                        }

            filter.push(newItem)

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        case "ADD_FILTER":

            const newFilter = {
                    name: "Set Name",
                    disabled: false,
                    filters: [
                        {
                            key: ["Set json path (split by comma)"],
                            id: 1,
                            disabled: false,
                            rule: "include",
                            value: ""
                        }
                    ]
                }

            state.servicelist.filters.push(newFilter)

            library_c.saveToLocalStorage("rules", state)

            return {...state}

        default:

            let rules = library_c.getItemLocalStorage("rules")

            if (rules !== null) {

                state = rules

            }

            return {...state}
    }
}