import library_c from '../assets/js/library_c'

const initialState = {
    disabled: true,
    task_id: false, // task_id: {id: answer, env: env} {id: 1369, env: "aws_beta"} {id: 1367, env: "aws_beta"},{id: 1368, env: "aws_beta"}
    output: null,
    timer: false,
    task_queue: [],
    runningTasks: []
}

export default function (state = initialState, action) {

    let data = action.data,
        output = action.output,
        url = action.url,
        building

    switch (action.type) {

        case "ADD_TASK_ID":

            console.log("------------- BUILDING.ADD Task ID to queue -------------")

            if (state.task_id) {

                state.task_queue.push(state.task_id)
                state.task_id = data

            } else {

                state.task_id = data

            }

            state.timer = true

            library_c.saveToLocalStorage('building',state)

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "ADD_TASKS_ID":

            console.log("------------- BUILDING.ADD Tasks IDs to queue -------------")


            if (!state.task_id && data[0].task_id) state.task_id = data[0].task_id

            state.task_queue = []
            state.runningTasks = data ? data : []

            for (let i=1; i<data.length; i++) {
                state.task_queue.push(data[i].task_id)
            }

            state.timer = true

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "UPDATE_OUTPUT":

            console.log("------------------ BUILDING.GET output ------------------")

            state.output = output

            console.log("------------ BUILDING.Checking status of task -----------")

            if (output.status === "SUCCESS" || output.status === "FAILED" || output.status === "ERROR" || output.status === "NON CONSISTENT" || output.status === "PAUSED" || output.status === "STOPPED") {

                console.log("Task is finished!")

                if (state.task_queue.length > 0) {

                    console.log("Change task id")
                    console.log("before: ",state.task_id)

                    state.task_id = state.task_queue[0]
                    state.task_queue.shift()

                    console.log("after: ",state.task_id)

                    library_c.saveToLocalStorage('building',state)

                } else {

                    console.log("All tasks are completed")

                    state.task_id = false
                    state.timer = false

                    library_c.clearItemLocalStorage('building')

                }

            }

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "SHOW_TASK_ID":

            console.log("------------- BUILDING.Show Task ID  -----------------")

            state.task_id = data

            state.timer = true

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        default:

            building = library_c.getItemLocalStorage("building")

            if (building !== null) {

                state = building

            }

            return {...state}

    }
}