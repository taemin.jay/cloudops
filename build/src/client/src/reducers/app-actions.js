const initialState = {
    actions: [
        {
            id: 1,
            name: "status",
            disabled: true,
            fullname: ["AppManage.2", "Other"],
            choose: false
        },
        {
            id: 2,
            name: "update",
            disabled: true,
            fullname: ["AppManage.2", "ConfigRefresh"],
            choose: false
        },
        {
            id: 3,
            name: "restart",
            disabled: true,
            fullname: ["AppManage.2"],
            choose: false
        },
        {
            id: 4,
            name: "down",
            disabled: false,
            fullname: ["AppManage.2"],
            choose: false
        },
        {
            id: 5,
            name: "scale",
            disabled: false,
            fullname: ["AppManage.2"],
            choose: false
        },
        {
            id: 6,
            name: "run-job",
            disabled: false,
            fullname: ["AppManage.2"],
            choose: false
        },
        {
            id: 7,
            name: "static",
            disabled: false,
            fullname: ["AppManage.2"],
            choose: false
        },
        {
            id: 8,
            name: "stop",
            disabled: true,
            fullname: ["Other"],
            choose: false
        },
        {
            id: 9,
            name: "start",
            disabled: true,
            fullname: ["Other"],
            choose: false
        },
        {
            id: 10,
            name: "deploy",
            disabled: true,
            fullname: ["Other"],
            choose: false
        }
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case "ENABLE_DISABLE_ACTION":

            console.log(action.action)
            state.actions.find( x => x.name === action.action).disabled = !state.actions.find( x => x.name === action.action).disabled

            return {...state}

        default:
            return state
    }
}