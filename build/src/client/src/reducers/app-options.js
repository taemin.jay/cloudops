import library_c from "../assets/js/library_c";

const initialState = {
    disabled: true,
    id: 1,
    name: "",
    desc: "",
    view: "grid",
    additional: null,
    main: [
        {
            disabled: true,
            id: 2,
            name: "Console",
            desc: "Console",
            type: "Console-check",
            link: "/console",
            icon: "tv icon"
        },
        {
            disabled: true,
            id: 3,
            name: "VPN",
            desc: "GD",
            link: "/vpn",
            type: "IP-check",
            ip: "172.18.171.72",
            icon: "wifi icon"
        },
        {
            disabled: true,
            id: 4,
            name: "VPN",
            desc: "JCP",
            link: "/vpn",
            type: "IP-check",
            ip: "10.248.66.200",
            icon: "wifi icon"
        },
        {
            disabled: true,
            id: 5,
            name: "Logout",
            desc: "Logout",
            type: "Logout",
            link: "/logout",
            icon: "power off icon"
        }
    ],
    variable: [
        {
            disabled: true,
            show: false,
            id: 1,
            name: "search",
            object: "input-search",
            icon: "search icon",
            path: ["/deploy", "/deploy-tool","/aws"]
        },
        {
            disabled: true,
            show: false,
            id: 2,
            name: "play",
            desc: "resume",
            object: "button-icon",
            icon: "play circle outline icon",
            path: ["/console"]
        },
        {
            disabled: true,
            show: false,
            id: 3,
            name: "stop",
            desc: "stop",
            object: "button-icon",
            icon: "stop circle icon",
            path: ["/console"]
        },
        {
            disabled: true,
            show: false,
            id: 4,
            name: "pause",
            desc: "pause",
            object: "button-icon",
            icon: "pause circle outline icon",
            path: ["/console"]
        },
        {
            disabled: true,
            show: false,
            id: 5,
            name: "update",
            desc: "update",
            object: "button-icon",
            icon: "sync alternate icon",
            path: ["/console","/aws"]
        },
        {
            disabled: true,
            show: false,
            id: 6,
            name: "view",
            object: "table-view",
            view: [
                {
                    icon: "th icon",
                    type: "grid"
                },
                {
                    icon: "th list icon",
                    type: "list"
                }
            ],
            path: ["/deploy", "/deploy-tool"]
        },
        {
            disabled: true,
            show: false,
            id: 7,
            name: "Cart",
            object: "cart",
            icon: 'shopping basket icon',
            path: ["/deploy", "/console", "/history", "/templates", "/deploy-tool"]
        }
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {

        case "SHOW_VARIABL_OPTIONS":

            const path = action.path

            for (let i=0; i<state.variable.length; i++) {

                if (state.variable[i].path.indexOf(path) !== -1) {
                    state.variable[i].show = true
                } else {
                    state.variable[i].show = false
                }

            }

            return {...state}

        case "CHANGE_VIEW":

            state.view = action.view
            library_c.saveToLocalStorage("view", action.view)

            return {...state}

        case "CHANGE_VPN":

            let keys = Object.keys(action.data), length

            state.main = state.main.filter( x => x.name !== "VPN")
            length = state.main.length

            keys.forEach( key => {
                state.main.splice(length - 1, 0, {
                    disabled: true,
                    name: "VPN",
                    desc: action.data[key].name,
                    link: "/vpn",
                    type: "IP-check",
                    ip: action.data[key].ip,
                    icon: "wifi icon"
                })
            })

            return {...state}

        default:

            const view = library_c.getItemLocalStorage('view')
            if (view) state.view = view

            return {...state}
    }
}