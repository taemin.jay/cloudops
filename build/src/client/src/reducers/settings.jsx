import library_c from "../assets/js/library_c";

const initialState = {
    disabled: true,
    deploy: {
        services: {
            updating: false,
            env: null,
            simpleIncludes: null,
            excludes: null
        }
    },
    aws: {
        username:'centos',
        password: '',
        hostAWS_ssh_port: '22',
        hostAWS_key_prod: {
            disabled: false,
            filename: "...ssh-key for prod",
            key:``
        },
        hostAWS_key_nonprod: {
            disabled: false,
            filename: "...ssh-key for dev",
            key:``
        },
        auto_ssh: false,
        auto_cloudwatch: false

    },
    splunk: {
        raw_count: 20,
        urls: {
            description: "Without 'http://'",
            list: [
                {
                    name: "LENA",
                    type: "prod",
                    domain: "splunkprodweb-len.jcpenney.com"
                },
                {
                    name: "COLA",
                    type: "prod",
                    domain: "splunkprodweb-col.jcpenney.com"
                },
                {
                    name: "DEV",
                    type: "dev",
                    domain: "splunk.dp-dev.jcpcloud2.net:8000"
                }
            ]
        }
    },
    homepage: {
        auto_check_tools: true,
        timeout: 10000
    }
}

export default function (state = initialState, action) {

    let buff, settings

    switch (action.type) {

        case "STOP_UPDATING":

            console.log("------------- Settings.DEPLOY. Stop Updating Services -------------")

            state.deploy.services.updating = false

            state.deploy.services.env = null
            state.deploy.services.simpleIncludes = null
            state.deploy.services.excludes = null

            console.log(state.deploy)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "SETTINGS_SET_INC&&EXCLUDES":

            console.log("------------- Settings.DEPLOY. Set includes and excludes -------------")

            state.deploy.services.env = action.data.env
            state.deploy.services.simpleIncludes = action.data.simpleIncludes
            state.deploy.services.excludes = action.data.excludes

            console.log(state.deploy)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "START_UPDATING":

            console.log("------------- Settings.DEPLOY. Start Updating Services -------------")

            state.deploy.services.updating = true

            console.log(state.deploy)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "AWS_USERNAME":

            console.log("------------- Settings.AWS. Change username -------------")

            state.aws.username = action.value

            console.log(state.aws)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "AWS_autoSSH":

            console.log("------------- Settings.AWS. Change SSH Auto -------------")

            state.aws.auto_ssh = action.value

            console.log(state.aws)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "AWS_autoCLOUDW":

            console.log("------------- Settings.AWS. Change CloudWatch Auto -------------")

            state.aws.auto_cloudwatch = action.value

            console.log(state.aws)

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "SPLUNK_COUNT_RAW":

            console.log("------------- Settings.SPLUNK. Change raw count -------------")

            state.splunk.raw_count = action.value

            console.log(state.splunk)
            console.log("-------------------------------------------------------------")

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "SPLUNK_DOMAINS":

            console.log("------------- Settings.SPLUNK. Change domain -------------")

            buff = state.splunk.urls.list.find( x => x.name === action.name)

            buff.domain = action.domain

            console.log(state.splunk)
            console.log("-------------------------------------------------------------")

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        case "AWS_KEY":

            console.log("------------- Settings. AWS. Change key -------------")

            console.log(state.aws[action.key])

            console.log(action)
            state.aws[action.key].filename = action.filename
            state.aws[action.key].disabled = true
            state.aws[action.key].key = `${action.value}`

            console.log(state.aws[action.key])

            console.log("-------------------------------------------------------------")

            library_c.saveToLocalStorage("settings", state)

            return {...state}

        default:

            settings = library_c.getItemLocalStorage("settings")

            if (settings !== null) {

                state = settings

            }

            return {...state}

    }
}