import {combineReducers} from 'redux';
import InfoReducers from './application';
import RouterReducer from './router';
import AppOptionsReducer from './app-options'
import RulesReducer from './rules'
import AppActionsReducer from './app-actions'
import CartReducer from './cart'
import AppNameConventionReducer from './app-name-convention'
import SearchingReducer from './searching'
import BuildingReducer from './building'
import TemplatesReducer from './templates'
import DataAdditionalReducer from "./data-additional-block";
import SettingsReducer from './settings'
import NotificationReducer from './notification'

const allReducers = combineReducers({
    appinfo: InfoReducers,
    router: RouterReducer,
    options: AppOptionsReducer,
    rules: RulesReducer,
    actions: AppActionsReducer,
    cart: CartReducer,
    nameconvention: AppNameConventionReducer,
    searching: SearchingReducer,
    building: BuildingReducer,
    templates: TemplatesReducer,
    additionaldatablock: DataAdditionalReducer,
    settings: SettingsReducer,
    notification: NotificationReducer
});

export default allReducers