const initialState = {
    key_servicename: "Service",
    key_action: "Action",
    key_fullname: "fullname",
    key_params: "params",
    keys: {
        microservice: {
            fullname: ["fullname"],
            params: ["params"],
            Service: ["params", "Service"],
            Action: ["params", "Action"]
        },
        task_status: {
            fullname: ["job", "fullname"],
            params: ["job", "params"],
            Service: ["job", "params", "Service"],
            Action: ["job", "params", "Action"]

        }
    }

}

export default function (state = initialState, action) {
    switch (action.type) {
        default:
            return state
    }
}

