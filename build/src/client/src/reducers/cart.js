import library_c from '../assets/js/library_c'

const initialState = {
    cart: [],
    env: "",
    id: 1,
    icon: 'shopping basket icon',
    showCart: false,
    action_CtrlA: null,
    options: [
        {
            name: "start",
            disabled: true,
            desc: "start",
            icon: "play icon"
        },
        {
            name: "delete_all",
            disabled: true,
            desc: "delete",
            icon: "trash alternate outline icon"
        },
        {
            name: "save_temp",
            disabled: true,
            desc: "save",
            icon: "save outline icon"
        },
        {
            name: "upload",
            disabled: true,
            desc: "upload",
            icon: "cloud upload icon"
        },
        {
            name: "download",
            desc: "download",
            disabled: true,
            icon: "cloud download icon"
        }
    ]
}

export default function (state = initialState, action) {

    let keys, servicename, job, jobs, param, rules, buff, cart

    switch (action.type) {

        case "ADD_TO_CART":

            console.log("------------------- CART.ADD to Cart -------------------")
            keys = action.keys
            rules = action.rules.params

            job = JSON.parse(JSON.stringify(action.payload)) // create new Object without symlinks

            job = library_c.replaceValueInParams(job, keys.keys.microservice.Action, action.action) // set action for the choosen job

            console.log("Setting the default values for parametres.")
            console.log("Before: "+ JSON.stringify(job))
            let def_params = library_c.getServiceDefaultParams(job, keys.keys, "microservice", "params", rules.excludes, rules.replace) // get objects keys of the job parametres
            def_params.keys.forEach( key => {
                job[keys.key_params][key] = def_params.params[key] // set default value for each of the jobs parametres
            })
            console.log("After: "+ JSON.stringify(job))

            job['id'] = state.id
            state.cart.push(job)
            state.id ++

            console.log("Added to cart:")
            console.log(state.cart)
            console.log("---------------------------------------------------------")

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case "REMOVE_FROM_CART":

            console.log("---------------- CART.Remove from Cart -----------------")
            keys = action.keys

            // Проверка на наличие Service. Если нет, то используется fullname
            if (action.payload[keys.key_params][keys.key_servicename]) {
                servicename = action.payload[keys.key_params][keys.key_servicename]
                state.cart = state.cart.filter(x => {
                    x[keys.key_params][keys.key_servicename] === servicename ? state.id-- : null
                    return x[keys.key_params][keys.key_servicename] !== servicename
                })
            } else {
                servicename = action.payload[keys.key_fullname]
                state.cart = state.cart.filter(x => {
                    x[keys.key_fullname] === servicename ? state.id-- : null
                    return x[keys.key_fullname] !== servicename
                })
            }

            state.cart.forEach( (x, index) => {
                x.id = index + 1
            })

            console.log("Deleting from the cart:")
            console.log(state.cart)

            console.log("---------------------------------------------------------")

            if (state.cart.length > 0) {
                library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})
            } else {
                library_c.clearItemLocalStorage("cart")
            }

            return {...state}

        case "REMOVE_ALL_FROM_CART":

            console.log("--------------- CART.Remove All From Cart --------------")
            state.cart = []
            state.id = 1

            console.log(state)

            console.log("---------------------------------------------------------")

            library_c.clearItemLocalStorage("cart")

            return {...state}

        case 'CHANGE_ACTION_CART':

            console.log("------------------- CART.Change Cart -------------------")

            keys = action.keys

            // Проверка на наличие Service. Если нет, то используется fullname
            if(action.payload[keys.key_params][keys.key_servicename]) {
                servicename = action.payload[keys.key_params][keys.key_servicename]
                job = state.cart.find( x => x[keys.key_params][keys.key_servicename] === servicename)
            } else {
                servicename = action.payload[keys.key_fullname]
                job = state.cart.find( x => x[keys.key_fullname] === servicename)
            }

            job = library_c.replaceValueInParams(job, keys.keys.microservice.Action, action.action)

            console.log("Changing the cart:")
            console.log(state.cart)
            console.log("---------------------------------------------------------")

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case 'SET_ENV_TO_CART':

            console.log("----------------- CART.Set environments ----------------")

            if (state.env !== action.payload) {
                state.cart = []
                state.id = 1

                console.log("Setting environment")
                state.env = action.payload

                library_c.clearItemLocalStorage("cart")

            }

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case 'CHANGE_PARAM_CART':

            console.log("---------------- CART.Changes parametres ---------------")
            keys = action.keys
            param = action.param

            // Проверка на наличие Service. Если нет, то используется fullname
            if(action.payload[keys.key_params][keys.key_servicename]) {
                servicename = action.payload[keys.key_params][keys.key_servicename]
                job = state.cart.find( x => x[keys.key_params][keys.key_servicename] === servicename)
            } else {
                servicename = action.payload[keys.key_fullname]
                job = state.cart.find( x => x[keys.key_fullname] === servicename)
            }

            if (job) {
                job = library_c.replaceValueInParams(job, [keys.key_params, param], action.value)
                console.log("Changing the cart:")
                console.log(state.cart)
            } else {
                console.log("Cart haven't the job")
            }

            console.log("---------------------------------------------------------")

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case 'CHANGE_ACTION_CART_CTRLA':

            console.log("--------- CART.Changes Action for ALL services ---------")

            if (state.action_CtrlA === action.payload) {

                state.action_CtrlA = ""

            } else {

                state.action_CtrlA = action.payload

            }

            console.log(state)

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case 'ADD_SELECTED_SRVS_CART':

            state.action_CtrlA = action.action

            console.log("----------- CART.ADD Selected services to Cart ----------")

            console.log(action)

            keys = action.keys
            rules = action.rules.params
            jobs = JSON.parse(JSON.stringify(action.payload)) // create new Object without symlinks

            console.log("---> Setting the default values for parametres.")
            jobs.forEach( job => {
                job = library_c.replaceValueInParams(job, keys.keys.microservice.Action, action.action) // set action for the choosen job

                let def_params = library_c.getServiceDefaultParams(job, keys.keys, "microservice", "params", rules.excludes, rules.replace) // get objects keys of the job parametres
                def_params.keys.forEach( key => {
                    job[keys.key_params][key] = def_params.params[key] // set default value for each of the jobs parametres
                })

                let checking = library_c.checkObjectInArray(state.cart, job, keys.keys, "microservice", "Service") //  проверяет наличие сервиса в корзине, если есть, то возвращает true

                // Если в корзине существует сервис из списка к добавлению, то следующая функция заменит экшн у сервиса в корзине
                if (checking.checking) {

                    buff = state.cart.find( x => x[keys.key_fullname] === checking.fullname && x[keys.key_params][keys.key_servicename] === checking.servicename)
                    library_c.replaceValueInParams(buff, keys.keys.microservice.Action, action.action) // заменяет Action у сервиса в корзине

                } else {

                    job['id'] = state.id
                    state.cart.push(job)
                    state.id++

                }

            })

            console.log("Added to cart:")
            console.log(state.cart)
            console.log("---------------------------------------------------------")

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case "REMOVE_SELECTED_FROM_CART":

            console.log("---------- CART.Remove Selected Srvs from Cart ---------")
            keys = action.keys

            action.payload.forEach( service => {

                // Проверка на наличие Service. Если нет, то используется fullname
                if(service[keys.key_params][keys.key_servicename]) {
                    servicename = service[keys.key_params][keys.key_servicename]
                    state.cart = state.cart.filter( x => {
                        x[keys.key_params][keys.key_servicename] === servicename ? state.id -- : null
                        return x[keys.key_params][keys.key_servicename] !== servicename
                    } )
                } else {
                    servicename = service[keys.key_fullname]
                    state.cart = state.cart.filter( x => {
                        x[keys.key_fullname] === servicename ? state.id -- : null
                        return x[keys.key_fullname] !== servicename
                    } )
                }

            })

            state.cart.forEach( (x, index) => {
                x.id = index + 1
            })

            console.log("Deleting from the cart:")
            console.log(state.cart)
            console.log("---------------------------------------------------------")

            if (state.cart.length > 0) {
                library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})
            } else {
                library_c.clearItemLocalStorage("cart")
            }

            return {...state}

        case 'CHANGE_ACTION_SELECTED_SRVS_CART':

            console.log("-------- CART.Change Action for Selected Srvs Cart ---------")

            keys = action.keys

            action.payload.forEach( service => {

                // Проверка на наличие Service. Если нет, то используется fullname
                if(service[keys.key_params][keys.key_servicename]) {
                    servicename = service[keys.key_params][keys.key_servicename]
                    job = state.cart.find( x => x[keys.key_params][keys.key_servicename] === servicename)
                } else {
                    servicename = service[keys.key_fullname]
                    job = state.cart.find( x => x[keys.key_fullname] === servicename)
                }

                library_c.replaceValueInParams(job, keys.keys.microservice.Action, action.action)

            })

            console.log("Changing the cart:")
            console.log(state.cart)
            console.log("---------------------------------------------------------")

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            return {...state}

        case "ADD_UPLOADED_SRVS_CART":

            console.log("---------- CART.ADD uploaded services to CART -----------")

            state.cart = action.payload

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "ADD_TO_CART_FROM_HISTORY":

            console.log("------------- CART.ADD to CART from History -------------")

            console.log(action.jobs)

            jobs = action.jobs
            state.cart = jobs
            state.id = jobs.length + 1
            state.env = jobs[0].fullname.split("/")[2] || jobs[0].params.environment

            library_c.saveToLocalStorage("cart", {cart:state.cart,env:state.env,id:state.id})

            console.log(state)
            console.log("---------------------------------------------------------")

            return {...state}

        case "SHOW_CART":

            console.log("--------------------- CART.SHOW Cart --------------------")

            state.showCart = !state.showCart
            console.log(state.showCart)

            console.log("---------------------------------------------------------")

            return {...state}

        default:

            cart = library_c.getItemLocalStorage("cart")

            if (cart !== null) {

                state = {...state,...cart}

            }

            return {...state}

    }
}