const ping = require('ping');

const checking_connection = (host) => new Promise((resolve, reject) => {
    ping.promise.probe(host)
        .then(function (res) {
            resolve(res.alive)
        });
})

module.exports = {
    checking_connection
}
