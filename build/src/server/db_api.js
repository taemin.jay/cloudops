const sqlite3 = require('sqlite3').verbose(),
    logging = require('./logging'),
    LOG_db = new logging.log("db_api.js",'', false)

const dbConnect = (db_path) => {
    let db_conn = new sqlite3.Database(db_path)
    LOG_db.info('SQLite DB is exist. Connection established.')
    return db_conn
}

const dbClose = (db_conn) => {
    db_conn.close((err) => {
        if (err) {
            return false
        }
        LOG_db.info('Connection closed.');
    });
}


const DBGetTempTitle = (db_conn) => new Promise((resolve,reject) => {
    LOG_db.info("==========> DBGetTempTitle <==========")
    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM templates`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            LOG_db.info(`Length: ${rows.length}`)
            return resolve(rows)
        });
    })
})

const DBGetTempSrvs = (db_conn, id) => new Promise((resolve,reject) => {
    LOG_db.info("==========> DBGetTempSrvs <==========")
    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM templates_srvs WHERE ID_TEMP=${id}`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            LOG_db.info(`Length: ${rows.length}, ${rows}`)
            return resolve(rows)

        });
    })
})

const DBDeleteTemplate = (db_conn,id) => new Promise((resolve,reject) => {
    LOG_db.info("==========> DBDeleteTemplate <==========")
    Promise.all([DBDeleteTitle(db_conn,id), DBDeleteSrvs(db_conn,id)])
        .then(resolve({"status": `Template #${id} deleted successfully!`}))
        .catch(reject({"status": `Template #${id} deleted failed!`}))
})

const DBDeleteTitle = (db_conn, id) => new Promise((resolve,reject) => {
    db_conn.serialize(function () {
        db_conn.all(`DELETE FROM templates WHERE ID_TEMP = ${id}`, [], (err) => {
            LOG_db.info("==========> DBDeleteTitle")
            if (err) {
                return reject(err)
            }
            LOG_db.info(`Template with id=${id} has been deleted from "templates" table.`)
            return resolve(true)
        });
    })
})

const DBDeleteSrvs = (db_conn, id) => new Promise((resolve,reject) => {
    db_conn.serialize(function () {
        db_conn.all(`DELETE FROM templates_srvs WHERE ID_TEMP = ${id}`, [], (err) => {
            LOG_db.info("==========> DBDeleteSrvs")
            if (err) {
                return reject(err)
            }
            LOG_db.info(`Template srvs with id=${id} has been deleted from "templates_srvs" table.`)
            return resolve(true)
        });
    })
})

const DBSaveTemplate = (db_conn, array) => new Promise((resolve,reject) => {
    LOG_db.info("==========> DBSaveTemplate <==========")
    const name_temp = array.name,
        count_temp = array.count,
        createDate = new Date(),
        updateDate = new Date(),
        disabled = false
    LOG_db.debug(`${name_temp},${count_temp},${createDate},${updateDate}`)

    LOG_db.info("==========> DBSaveTemplate Title")
    db_conn.run(`INSERT INTO templates(NAME_TEMP, COUNT_TEMP, DISABLE, CREATEDATE, UPDATEDATE) VALUES('${name_temp}','${count_temp}','${disabled}','${createDate}','${updateDate}')`, function(err) {
        if (err) {
            LOG_db.error(err);
            return reject(err)
        }
        LOG_db.info(`TEMP_ID inserted ${this.lastID}`);
        const id_temp = this.lastID
        LOG_db.info("==========> DBSaveTemplate Services list")
        JSON.parse(array.services).forEach( (service, index) => {
            const id_srv = index+1,
                srv_name = service.Service,
                srv_action = "",
                srv_params = ""

            db_conn.run(`INSERT INTO templates_srvs VALUES('${id_temp}','${id_srv}','${srv_name}','${srv_action}','${srv_params}','${createDate}','${updateDate}')`, function(err) {
                if (err) {
                    LOG_db.error(err);
                    return reject(err)
                }
                return resolve({"status": "Template added successfully!"})
            })

        })
    });
})

const DBUpdateTemplateTitle = (db_conn, id, value) => new Promise ((resolve, reject) => {
    LOG_db.info("==========> DBUpdateTemplateTitle <==========")

    db_conn.run(`UPDATE templates SET NAME_TEMP='${value}' WHERE ID_TEMP=${id}`, (err) => {
        if (err) {
            LOG_db.error(err)
            return reject(err)
        }
        return resolve({"status": `Template #${id} updated successfully!`})
    })
})

const titlesTemplatesFormate = (rows) => {
    LOG_db.info("==========> titlesTemplatesFormate <==========")
    let json = []
    rows.forEach( row => {
        json.push(
            {
                id: row.ID_TEMP,
                name: row.NAME_TEMP,
                count: row.COUNT_TEMP,
                disabled: row.DISABLE
            }
        )
    })
    LOG_db.debug(JSON.stringify(json))
    return json
}

const srvsTemplatesFormate = (rows) => {
    LOG_db.info("==========> srvsTemplatesFormate <==========")
    let json = []
    rows.forEach( row => {
        json.push(
            {
                id: row.ID_SERVICE,
                Service: row.SERVICE_NAME,
                upToDate: row.UPDATEDATE
            }
        )
    })
    LOG_db.debug(JSON.stringify(json))
    return json
}


module.exports = {
    dbConnect,
    dbClose,
    DBGetTempTitle,
    DBGetTempSrvs,
    titlesTemplatesFormate,
    srvsTemplatesFormate,
    DBDeleteTemplate,
    DBSaveTemplate,
    DBUpdateTemplateTitle
}