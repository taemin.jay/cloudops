const http = require('http');

const appServices = {
    'create-json': {
        srvsTemplatesFormatehostname: '0.0.0.0',
        //hostname: 'create-json-tool',
        port: 9999,
        path: '/api/v2',
        api: {
            getEnvs: '/get-envs',
            getSrvList: '/all-services/cache',
            updateSrvList: '/all-services/generate',//на данный момент данный запрос дергается напрямую из клиента #000001
            excludes: '/env-exclude/',
            simpleIncludes: '/simple-include'
        }
    },
    'jenkins-tool': {
        hostname: '0.0.0.0',
        //hostname: 'process-jenkins-tool',
        port: 5050,
        path: '',
        api: {
            getHistory: '/get_all_status',
            startTask: '/server',
            getTaskStatus: '/get_request_status/'
        }
    },
    'aws-tool': {
        hostname: '0.0.0.0',
        //hostname: 'aws-tool',
        port: 5001,
        path: '',
        api: {
            getAllList: '/ec2/info',
            checkSSH: '/ec2/check_instance/',
            getAlarmNode: '/cloudwatch/alarm/history/',
            getAllAlarm: '/cloudwatch/alarms/process'
        }
    },
    'splunk-tool': {
        hostname: '0.0.0.0',
        //hostname: 'splunk-tool',
        port: 8081,
        path: '',
        api: {
            execute: '/execute',
            status: '/status/',
            result: '/result/'
        }
    }
}


var getOptions = (tool, apiName, env_or_id, method) => {
    let path,
        app = appServices[tool]

    if (env_or_id) {
        path = app.path + app.api[apiName] + env_or_id
    } else {
        path = app.path + app.api[apiName]
    }

    return {
        hostname: app.hostname,
        port: app.port,
        path: path,
        method: method,
        timeout: 600000,
        headers: {
            'Content-Type': 'application/json'
        }
    }

}

const getRequestAPI = (options) => new Promise((resolve, reject) => {

    let req = http.request(options, (res) => {

        let chunks = [], result

        res.on('data', (data) => {
            chunks.push(data)
        }).on('end', () => {
            let data   = Buffer.concat(chunks)

            try {
                result = JSON.parse(data)

            } catch {
                result = data.toString()
            }

            resolve(result)

        });
    });

    req.on('error', (e) => {
        reject(e);
    });
    req.end();
});

const postRequestAPI = (options, data) => new Promise((resolve, reject) => {

    console.log(data)

    let req = http.request(options, (res) => {
        let chunks = []
        res.on('data', (answer) => {
            chunks.push(answer)
            console.log(chunks)
        }).on('end', () => {
            console.log("END")
            let data   = Buffer.concat(chunks), result
            try {
                result = JSON.parse(data)
            } catch {
                result = data.toString("utf8")
            }

            console.log(result)

            resolve(result)
        });
    });

    req.on('error', (e) => {
        reject(e);
    });
    req.write(JSON.stringify(data));
    req.end();
});


module.exports = {
    appServices,
    getRequestAPI,
    postRequestAPI,
    getOptions
}