const yaml = require('yaml')
const fs = require('fs')

const get_config = (CUSTOM_CONFIG, DEFAULT_CONFIG) => new Promise((resolve) => {

    let json

    try {

        fs.promises.access(CUSTOM_CONFIG);
        json = read_api_config(CUSTOM_CONFIG)

        json.message = "Using Custom config " + CUSTOM_CONFIG

        resolve(json)

    } catch (error1) {

        try {

            fs.promises.access(DEFAULT_CONFIG);
            json = read_api_config(DEFAULT_CONFIG)

            json.message = "WARNING! Custom config is not set. Using " + DEFAULT_CONFIG + "."
            json.error1 = error1

            resolve(json)

        } catch (error2) {

            json = {
                message: "ERROR! Custom and Default configs are not present. " +
                "Please make sure that you set "+CUSTOM_CONFIG+" file as volume in the docker-compose " +
                "or add this to 'volume/web-tool' folder",
                error1: error1,
                error2: error2
            }

            resolve(json)
        }
    }

})

const read_api_config = (config) => {

    const file = fs.readFileSync(config, 'utf8')
    const json = yaml.parse(file)
    return json

}

module.exports = {
    get_config
}