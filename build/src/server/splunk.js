const http = require('http'),
    fs = require('fs'),
    lazy = require('lazy')

const getRequestAPI = (options) => new Promise((resolve, reject) => {

    let req = http.request(options, (res) => {

        let chunks = [], buff, length

        res.on('data', (data) => {

            chunks.push(data)

        }).on('end', async () => {

            let data = Buffer.concat(chunks)

            try {

                buff = JSON.parse(data)
                length = buff.length
                buff = JSON.stringify(buff)

            } catch {

                buff = data.toString()
                length = (buff.split("\n")).length

            } finally {

                console.log(buff)

                fs.writeFile(__dirname + '/splunk_webData', buff, (err) => {
                    if (err) return false
                    console.log("SUCCESS")

                    resolve(length);

                })

            }

        })

    });

    req.on('error', (e) => {
        reject(e);
    });
    req.end();
});

const getChunkData = (start, end) => new Promise((resolve, reject) => {

    let data = []
    new lazy(fs.createReadStream(__dirname + '/splunk_webData'))
        .lines
        .skip(start-1)
        .take(end)
        .forEach( line => {

            data.push(JSON.parse(line));

        })
        .on('pipe', (err) => {
            if (err) reject(err)
            resolve(data)
        })

})


module.exports = {
    getRequestAPI,
    getChunkData
}