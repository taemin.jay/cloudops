const groupHistoryJobFunction = (history_list) => {

    //группировка истории по id запущенных тасок. Необходимо, т.к. получаю массив из всех запущенных джоб, а не тасок
    let id_list = [],
        history_items = [],
        jobs = [],
        id_list_uni,
        status,
        env

    for (var i = 0; i < history_list.length; i++) {
        id_list.push(history_list[i].id);
    }

    //проверка + группировка джоб по id
    id_list_uni = Array.from(new Set(id_list));
    id_list_uni.forEach( id => {
        jobs = history_list.filter( x => x.id === id)
        status = checkHistoryStatus(jobs)
        env = getEnvOfTask(jobs[0])
        history_items.push({"id": id, "status":status, "env":env, "count": jobs.length, "jobs": jobs});
    })

    return history_items
}

const getEnvOfTask = (job) => {

    return job.job.fullname.split("/")[2] || job.job.params.environment

}

const checkHistoryStatus = (jobs) => {

    const status_error = [
        "Not found",
        "FAILED",
        "FAILURE"
    ],
        status_process = [
            "in process",
            "in queue"
        ],
        status_success = [
            "SUCCESS",
            "finished"
        ]


    if (jobs.some( job => status_process.includes(job.status))) {
        return "IN PROCESS"
    } else if (!jobs.some( job => status_error.includes(job.status))) {
        return "SUCCESS"
    } else if (jobs.some( job => status_error.includes(job.status)) && jobs.some( job => status_success.includes(job.status))) {
        return "WARNING"
    } else {
        return "ERROR"
    }

    /*Checking status
    let notfound = jobs.filter( x => x.status === "Not found").length,
        error = jobs.filter( x => x.status === "FAILED" || x.status === "FAILURE").length,
        process = jobs.filter( x => x.status === "in process" || x.status === "in queue").length,
        success = jobs.filter( x => x.status === "SUCCESS" || x.status === "finished").length
    console.log("id:"+id,"notfound:"+notfound, "error:"+error, "success:"+success, "process:"+process)
    */

}

module.exports = {
    groupHistoryJobFunction
}